2 minute introduction to the project (concept & goals)

5 to 6 min narrated demonstration of the final product

3 minute explanation of how images turn into geometry

1 to 2 minutes critiquing my own work implementing this system (pros of how easy it was to make new mesh types after the initial algorithm was implememnted, cons of how you had to do lots of things in a specific order and classes got very bloated trying to handle all this)

2 minutes on the attempt at compute shaders

3 minutes on the success of multithreading and what got multithreaded, including some examples of profiling before and after code was converted to multithreading

1 minute showing research into things that didnt work out otherwise (semaphores, nativestream)

brief mention of the packages used for stuff i couldnt figure out on my own or was not the research focus of the project (simplefilebrowser, psd package that didnt work out, compound collider search)






POINTS TO CONSIDER FOR HIGHER GRADES:
- Refer back to your literature as often as you can. Let's take the example of compute shaders. Imagine if this was in your literature review initially, you could draw reference to the fact that you uncovered that methodology prior to impelementaiton and therefore discuss pros and cons.

- When you talk about a procedure, such as multi-threading, make sure you encapsulate the benefits in your discussion. It would also be usefull to compare/contrast two implementations (single vs multi thread) which showcases how much more performant the latter actually is. If you can display this via graph, that's a BIG plus. Source control will save you a headache here

- The main point is to back up why you are talking about specific methodologies; don't pluck them from thin air. Highlight a problem, highlight a solution, and then discuss the implementation (whether it's one for the future or it's in the current application).

Other than that, I think that's a fantastic rundown. If you want to send me bits of your video I will happily review multiple times - it does not have to be the complete video. 