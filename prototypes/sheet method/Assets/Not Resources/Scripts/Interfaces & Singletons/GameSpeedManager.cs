using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpeedManager:MonoBehaviour,IEditorFunctionality
{

    [SerializeField] float slowdownSpeed = 0.05f;


    [SerializeField] float FOVMultiplier_comedownTimer = 0.3f;
    private float FOVMultiplier_comedownTimer_current = 0.0f;

    [SerializeField] float FOVMultiplier = 1.13f;

    List<float> slowdownEntries = new List<float>();



    


    private static GameSpeedManager _instance;

    public static GameSpeedManager Instance { get { return _instance; } }//property for the private static _instance, only uncomment this if you need other classes to access the singleton script directly.


    void SingletonExecution()
    {
        if (_instance != null && _instance != this)
        {
            ExtendedFunctionality.SafeDestroy(gameObject);

        }
        else
        {

            _instance = this;
        }



        gameObject.name = "GameSpeedManager";
    }

    private void Update()
    {
        SingletonExecution();
        AnalyzeSlowdownEntries();
    }

    void AnalyzeSlowdownEntries()
    {



        List<float> newEntries = new List<float>();

        foreach(float timer in slowdownEntries)
        {
            if (timer > 0)
            {
                newEntries.Add(timer);
            }
        }

        slowdownEntries = newEntries;

        for(int i=0;i<slowdownEntries.Count;i++)
        {

            slowdownEntries[i] -= Time.unscaledDeltaTime;
        }

        
        if((slowdownEntries!=null && slowdownEntries.Count <= 0) || slowdownEntries == null)
        {
            Time.timeScale = 1.0f;
        }
        else
        {
            Time.timeScale = slowdownSpeed;
        }

        Time.fixedDeltaTime = 0.02f * Time.timeScale;//0.02f is the default value of fixeddeltatime.


        CamArmControl[] camArmScr = GameObject.FindObjectsOfType<CamArmControl>();

        if (camArmScr != null)
        {
            for(int i = 0; i < camArmScr.Length; i++)
            {
                if (camArmScr[i] != null)
                {
                    if (camArmScr[i].CamScr != null)
                    {
                        float origFov = camArmScr[i].OrigFOV ?? 60;

                        float newFOV = origFov * FOVMultiplier;


                        float fovProgress = Mathf.InverseLerp(0, FOVMultiplier_comedownTimer, FOVMultiplier_comedownTimer_current);

                        camArmScr[i].CamScr.fieldOfView = Mathf.Lerp(origFov, newFOV, fovProgress);

                    }
                }

            }
        }

        if(FOVMultiplier_comedownTimer_current>0)
        {
            FOVMultiplier_comedownTimer_current -= Time.unscaledDeltaTime;
        }
        else
        {
            FOVMultiplier_comedownTimer_current = 0;
        }

    }

    void OnDestroy()
    {
        slowdownEntries.Clear();
        AnalyzeSlowdownEntries();
    }

    public void AddSlowdownTimer(float slowdownTime)
    {
        slowdownEntries.Add(slowdownTime);
        FOVMultiplier_comedownTimer_current = FOVMultiplier_comedownTimer;
    }

    public void EditorUpdate()
    {
        SingletonExecution();
    }
}
