﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//put onto any class that needs editor update call functionality.
public interface IEditorFunctionality
{
    void EditorUpdate();

}

[ExecuteInEditMode]
//i wouldn't have typically made this a singleton, but its good to show i know how they work and because otherwise id be repeating myself running EditorUpdate() from every IEditorFunctionality instance one by one
//this code is unoptimized, but since it only runs in editor, it shouldn't matter.

public class IEditorFunctionalityManager:MonoBehaviour
{
    public List<IEditorFunctionality> ALL_EDITOR_FUNCTIONALITY_SCRIPTS = new List<IEditorFunctionality>();//as far as i know, lists cannot be properties, so this is public without the get-set stuff

    private static IEditorFunctionalityManager _instance;

    //public static IEditorFunctionalityManager Instance { get { return _instance; } }//property for the private static _instance, only uncomment this if you need other classes to access the singleton script directly.


    void SingletonExecution()
    {
        if (!(Application.isEditor && !Application.isPlaying))
        {
            if (_instance != null && _instance != this)
            {
                ExtendedFunctionality.SafeDestroy(gameObject);

            }
            else
            {

                _instance = this;
            }

        }



        
    }

    void AddEditorFunctionalityInterfaceList(IEditorFunctionality interf)
    {
        if (!ALL_EDITOR_FUNCTIONALITY_SCRIPTS.Contains(interf))
        {
            ALL_EDITOR_FUNCTIONALITY_SCRIPTS.Add(interf);
        }
    }

    void GenerateEditorFunctionalityList()
    {
        ALL_EDITOR_FUNCTIONALITY_SCRIPTS.Clear();
        IEnumerable<IEditorFunctionality> allRelevantScripts = FindObjectsOfType<MonoBehaviour>().OfType<IEditorFunctionality>();
        foreach(IEditorFunctionality thing in allRelevantScripts)
        {
            AddEditorFunctionalityInterfaceList(thing);
        }
    }

    private void Update()
    {
        SingletonExecution();
        gameObject.name = "IEditorFunctionalityManager";

        if (Application.isPlaying == false)
        {
            SingletonExecution();
            GenerateEditorFunctionalityList();
            foreach (IEditorFunctionality editorScript in ALL_EDITOR_FUNCTIONALITY_SCRIPTS)
            {
                if (editorScript != null)
                {
                    editorScript.EditorUpdate();

                }
            }
        }

    }


}