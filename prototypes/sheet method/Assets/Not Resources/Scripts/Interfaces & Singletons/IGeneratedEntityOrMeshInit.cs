using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this interface is mainly for components of gOs spawned through SheetMethod that rely on parts of SheetMethod AFTER GenerateEntities() and GenerateModels() for a proper awake() call.
public interface IGeneratedEntityOrMeshInit
{
    public void SetupEntityOrMesh(GameObject playerObj=null);
}
