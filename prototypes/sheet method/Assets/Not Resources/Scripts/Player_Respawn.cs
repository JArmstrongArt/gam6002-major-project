using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Player_Respawn : MonoBehaviour,IGeneratedEntityOrMeshInit
{
    public Vector3 RespawnPoint { set; private get; }

    private Player_GravityControl gravScr;

    private Player_Statistics statsScr;



    private InputManager inputScr;

    private GameObject sE_player_die;
    void Awake()
    {
        if (FindDependencies() && FindResources())
        {
            SetupEntityOrMesh();

        }
    }

    public void SetupEntityOrMesh(GameObject playerObj=null )
    {
        if (FindDependencies() && FindResources())
        {
            RespawnPoint = gameObject.transform.position;

        }
    }

    void Update()
    {

        if (FindDependencies() && FindResources())
        {
            if (gameObject.transform.position.y <= -Mathf.Abs(statsScr.FallToDeathWorldHeight))
            {
                Debug.Log("respawn attempted.");
                Respawn();
            }


        }
    }


    public void Respawn()
    {
        if (FindDependencies() && FindResources())
        {
            Instantiate(sE_player_die);
            ExtendedFunctionality.DirectMoveCharacterController_FallbackIfNotPresent(gameObject, RespawnPoint);
            gravScr.AbruptStopCurGravity();
            statsScr.HazardousGoop_Active = false;
            statsScr.WaterMode = false;
            statsScr.deathCount += 1;
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<Player_GravityControl>();
        }

        if (statsScr == null)
        {
            statsScr = gameObject.GetComponent<Player_Statistics>();
        }

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (inputScr!=null && statsScr != null && gravScr != null)
        {
            ret = true;
        }
        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_player_die == null)
        {
            sE_player_die = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_player_die/sE_player_die");
        }


        if (sE_player_die != null && sE_player_die != null)
        {
            ret = true;
        }

        return ret;
    }

}
