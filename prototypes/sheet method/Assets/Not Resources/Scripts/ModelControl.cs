using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ModelControl : MonoBehaviour, IGeneratedEntityOrMeshInit
{
    [SerializeField] float modelRelativeScale = 1.0f;
    [SerializeField] Vector3 modelOffset=Vector3.zero;
    [SerializeField] string modelPath = "";
    [SerializeField] bool idleOverride = false;
    [SerializeField] string idleOverride_idleAnimName = "Idle";

    private GameObject myModel_prefab;
    private GameObject myModel_inst;

    private Animator myModel_anim;


    protected Vector3 ModelOffset
    {
        set
        {
            modelOffset = value;
        }
    }

    protected Animator MyModel_Anim
    {
        get
        {
            return myModel_anim;
        }
    }

    private Renderer myRend;
    protected GameObject MyModel_Inst
    {
        get
        {
            return myModel_inst;
        }
    }

    public bool IdleOverride
    {
        set
        {
            idleOverride = value;
        }
    }


    void OnDestroy()
    {
        ExtendedFunctionality.SafeDestroy(myModel_inst);
    }

    void Update()
    {
        if (FindResources() && FindDependencies())
        {
            myRend.enabled = myModel_inst == null;

            if (myModel_inst != null)
            {
                if (myModel_anim == null)
                {
                    myModel_anim = myModel_inst.GetComponent<Animator>();
                }

                if (myModel_anim != null)
                {
                    if (idleOverride)
                    {
                        myModel_anim.Play(idleOverride_idleAnimName);
                    }
                    else
                    {
                        int? animEnumToApply = DecideAnimation_Contextual();

                        if (animEnumToApply != null)
                        {
                            DecideAnimation_ApplyFromContext(animEnumToApply??0);

                        }
                        else
                        {
                            myModel_anim.Play(idleOverride_idleAnimName);

                        }
                    }

                }

            }
        }


    }

    protected abstract int? DecideAnimation_Contextual(); 
    protected abstract void DecideAnimation_ApplyFromContext(int? animEnum);


    void SetModelPos()
    {
        if(FindResources() && FindDependencies())
        {
            if (myModel_inst != null)
            {
                myModel_inst.transform.position = gameObject.transform.position + new Vector3(modelOffset.x*gameObject.transform.localScale.x, modelOffset.y * gameObject.transform.localScale.y, modelOffset.z * gameObject.transform.localScale.z);

            }
        }
    }

    void LateUpdate()
    {
        if(FindDependencies() && FindResources())
        {
            SetModelPos();
        }
    }
    public virtual void SetupEntityOrMesh(GameObject playerObj = null)
    {
        if (FindResources() && FindDependencies() )
        {

            ExtendedFunctionality.SafeDestroy(myModel_inst);
            if (myModel_inst == null)
            {

                myModel_inst = Instantiate(myModel_prefab, null);
                myModel_inst.transform.localScale = gameObject.transform.localScale* modelRelativeScale;
                SetModelPos();

            }

        }
    }

    protected bool FindResources()
    {
        bool ret = false;

        if (myModel_prefab == null)
        {
            myModel_prefab = Resources.Load<GameObject>(modelPath);

        }

        if (myModel_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

    protected virtual bool FindDependencies()
    {
        bool ret = false;

        if (myRend == null)
        {
            myRend = gameObject.GetComponent<Renderer>();
        }

        if (myRend != null)
        {
            ret = true;
        }

        return ret;
    }


}
