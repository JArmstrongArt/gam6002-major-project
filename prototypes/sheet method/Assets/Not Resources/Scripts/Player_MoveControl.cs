using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_MoveControl : MonoBehaviour
{
    private GameObject playerCam_prefab;
    private GameObject playerCam_inst;
    private InputManager inputScr;
    [SerializeField] float moveSpeed = 13.5f;
    private float moveSpeed_current;
    public CamArmControl ArmControlScr { get; private set; }

    private CharacterController charConScr;

    private Player_GravityControl gravScr;

    private Player_Statistics statsScr;

    private float dashMultiplier_current = 1.0f;


    private Player_Dash dashScr;

    private float inp_forward, inp_side;


    [SerializeField] float friction = 9.0f;

    [SerializeField] float friction_airDivider = 4.5f;
    [SerializeField] float friction_iceDivider = 7.5f;


    public Vector3 MoveCalc { get; private set; }


    private void FixedUpdate()
    {
        if (FindDependencies() && FindResources())
        {


            if (!dashScr.DashInProgress)
            {
                float frictionMultiplier = 1.0f;



                if (gravScr.GroundRay.RaySurface != null)
                {
                    if (gravScr.GroundRay.RaySurface.GetComponent<Ice>() != null)
                    {
                        frictionMultiplier = (float)1 / (float)friction_iceDivider;

                    }

                }
                else
                {
                    frictionMultiplier = (float)1 / (float)friction_airDivider;

                }

                if (statsScr.WaterMode)
                {
                    frictionMultiplier = frictionMultiplier * ((float)1 / (float)statsScr.WaterMode_MovementDriftDivider);
                }

                inp_forward = Mathf.Lerp(inp_forward, inputScr.GetInputByName("Movement", "Vertical"), friction * frictionMultiplier * Time.fixedDeltaTime);
                inp_side = Mathf.Lerp(inp_side, inputScr.GetInputByName("Movement", "Horizontal"), friction * frictionMultiplier * Time.fixedDeltaTime);




            }






            float dashAmount_normalized = Mathf.InverseLerp(dashScr.DashDuration, 0.0f, dashScr.DashDuration_Current);


            dashMultiplier_current = Mathf.Lerp(1.0f, dashScr.DashMultiplier, dashAmount_normalized);


            if (statsScr.HazardousGoop_Active)
            {
                moveSpeed_current = moveSpeed * statsScr.HazardousGoop_MoveSpeed_Multiplier* dashMultiplier_current;
            }
            else
            {
                moveSpeed_current = moveSpeed * dashMultiplier_current;
            }

            if (playerCam_inst == null)
            {
                playerCam_inst = Instantiate(playerCam_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                ArmControlScr = playerCam_inst.GetComponent<CamArmControl>();


            }
            else
            {
                if (ArmControlScr != null)
                {
                    ArmControlScr.TrackObj = gameObject;









                    Vector3 moveForward = ArmControlScr.FlatDirs.forDir * moveSpeed_current * inp_forward;
                    Vector3 moveRight = ArmControlScr.FlatDirs.sideDir * moveSpeed_current * inp_side;




                    MoveCalc = moveForward + moveRight;


                    if (dashScr.DashInProgress)
                    {
                        Vector3 moveDir = MoveCalc.normalized;
                        MoveCalc = moveDir * moveSpeed_current;
                    }

                    charConScr.Move(MoveCalc * Time.fixedDeltaTime);






                }
            }


        }
    }





    bool FindResources()
    {
        bool ret = false;

        if (playerCam_prefab==null)
        {
            playerCam_prefab = Resources.Load<GameObject>("Entities/Player/playerCam_arm");
        }

        if (playerCam_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (charConScr == null)
        {
            charConScr = gameObject.GetComponent<CharacterController>();
        }

        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<Player_GravityControl>();
        }

        if (statsScr == null)
        {
            statsScr = gameObject.GetComponent<Player_Statistics>();
        }

        if (dashScr == null)
        {
            dashScr = gameObject.GetComponent<Player_Dash>();
        }

        if (dashScr!=null && statsScr!=null && inputScr != null && charConScr!=null && gravScr!=null)
        {
            ret = true;
        }

        return ret;
    }
}
