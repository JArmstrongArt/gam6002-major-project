using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum E_SineType
{
    Sine,
    Cosine
}

public enum E_WorldAxes
{
    X,
    Y,
    Z
}






[System.Serializable]
public struct SineData
{
    [SerializeField] Vector3 rootPos;
    public Vector3 RootPos
    {
        get
        {
            return rootPos;
        }
    }

    [SerializeField] E_SineType sineType;
    public E_SineType SineType
    {
        get
        {
            return sineType;
        }
    }


    [SerializeField] E_WorldAxes sineAxis;
    public E_WorldAxes SineAxis
    {
        get
        {
            return sineAxis;
        }
    }

    [SerializeField] float sinePower;
    public float SinePower
    {
        get
        {
            return sinePower;
        }
    }

    [SerializeField] float sineFrequency;
    public float SineFrequency
    {
        get
        {
            return sineFrequency;
        }
    }

    public float sineDelayFromWorldTime;



    public SineData(Vector3 rP, E_SineType sT, E_WorldAxes sA, float sP, float sF, float sDFWT = 0.0f)
    {
        rootPos = rP;
        sineType = sT;
        sineAxis = sA;
        sinePower = sP;
        sineFrequency = sF;
        sineDelayFromWorldTime = sDFWT;

    }




}

public class SineMove : MonoBehaviour,IGeneratedEntityOrMeshInit
{
    [Tooltip("NOTE: RootPos in this component is ignored.")]
    [SerializeField] SineData sineData;
    public SineData SineData
    {
        get
        {
            return sineData;
        }

        set
        {
            sineData = value;
        }
    }
    private Vector3 rootPosOverride;

    void Awake()
    {
        SetupEntityOrMesh();
    }

    void SetupSineData()
    {
        rootPosOverride = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 sinePos = ExtendedFunctionality.GetSinePos(sineData, rootPosOverride);

        Vector3 sineDifference = sinePos - rootPosOverride;

        sineDifference = new Vector3(sineDifference.x * gameObject.transform.localScale.x, sineDifference.y * gameObject.transform.localScale.y, sineDifference.z * gameObject.transform.localScale.z);

        gameObject.transform.position = rootPosOverride+ sineDifference;

    }

    public void SetupEntityOrMesh(GameObject playerObj = null)
    {
        SetupSineData();

    }
}