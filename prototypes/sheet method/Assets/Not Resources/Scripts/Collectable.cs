using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Collectable : MonoBehaviour
{
    [SerializeField] Vector2 collected_moveSpeed_range;
    [SerializeField] Vector2 collected_gravityJump_range;

    private GameObject sE_collectable;

    private void OnTriggerEnter(Collider other)
    {
        if (FindResources())
        {
            Player_Statistics playerProof = other.GetComponent<Player_Statistics>();
            if (playerProof != null)
            {
                Instantiate(sE_collectable);


                playerProof.Score += 1;


                GameObject collectedObj = Instantiate(gameObject, gameObject.transform.position, gameObject.transform.rotation, gameObject.transform.parent);
                collectedObj.transform.localScale = gameObject.transform.localScale;

                List<System.Type> whitelist = new List<System.Type>
            {
                typeof(MeshRenderer),
                typeof(MeshFilter),
                typeof(Spin),
                typeof(ModelControl)

            };

                ExtendedFunctionality.GutComponentsFromGameObject(collectedObj, whitelist);

                GenericGravity collectedObj_gravity = collectedObj.AddComponent<GenericGravity>();
                collectedObj_gravity.SelfMoving = true;
                collectedObj_gravity.GravityCurrent = Random.Range(collected_gravityJump_range.x, collected_gravityJump_range.y);
                GenericMoveInDir collectedObj_moveInDir = collectedObj.AddComponent<GenericMoveInDir>();
                collectedObj_moveInDir.SelfMoving = true;
                collectedObj_moveInDir.MoveDir = new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1)).normalized;
                collectedObj_moveInDir.MoveSpeed = Random.Range(collected_moveSpeed_range.x, collected_moveSpeed_range.y);

                Spin collectedObj_spin = collectedObj.GetComponent<Spin>();

                if (collectedObj_spin != null)
                {
                    collectedObj_spin.SpinPower *= Random.Range(1, 2.5f);
                }

                collectedObj.AddComponent<LifeTimer>();

                ModelControl mdlScr = collectedObj.GetComponent<ModelControl>();

                if (mdlScr != null)
                {
                    mdlScr.SetupEntityOrMesh(null);

                    Collectable_ModelControl colMdlScr = (Collectable_ModelControl)mdlScr;
                    Collectable_ModelControl colMdlScr_mine = gameObject.GetComponent<Collectable_ModelControl>();
                    if (colMdlScr != null)
                    {
                        if (colMdlScr_mine != null)
                        {
                            colMdlScr.SetSmileyColour(colMdlScr_mine.GetSmileyColour());

                        }
                        else
                        {
                            colMdlScr.SetSmileyColour();

                        }
                    }
                }


                ExtendedFunctionality.SafeDestroy(gameObject);
            }
        }

    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_collectable == null)
        {
            sE_collectable = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_collectable/sE_collectable");
        }

        if (sE_collectable != null)
        {
            ret = true;
        }

        return ret;
    }
}
