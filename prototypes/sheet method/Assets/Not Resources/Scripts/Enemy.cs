using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IGeneratedEntityOrMeshInit
{

    private Vector3 origScale = Vector3.one;
    [SerializeField] float scaleMultiplier = 1.8f;
    private GameObject playerEntObj;
    private LookAtObject lookAtScr;
    [SerializeField] float awarenessRadius = 12.75f;

    [SerializeField] float shotChargeTime = 2f;
    private float shotChargeTime_current = 0.0f;

    private GenericRay wallCheckRay;

    private LineRenderer lineRend;


    [SerializeField] float lineOfSight_width = 1.0f;
    [SerializeField] float deathSlowdownLength = 0.135f;
    private GameObject sE_enemy_shoot;

    private GameObject sE_enemy_die;

    private bool slowdownOnDeath = false;
    public bool SlowdownOnDeath
    {
        set
        {
            slowdownOnDeath = value;
        }
    }

    private bool soundEffectOnDeath = false;
    public bool SoundEffectOnDeath
    {
        set
        {
            soundEffectOnDeath = value;
        }
    }
    float GetScalesAwarenessRadius()
    {
        return awarenessRadius * ExtendedFunctionality.GetAverageOfVec3Dimensions(origScale);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(gameObject.transform.position, GetScalesAwarenessRadius());
    }

    void Awake()
    {
        if (FindDependencies() && FindResources())
        {
            SetupEntityOrMesh();

        }
    }

    void Update()
    {
        if (FindDependencies() && FindResources())
        {
            if (playerEntObj != null)
            {
                float scaleProgress = Mathf.InverseLerp(0, shotChargeTime, shotChargeTime_current);


                Vector3 meToPlayer = (playerEntObj.transform.position - gameObject.transform.position);



                wallCheckRay.RayLength = meToPlayer.magnitude;
                wallCheckRay.CustomDirVector = meToPlayer.normalized;


                bool withinRange = meToPlayer.magnitude <= GetScalesAwarenessRadius();
                bool noWall = wallCheckRay.RaySurface == null;

                Vector3[] lineRend_posArray = new Vector3[0];

                if (withinRange && noWall)
                {


                    if (shotChargeTime_current < shotChargeTime)
                    {
                        shotChargeTime_current += Time.deltaTime;
                    }

                    if (shotChargeTime_current >= shotChargeTime)
                    {
                        //fire shot
                        Instantiate(sE_enemy_shoot);
                        Player_Respawn respawnScr = playerEntObj.GetComponent<Player_Respawn>();
                        if (respawnScr != null)
                        {



                            respawnScr.Respawn();
                        }


                        shotChargeTime_current = 0.0f;
                    }

                    lineRend_posArray = new Vector3[] { gameObject.transform.position, playerEntObj.transform.position };

                }
                else
                {

                    shotChargeTime_current = Mathf.Lerp(shotChargeTime_current, 0.0f, 5 * Time.deltaTime);

                }

                lineRend.useWorldSpace = true;
                lineRend.SetPositions(lineRend_posArray);

                lineRend.widthMultiplier = scaleProgress*lineOfSight_width;


                gameObject.transform.localScale = Vector3.Lerp(origScale, new Vector3(origScale.x * scaleMultiplier, origScale.y * scaleMultiplier, origScale.z * scaleMultiplier), scaleProgress);

            }
        }

    }

    public void SetupEntityOrMesh(GameObject playerObj = null)
    {
        if (FindDependencies() && FindResources())
        {
            playerEntObj = playerObj;
            lookAtScr.LookTarget = playerEntObj;
            origScale = gameObject.transform.localScale;
        }
    }

    void OnDestroy()
    {
        if (FindDependencies() && FindResources())
        {
            if (slowdownOnDeath)
            {
                soundEffectOnDeath = true;
                if (GameSpeedManager.Instance != null)
                {
                    GameSpeedManager.Instance.AddSlowdownTimer(deathSlowdownLength);
                }
            }

            if (soundEffectOnDeath)
            {
                Instantiate(sE_enemy_die);

                Player_Statistics[] allStats = GameObject.FindObjectsOfType<Player_Statistics>();

                if (allStats != null)
                {
                    for(int i = 0; i < allStats.Length; i++)
                    {
                        if (allStats[i] != null)
                        {
                            allStats[i].enemyKillCount += 1;

                        }
                    }
                }

            }



        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (lookAtScr == null)
        {
            lookAtScr = gameObject.GetComponent<LookAtObject>();
        }

        if (wallCheckRay == null)
        {
            wallCheckRay = ExtendedFunctionality.GetComponentInChildrenByName<GenericRay>(gameObject, "wallCheckRay");
        }

        if (lineRend == null)
        {
            lineRend = gameObject.GetComponent<LineRenderer>();
        }

        if (lookAtScr != null && wallCheckRay!=null && lineRend!=null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_enemy_shoot == null)
        {
            sE_enemy_shoot = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_enemy_shoot/sE_enemy_shoot");
        }

        if (sE_enemy_die == null)
        {
            sE_enemy_die = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_enemy_die/sE_enemy_die");
        }

        if (sE_enemy_shoot != null && sE_enemy_die!=null)
        {
            ret = true;
        }

        return ret;
    }
}
