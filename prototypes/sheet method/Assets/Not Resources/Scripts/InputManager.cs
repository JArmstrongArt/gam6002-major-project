using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



//this whole thing is just a way to pass the unity input system into one that accepts the entire controls being disabled with a bool, i pass this script around a lot of my projects.

//a struct that holds the info i may need about any given unity input
[Serializable]
public struct singleInputInfo
{
    [SerializeField] string inputName;
    public string InputName
    {
        get
        {
            return inputName;
        }

        set
        {
            inputName = value;
        }
    }



    public float inputValue;



    [SerializeField] bool unaffectedByAllInputsDisabled;
    public bool UnaffectedByAllInputsDisabled
    {
        get
        {
            return unaffectedByAllInputsDisabled;
        }
    }








}


[Serializable]
public class inputGroup
{
    [SerializeField] string inputGroupName;
    public string InputGroupName
    {
        get
        {
            return inputGroupName;
        }
    }

    [SerializeField] singleInputInfo[] inputsInGroup;
    public singleInputInfo[] InputsInGroup
    {
        get
        {
            return inputsInGroup;
        }
    }


}


public class InputManager : MonoBehaviour
{

    [SerializeField] bool inputEnabled=true;
    public bool InputEnabled
    {

        set
        {
            inputEnabled = value;
        }
    }
    [SerializeField] inputGroup[] allInputValues;


    void RegularInputActivity(int x, int y)
    {
        string inName = allInputValues[x].InputsInGroup[y].InputName;

        float lowerClamp = float.MinValue;


        allInputValues[x].InputsInGroup[y].inputValue = Mathf.Clamp(Input.GetAxis(inName), lowerClamp, float.MaxValue);



    }

    void ZeroInputActivity(int x, int y)
    {
        allInputValues[x].InputsInGroup[y].inputValue = 0;
    }



    // Update is called once per frame
    void Update()
    {
        //for every input i want to be getting
        for (int i = 0; i < allInputValues.Length; i++)
        {
            for (int j = 0; j < allInputValues[i].InputsInGroup.Length; j++)
            {


                if (inputEnabled == true)
                {


                    RegularInputActivity(i, j);
                }
                else
                {
                    if (allInputValues[i].InputsInGroup[j].UnaffectedByAllInputsDisabled == false)
                    {
                        ZeroInputActivity(i, j);
                    }
                    else
                    {
                        RegularInputActivity(i, j);
                    }


                }


            }
        }
    }

    public void SetInputStateByName(string groupName, string inputName)
    {
        (int groupIndex, int nameIndex) index = GetSingleInputInfoIndexByName(groupName, inputName) ?? (0, 0);




    }

    //the function other classes use once they get a reference to this class object in order to get input values.
    public float GetInputByName(string groupName, string inputName)
    {
        (int groupIndex, int nameIndex) index = GetSingleInputInfoIndexByName(groupName, inputName)??(0,0);



        return allInputValues[index.groupIndex].InputsInGroup[index.nameIndex].inputValue;
    }

    (int groupIndex, int nameIndex)? GetSingleInputInfoIndexByName(string groupName, string inputName)
    {
        (int groupIndex, int nameIndex)? ret = null;
        for (int i = 0; i < allInputValues.Length; i++)
        {
            for (int j = 0; j < allInputValues[i].InputsInGroup.Length; j++)
            {
                if (allInputValues[i].InputGroupName == groupName)
                {

                    if (allInputValues[i].InputsInGroup[j].InputName == inputName)
                    {
                        ret = (i, j);
                        break;



                    }
                }

            }

        }
        return ret;
    }






}
