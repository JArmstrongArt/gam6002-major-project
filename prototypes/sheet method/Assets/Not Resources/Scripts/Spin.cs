using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    [SerializeField] Vector3 spinPower;
    public Vector3 SpinPower
    {
        get
        {
            return spinPower;
        }

        set
        {
            spinPower = value;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(new Vector3(spinPower.x * Time.deltaTime, spinPower.y * Time.deltaTime, spinPower.z * Time.deltaTime));
    }
}
