using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTimer : MonoBehaviour
{
    [SerializeField] float lifeTime = 5.0f;
    public float LifeTime
    {
        set
        {
            lifeTime = value;
        }

        get
        {
            return lifeTime;
        }
    }


    void Update()
    {
        if (lifeTime > 0)
        {
            lifeTime -= Time.deltaTime;
        }
        else
        {
            ExtendedFunctionality.SafeDestroy(gameObject);
        }
    }
}
