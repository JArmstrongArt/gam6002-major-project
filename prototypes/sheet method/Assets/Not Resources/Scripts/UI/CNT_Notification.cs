using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public abstract class CNT_Notification : UIElemBase
{
    private int? overrideFontSize = null;

    public int? OverrideFontSize
    {
        set
        {
            overrideFontSize = value;
        }
    }

    private GameObject btn_uiOption_prefab;

    private List<GameObject> btn_uiOption_insts = new List<GameObject>();

    private RectTransform selfRect;


    private Text txt_title;

 

    private Text txt_notif;


    private float btnWidthMultiplier = 1.0f;

    public float BtnWidthMultiplier
    {
        set
        {
            btnWidthMultiplier = value;
        }
    }


    void SetupNotification(string titleTxt, string notifTxt)
    {
        if(FindResources() && FindDependencies())
        {

            txt_title.text = titleTxt.ToUpper();
            txt_notif.text = notifTxt.ToUpper();
            txt_notif.fontSize = overrideFontSize ?? txt_notif.fontSize;
        }
    }

    protected void CreateButton(string? nameTxt = null, List<UnityEngine.Events.UnityAction> allButtonFunctions = null)
    {
        if (FindResources() && FindDependencies())
        {
            GameObject btnInst = SpawnObjectUnderThisElement(btn_uiOption_prefab);

            btn_uiOption_insts.Add(btnInst);
            BTN_UIOption btnInst_scr = btnInst.GetComponent<BTN_UIOption>();


            if (btnInst_scr != null)
            {

                btnInst_scr.SetupButton(new ButtonData(allButtonFunctions, nameTxt));
            }
            else
            {
                ExtendedFunctionality.SafeDestroy(btnInst);
            }

        }
    }


    public virtual void FullSetup(string titleTxt , string notifTxt)
    {

        if (FindResources() && FindDependencies())
        {
            SetupNotification(titleTxt,notifTxt);
            CreateAllButtons();
            PositionAllButtons();

        }


    }



    protected abstract void CreateAllButtons();

    void PositionAllButtons()
    {

        if (FindResources() && FindDependencies())
        {
            (float cntWidth, float cntHeight) cntSize = (selfRect.sizeDelta.x, selfRect.sizeDelta.y);

            float divideFactor_btnRaiseFromBottom = 10.75784190715182f;
            float divideFactor_btnWidth = 3.897333333333333f;
            float divideFactor_btnHeight = 9.082627118644068f;

            float divideFactor_padding = 58.46f;

            float btn_xPos_padding = (float)cntSize.cntWidth / (float)divideFactor_padding;

            for (int i = 0; i < btn_uiOption_insts.Count; i++)
            {
                GameObject btn_current = btn_uiOption_insts[i];
                if (btn_current != null)
                {
                    RectTransform btnRect = btn_current.GetComponent<RectTransform>();
                    if (btnRect != null)
                    {
                        float btn_yPos = (float)cntSize.cntHeight / (float)divideFactor_btnRaiseFromBottom;
                        float btn_width = ((float)cntSize.cntWidth / (float)divideFactor_btnWidth)*btnWidthMultiplier;
                        float btn_height = (float)cntSize.cntHeight / (float)divideFactor_btnHeight;

                        float btn_xPos_range = Mathf.Abs((btn_uiOption_insts.Count - 1) * ((float)(btn_width + btn_xPos_padding) / (float)2));

                        float btn_xPos_lerpProgress = Mathf.InverseLerp(0, btn_uiOption_insts.Count - 1, i);

                        float btn_xPos = Mathf.Lerp(-btn_xPos_range, btn_xPos_range, btn_xPos_lerpProgress);

                        ExtendedFunctionality_UI.SetupRectTransform(btnRect,
                        new Vector3(btn_xPos, btn_yPos, 0),
                        new Vector2(btn_width, btn_height),
                        new Vector2(0.5f, 0f),
                        new Vector2(0.5f, 0f),
                        new Vector2(0.5f, 0.5f),
                        Vector3.zero,
                        Vector3.one);
                    }
                    else
                    {
                        ExtendedFunctionality.SafeDestroy(btn_current);
                    }
                }

            }
        }



    }



    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (btn_uiOption_prefab == null)
        {
            btn_uiOption_prefab = Resources.Load<GameObject>("UI/btn_uiOption");
        }


        if (btn_uiOption_prefab != null && parRet)
        {
            ret = true;
        }

        

        return ret;
    }

    protected bool FindDependencies()
    {
        bool ret = false;

        if (selfRect == null)
        {
            selfRect = gameObject.GetComponent<RectTransform>();
        }

        if (txt_title == null)
        {
            txt_title = ExtendedFunctionality.GetComponentInChildrenByName<Text>(this.gameObject, "txt_notification_title");
        }

        if (txt_notif == null)
        {
            txt_notif = ExtendedFunctionality.GetComponentInChildrenByName<Text>(this.gameObject, "txt_notification_data");
        }

        if (selfRect != null && txt_title != null && txt_notif!=null)
        {
            ret = true;
        }

        return ret;
    }
}
