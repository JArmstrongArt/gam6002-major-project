using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CNV_100Percent : UIElemBase
{
    private GameObject sE_100Percent;
    protected override void FullSetup()
    {
        base.FullSetup();

        if (FindResources())
        {
            SpawnDecor(E_BGDecor.Background, "You got 100% completion!");
            CreateBackButton(E_BackButtonAlignment.BR, "MainMenu");
            MusicManager.PlaySong(E_TrackList.MainMenu);
            Instantiate(sE_100Percent);
        }


    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet = base.FindResources();

        if (sE_100Percent == null)
        {
            sE_100Percent = Resources.Load<GameObject>("Sound Effects/UI/sE_100Percent/sE_100Percent");
        }

        if (sE_100Percent != null)
        {
            ret = true;
        }

        return ret;
    }

}
