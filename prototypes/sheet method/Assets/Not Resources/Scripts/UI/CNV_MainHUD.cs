using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CNV_MainHUD : UIElemBase
{
    private GameObject cnt_gameplayTracker_prefab;
    [SerializeField] float trackerPadding = 15.0f;

    public Player_Statistics StatScr { set; private get; }

    private List<CNT_GameplayTracker> allTrackers = new List<CNT_GameplayTracker>();


    bool fullSetupDelay = false;

    [SerializeField] int HUDUpdateInterval = 2;
    private int HUDUpdateInterval_current = 0;


    protected override void FullSetup()
    {
        if (FindResources())
        {
            base.FullSetup();

            allTrackers.Clear();

            if (StatScr != null)
            {
                int trackerIndex = 0;
                if (StatScr.Score_Max > 0)
                {
                    CreateStatTracker(trackerIndex, typeof(CNT_GameplayTracker_IntField), StatScr, "score", false, "score_max", false, "UI/Gameplay/cnv_mainHUD/scoreIcon");
                    trackerIndex += 1;
                }

                if (StatScr.EnemyKillCount_Max > 0)
                {
                    CreateStatTracker(trackerIndex, typeof(CNT_GameplayTracker_IntField), StatScr, "enemyKillCount", true, "enemyKillCount_max", false, "UI/Gameplay/cnv_mainHUD/enemyKillCounterIcon");
                    trackerIndex += 1;
                }
                CreateStatTracker(trackerIndex, typeof(CNT_GameplayTracker_IntField), StatScr, "deathCount", true, null, false, "UI/Gameplay/cnv_mainHUD/deathCounterIcon");
            }

        }
    }

    private void Update()
    {
        if (FindResources())
        {


            if (fullSetupDelay == false)
            {
                if (StatScr != null)
                {
                    FullSetup();
                    fullSetupDelay = true;
                }
            }
            else
            {

                if(HUDUpdateInterval_current+1>= HUDUpdateInterval)
                {
                    foreach (CNT_GameplayTracker tracker in allTrackers)
                    {
                        tracker.UpdateStat();
                    }
                    HUDUpdateInterval_current = 0;
                }
                else
                {
                    HUDUpdateInterval_current += 1;
                }

            }


        }
    }


    void CreateStatTracker(int heightPaddingIndex,System.Type gameplayTrackerToAdd, object statSrc, string statSrc_fieldName, bool statIsPublic, string? statSrc_fieldName_outOf, bool statIsPublic_outOf, string spriteResourcePath)
    {

        if (FindResources())
        {
            trackerPadding = Mathf.Abs(trackerPadding);

            GameObject statTrackerInst = SpawnObjectUnderThisElement(cnt_gameplayTracker_prefab);


            Component statTracker_scr = statTrackerInst.AddComponent(gameplayTrackerToAdd);

            CNT_GameplayTracker statTracker_scr_typeSafe = (CNT_GameplayTracker)statTracker_scr;




            if (statTracker_scr_typeSafe != null)
            {
                statTracker_scr_typeSafe.statSource = statSrc;
                statTracker_scr_typeSafe.statSource_statFieldName = statSrc_fieldName;
                statTracker_scr_typeSafe.statSource_statFieldName_outOf = statSrc_fieldName_outOf;
                statTracker_scr_typeSafe.statIsPublic = statIsPublic;
                statTracker_scr_typeSafe.statIsPublic_outOf = statIsPublic_outOf;
                statTracker_scr_typeSafe.IconLocation = spriteResourcePath;
                allTrackers.Add(statTracker_scr_typeSafe);


                RectTransform statTrackerInst_rect = statTrackerInst.GetComponent<RectTransform>();

                if (statTrackerInst_rect != null)
                {
                    float trackerHeight = statTrackerInst_rect.sizeDelta.y;


                    ExtendedFunctionality_UI.SetupRectTransform(
                        statTrackerInst_rect,
                        new Vector2(trackerPadding, -trackerPadding + -((trackerPadding + trackerHeight) * heightPaddingIndex)),
                        statTrackerInst_rect.sizeDelta,
                        new Vector2(0, 1),
                        new Vector2(0, 1),
                        new Vector2(0, 1),
                        Vector3.zero,
                        Vector3.one


                    );
                }
            }
            else
            {
                ExtendedFunctionality.SafeDestroy(statTrackerInst);
            }


        }

    }

    protected override bool FindResources()
    {
        bool parRet= base.FindResources();

        bool ret = false;

        if (cnt_gameplayTracker_prefab == null)
        {
            cnt_gameplayTracker_prefab = Resources.Load<GameObject>("UI/Gameplay/cnv_mainHUD/cnt_gameplayTracker");
        }

        if (parRet && cnt_gameplayTracker_prefab != null)
        {
            ret = true;
        }

        return ret;
    }


}
