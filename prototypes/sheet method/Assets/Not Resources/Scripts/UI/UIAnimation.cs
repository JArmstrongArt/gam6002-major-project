using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum E_PlayCondition
{
    Manual,
    ManualInterrupt,
    Loop,
    Timer
}

public enum E_AnimationTimeScale
{
    Scaled,
    Unscaled
}

[RequireComponent(typeof(UIAnimationCombiner))]

public abstract class UIAnimation : MonoBehaviour
{
    protected UIAnimationCombiner combinerScr;

    [SerializeField] E_PlayCondition playCondition = E_PlayCondition.Manual;
    [SerializeField] E_AnimationTimeScale timeScale = E_AnimationTimeScale.Unscaled;



    [SerializeField] float playCondition_timer_timeLength = 3.0f;
    private float playCondition_timer_timeLength_current=0.0f;
    protected bool animationInProgress = false;

    private bool firstInitPassed = false;

    public Vector3 AppliedAnchoredPos { get; protected set; }
    public Vector3 AppliedEuler { get; protected set; }
    public Vector3 AppliedScale { get; protected set; }



    protected float GetMyTimeScale()
    {
        switch (timeScale)
        {
            case E_AnimationTimeScale.Scaled:
            default:
                return Time.deltaTime;
                break;
            case E_AnimationTimeScale.Unscaled:
                return Time.unscaledDeltaTime;

                break;

        }
    }

    private void Awake()
    {
        if (FindDependencies())
        {

            CancelAnimationEffectsOnObject();
        }
    }


    private void Update()
    {
        if (FindDependencies())
        {
            switch (playCondition)
            {
                case E_PlayCondition.Timer:
                    if (animationInProgress)
                    {
                        playCondition_timer_timeLength_current = 0.0f;
                    }
                    else
                    {

                        playCondition_timer_timeLength_current += GetMyTimeScale();


                        if (playCondition_timer_timeLength_current >= playCondition_timer_timeLength)
                        {
                            PlayAnimation();
                        }
                    }
                    break;
                case E_PlayCondition.Loop:
                    if (!animationInProgress)
                    {
                        PlayAnimation();
                    }
                    break;
            }

            if (animationInProgress)
            {
                Animate();
            }
        }

    }

    protected void PlayAnimation()
    {
        if (FindDependencies())
        {
            bool interruptMode = false;
            switch (playCondition)
            {
                case E_PlayCondition.ManualInterrupt:
                case E_PlayCondition.Loop:
                case E_PlayCondition.Timer:
                    interruptMode = true;
                    break;

            }

            if (interruptMode || (!interruptMode && animationInProgress == false))
            {

                InitAnimation(!firstInitPassed);
                firstInitPassed = true;
                animationInProgress = true;
            }
        }

    }

    public void PlayAnimation_ManualOnly()
    {
        if (FindDependencies())
        {
            switch (playCondition)
            {
                case E_PlayCondition.Manual:
                case E_PlayCondition.ManualInterrupt:
                    PlayAnimation();
                    break;
            }
        }

    }

    protected abstract void InitAnimation(bool firstInitMode);

    public void CancelAnimationEffectsOnObject()
    {
        if (FindDependencies())
        {
            AppliedAnchoredPos = combinerScr.OrigAnchoredPos;
            AppliedEuler = combinerScr.OrigEulerAngles;
            AppliedScale = combinerScr.OrigScale;

            firstInitPassed = false;
            animationInProgress = false;
        }

    }

    private void OnDestroy()
    {
        if (FindDependencies())
        {
            CancelAnimationEffectsOnObject();

        }
    }

    protected abstract void Animate();

    protected bool FindDependencies()
    {
        bool ret = false;

        if (combinerScr == null)
        {
            combinerScr = gameObject.GetComponent<UIAnimationCombiner>();
        }



        if (combinerScr != null )
        {
            ret = true;
        }

        return ret;
    }
}
