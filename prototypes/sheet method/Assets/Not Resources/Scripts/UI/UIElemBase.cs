using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SimpleFileBrowser;
public enum E_BGDecor
{
    Background,
    Border
}

public enum E_BackButtonAlignment
{
    TL,
    TR,
    BL,
    BR
}

public abstract class UIElemBase : MonoBehaviour
{
    protected List<GameObject> instsISpawned = new List<GameObject>();


    private GameObject menuBackground_prefab;
    private GameObject menuBorder_prefab;

    private GameObject btn_uiOption_iconVariant_prefab;

    private GameObject btn_uiOption_iconVariant_inst;

    private Sprite spr_backButton;
    private void Awake()
    {
        if (FindResources())
        {
            FullSetup();

        }
    }

    protected void CreateBackButton(E_BackButtonAlignment backButtonLocation, string warpScene, E_SaveFileType? changeGenType=null, E_LevelFolderToLoad? changeLevelFolder=null, float btnSize=55)
    {
        if (FindResources())
        {
            ExtendedFunctionality.SafeDestroy(btn_uiOption_iconVariant_inst);

            btn_uiOption_iconVariant_inst = SpawnObjectUnderThisElement(btn_uiOption_iconVariant_prefab);

            BTN_UIOption_IconVariant btnInst_scr = btn_uiOption_iconVariant_inst.GetComponent<BTN_UIOption_IconVariant>();

            RectTransform btnRect = btn_uiOption_iconVariant_inst.GetComponent<RectTransform>();


            if (btnInst_scr != null && btnRect != null && btnInst_scr.MyRect_SizeDelta!=null)
            {

                List<UnityEngine.Events.UnityAction> btnFuncs = new List<UnityEngine.Events.UnityAction>();
                btnFuncs.Add(delegate { FileBrowser.HideDialog(false); });

                btnFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene(warpScene, changeGenType ?? SheetMethod.currentGenerateType, changeLevelFolder??CNV_LevelSelect.folderToLoad); });


                btnInst_scr.SetupButton(new ButtonData(btnFuncs, null, spr_backButton), new Vector2(btnSize, btnSize));

                Vector2 sizeDelta_btnInst = btnInst_scr.MyRect_SizeDelta ?? default(Vector2);

                Vector2 anchorMinMaxAndPivot = Vector2.zero;

                switch (backButtonLocation)
                {
                    case E_BackButtonAlignment.BL:
                        anchorMinMaxAndPivot = new Vector2(0, 0);

                        break;

                    case E_BackButtonAlignment.BR:
                        anchorMinMaxAndPivot = new Vector2(1, 0);
                        break;

                    case E_BackButtonAlignment.TL:
                        anchorMinMaxAndPivot = new Vector2(0, 1);

                        break;

                    case E_BackButtonAlignment.TR:
                        anchorMinMaxAndPivot = new Vector2(1, 1);

                        break;
                }
                


                ExtendedFunctionality_UI.SetupRectTransform(btnRect,
                Vector3.zero,
                sizeDelta_btnInst,
                anchorMinMaxAndPivot,
                anchorMinMaxAndPivot,
                anchorMinMaxAndPivot,
                Vector3.zero,
                Vector3.one);
            }
            else
            {
                ExtendedFunctionality.SafeDestroy(btn_uiOption_iconVariant_inst);
            }







        }
    }


    protected void ReadjustAttachedUIAnimations(GameObject subject)
    {
        if (FindResources())
        {
            UIAnimationCombiner combineScr = subject.GetComponent<UIAnimationCombiner>();

            UIAnimation[] animScr = subject.GetComponents<UIAnimation>();

            if (combineScr != null)
            {


                combineScr.SetOrigsFromCurrent();

                for (int j = 0; j < animScr.Length; j++)
                {
                    animScr[j].CancelAnimationEffectsOnObject();
                }
            }
        }

    }

    void DestroyAllInstsISpawned()
    {
        ExtendedFunctionality.SafeDestroy(instsISpawned);

    }

    protected virtual void FullSetup()
    {
        DestroyAllInstsISpawned();
    }

    private void OnDestroy()
    {
        DestroyAllInstsISpawned();

    }

    protected GameObject SpawnObjectUnderThisElement(GameObject spawnObj,string? childName=null,int? siblingIndex=null)
    {
        if (FindResources())
        {
            Transform par = this.transform;

            if (childName != null)
            {
                string childName_nonNullable = childName ?? default(string);


                Transform parInChildren = ExtendedFunctionality.GetComponentInChildrenByName<Transform>(this.gameObject, childName_nonNullable);
                if (parInChildren != null)
                {
                    par = parInChildren;
                }
            }

            GameObject inst = Instantiate(spawnObj, Vector3.zero, Quaternion.Euler(Vector3.zero), par);

            instsISpawned.Add(inst);

            if (siblingIndex != null)
            {
                int siblingIndex_nonNull = siblingIndex ?? 0;

                inst.transform.SetSiblingIndex(siblingIndex_nonNull);
            }

            return inst;
        }
        else
        {
            return null;
        }

    }

    protected GameObject SpawnDecor(E_BGDecor decorType,string decorTxt_1="DECORTXT1")
    {
        if (FindResources())
        {
            GameObject ret = null;
            switch (decorType)
            {
                case E_BGDecor.Background:
                    ret=SpawnObjectUnderThisElement(menuBackground_prefab,null,0);

                    CNT_MenuBackground menuBG_scr = ret.GetComponent<CNT_MenuBackground>();

                    if (menuBG_scr != null)
                    {
                        menuBG_scr.SetupMenuBG(decorTxt_1);
                    }

                    break;

                case E_BGDecor.Border:
                    ret = SpawnObjectUnderThisElement(menuBorder_prefab,null,0);
                    break;
            }

            if (ret != null)
            {
                RectTransform retRect = ret.GetComponent<RectTransform>();

                if (retRect != null)
                {
                    retRect.anchoredPosition = Vector2.zero;
                }
            }

            return ret;
        }
        else
        {
            return null;
        }
    }

    protected virtual bool FindResources()
    {
        bool ret = false;

        if (menuBackground_prefab == null)
        {
            menuBackground_prefab = Resources.Load<GameObject>("UI/cnt_menuBackground");
        }

        if (menuBorder_prefab == null)
        {
            menuBorder_prefab = Resources.Load<GameObject>("UI/cnt_menuBorder");
        }

        if (btn_uiOption_iconVariant_prefab == null)
        {
            btn_uiOption_iconVariant_prefab = Resources.Load<GameObject>("UI/btn_uiOption_iconVariant");
        }

        if (spr_backButton == null)
        {
            spr_backButton = Resources.Load<Sprite>("UI/spr_backButton");
        }

        if (spr_backButton != null && btn_uiOption_iconVariant_prefab != null && menuBorder_prefab != null && menuBackground_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
