using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class BTN_UIOption : BTN
{
    private Text txt_name;


    public override void SetupButton(ButtonData btnData, Vector2? btnWidthOverride = null)
    {
        if (FindDependencies() && base.FindResources())
        {
            base.SetupButton(btnData,btnWidthOverride);

            txt_name.text = btnData.ButtonTxt.ToUpper()??txt_name.text;
            txt_name.fontSize = btnData.FontSizeOverride ?? txt_name.fontSize;

        }

    }


    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet = base.FindDependencies();



        if (txt_name == null)
        {
            txt_name = gameObject.GetComponentInChildren<Text>();
        }



        if (txt_name != null && parRet)
        {
            ret = true;
        }

        return ret;
    }
}
