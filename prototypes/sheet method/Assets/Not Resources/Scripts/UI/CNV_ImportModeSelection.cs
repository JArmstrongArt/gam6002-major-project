using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CNV_ImportModeSelection : UIElemBase
{
    private GameObject cnt_importModeOption_prefab;

    private Sprite spr_icon_simple;
    private Sprite spr_icon_advanced;

    private List<GameObject> spawnedImportModeOptions = new List<GameObject>();



    protected override void FullSetup()
    {
        base.FullSetup();


        if (FindResources())
        {
            SpawnDecor(E_BGDecor.Background, "Import Mode");
            CreateBackButton(E_BackButtonAlignment.BR, "MainMenu", E_SaveFileType.Creation);
            MusicManager.PlaySong(E_TrackList.MainMenu);

            CreateImportModeOption("SIMPLE MODE", "IMPORT AN IMAGE, GET A LEVEL!", ".JPG, .PNG, ETC...", spr_icon_simple, - 1, new UnityEngine.Events.UnityAction(delegate { BTN_DelegateFunctions_FileBrowser.OpenFileBrowser_LPSimple(E_FileBrowserSuccessType.WithWarp); }));
            CreateImportModeOption("ADVANCED MODE", "IMPORT A FOLDER OF IMAGES, GET MORE CONTROL!", "FOLDERS ONLY", spr_icon_advanced, 1, new UnityEngine.Events.UnityAction(delegate { BTN_DelegateFunctions_FileBrowser.OpenFileBrowser_LPAdvanced(E_FileBrowserSuccessType.WithWarp); }));

            PositionAllImportModeOptions();

        }
    }



    void PositionAllImportModeOptions()
    {
        if (FindResources())
        {
            for (int i = 0; i < spawnedImportModeOptions.Count; i++)
            {
                GameObject subject = spawnedImportModeOptions[i];
                if (subject != null)
                {
                    RectTransform optionRect = subject.GetComponent<RectTransform>();

                    if (optionRect != null)
                    {
                        float halfSize = (float)optionRect.sizeDelta.x / (float)2;


                        float xPosMultiple_invLerp = Mathf.InverseLerp(0, spawnedImportModeOptions.Count - 1, i);

                        float xPosMultiple = Mathf.Lerp(-1, 1, xPosMultiple_invLerp);


                        float xPos = (Mathf.Abs(halfSize) + Mathf.Abs((float)halfSize / (float)20)) * xPosMultiple;

                        ExtendedFunctionality_UI.SetupRectTransform(optionRect,
                            new Vector3(xPos, 0, 0),
                            null,
                            new Vector2(0.5f, 0.5f),
                            new Vector2(0.5f, 0.5f),
                            new Vector2(0.5f, 0.5f),
                            Vector3.zero,
                            Vector3.one);
                    }
                    else
                    {
                        ExtendedFunctionality.SafeDestroy(subject);
                    }


                }

                ReadjustAttachedUIAnimations(subject);
            }
        }


    }

    void CreateImportModeOption(string title, string description, string fileTypes, Sprite icon, int xPosMultiple=1, UnityEngine.Events.UnityAction optionFunc=null)
    {
        if (FindResources())
        {
            GameObject optionInst = SpawnObjectUnderThisElement(cnt_importModeOption_prefab);
            CNT_ImportModeOption optionScr = optionInst.GetComponent<CNT_ImportModeOption>();
            RectTransform optionRect = optionInst.GetComponent<RectTransform>();

            BTN optionBtn = optionInst.GetComponent<BTN>();

            if (optionScr != null && optionRect != null && optionBtn != null)
            {

                spawnedImportModeOptions.Add(optionInst);




                optionScr.SetupContainer(title, description, fileTypes, icon);

                if (optionFunc != null)
                {
                    List<UnityEngine.Events.UnityAction> optionFuncs = new List<UnityEngine.Events.UnityAction>();
                    optionFuncs.Add(optionFunc);

                    optionBtn.SetupButton(new ButtonData(optionFuncs));

                }







            }
            else
            {
                ExtendedFunctionality.SafeDestroy(optionInst);
            }
        }

    }


    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (cnt_importModeOption_prefab == null)
        {
            cnt_importModeOption_prefab = Resources.Load<GameObject>("UI/cnv_importModeSelection/cnt_importModeOption");
        }

        if (spr_icon_simple == null)
        {
            spr_icon_simple = Resources.Load<Sprite>("UI/cnv_importModeSelection/spr_icon_simple");
        }

        if (spr_icon_advanced == null)
        {
            spr_icon_advanced = Resources.Load<Sprite>("UI/cnv_importModeSelection/spr_icon_advanced");
        }

        if (parRet && cnt_importModeOption_prefab != null && spr_icon_simple!=null && spr_icon_advanced!=null)
        {
            ret = true;
        }

        return ret;
    }
}
