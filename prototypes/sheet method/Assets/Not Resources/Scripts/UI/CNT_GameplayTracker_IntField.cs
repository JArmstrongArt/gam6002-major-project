using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CNT_GameplayTracker_IntField : CNT_GameplayTracker
{
    //this is nullable because i want the first update stat call to not trigger the animation
    private int? intVal_prev;

    public override void UpdateStat()
    {
        if (base.FindDependencies() && base.FindResources())
        {
            base.UpdateStat();

            int intVal = ExtendedFunctionality.GetFieldFromClassByName<int>(statSource, statSource_statFieldName,statIsPublic);

            int? intVal_outOf = null;

            if (statSource_statFieldName_outOf != null)
            {
                intVal_outOf = ExtendedFunctionality.GetFieldFromClassByName<int>(statSource, statSource_statFieldName_outOf, statIsPublic_outOf);
            }

            if (intVal_prev != null &&intVal>intVal_prev)
            {
                int intVal_prev_nonNull = intVal_prev ?? default(int);

                if(intVal > intVal_prev_nonNull)
                {
                    pulseAnimScr.PlayAnimation_ManualOnly();

                }

            }


            intVal_prev = intVal;

            if (intVal_outOf != null)
            {
                txt_stat.text = intVal.ToString()+"/"+(intVal_outOf??0).ToString();

            }
            else
            {
                txt_stat.text = intVal.ToString();

            }

        }

    }
}
