using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.EventSystems;
public class CNT_LevelSelectEntry : UIElemBase, IPointerEnterHandler, IPointerExitHandler
{
    public LevelFile levelILoad { set; private get; }

    public LevelProgress levelProgress { set; private get; }

    private Text txt_levelName;

    private BTN btn_playLevel;

    private Image img_criteria_levelBeat;
    private Image img_criteria_allCollectables;
    private Image img_criteria_allEnemies;
    private Image img_criteria_noDeaths;

    private Text txt_levelPercentComplete;


    private UIAnimation animScr;

    private Image bgImg;

    protected override void FullSetup()
    {
        base.FullSetup();

        if (base.FindResources() && FindDependencies())
        {
            if (levelILoad != null && levelProgress != null)
            {
                bgImg.color = ExtendedFunctionality.RainbowRandomThisColour(bgImg.color);

                txt_levelName.text = levelILoad.levelName;

                List<UnityEngine.Events.UnityAction> playFuncs = new List<UnityEngine.Events.UnityAction>();
                playFuncs.Add(delegate { BTN_DelegateFunctions.CopyLevelFileThenGoToScene(levelILoad, E_SaveFileType.Gameplay, "test"); });

                btn_playLevel.SetupButton(new ButtonData(playFuncs));

                txt_levelPercentComplete.text = ExtendedFunctionality.ZeroOneValueAsPercent( levelProgress.GetCompletionProgress());


                if (levelProgress.HasBeenAchieved(E_LevelProgress_CriteriaType.LevelCleared))
                {
                    img_criteria_levelBeat.color = Color.white;

                }
                else
                {
                    img_criteria_levelBeat.color = Color.black;
                    img_criteria_levelBeat.color = new Color(img_criteria_levelBeat.color.r, img_criteria_levelBeat.color.g, img_criteria_levelBeat.color.b, 0.5f);

                }

                if (levelProgress.HasBeenAchieved(E_LevelProgress_CriteriaType.AllCollectables))
                {
                    img_criteria_allCollectables.color = Color.white;
                }
                else
                {
                    img_criteria_allCollectables.color = Color.black;
                    img_criteria_allCollectables.color = new Color(img_criteria_allCollectables.color.r, img_criteria_allCollectables.color.g, img_criteria_allCollectables.color.b, 0.5f);


                }

                if (levelProgress.HasBeenAchieved(E_LevelProgress_CriteriaType.AllEnemies))
                {
                    img_criteria_allEnemies.color = Color.white;
                }
                else
                {
                    img_criteria_allEnemies.color = Color.black;
                    img_criteria_allEnemies.color = new Color(img_criteria_allEnemies.color.r, img_criteria_allEnemies.color.g, img_criteria_allEnemies.color.b, 0.5f);


                }

                if (levelProgress.HasBeenAchieved(E_LevelProgress_CriteriaType.NoDeaths))
                {
                    img_criteria_noDeaths.color = Color.white;
                }
                else
                {
                    img_criteria_noDeaths.color = Color.black;
                    img_criteria_noDeaths.color = new Color(img_criteria_noDeaths.color.r, img_criteria_noDeaths.color.g, img_criteria_noDeaths.color.b, 0.5f);


                }


            }
        }


    }

    public void FullSetup_PublicCall()
    {
        if(base.FindResources() && FindDependencies())
        {
            FullSetup();

        }
    }

    bool FindDependencies()
    {

        bool ret = false;


        if (txt_levelName == null)
        {
            txt_levelName = ExtendedFunctionality.GetComponentInChildrenByName<Text>(gameObject, "txt_levelname");
        }

        if (btn_playLevel == null)
        {
            btn_playLevel = gameObject.GetComponent<BTN>();
        }

        if (img_criteria_levelBeat == null)
        {
            img_criteria_levelBeat = ExtendedFunctionality.GetComponentInChildrenByName<Image>(gameObject, "img_criteria_levelBeat");
        }

        if (img_criteria_allCollectables == null)
        {
            img_criteria_allCollectables = ExtendedFunctionality.GetComponentInChildrenByName<Image>(gameObject, "img_criteria_allCollectables");
        }

        if (img_criteria_allEnemies == null)
        {
            img_criteria_allEnemies = ExtendedFunctionality.GetComponentInChildrenByName<Image>(gameObject, "img_criteria_allEnemies");
        }

        if (img_criteria_noDeaths == null)
        {
            img_criteria_noDeaths = ExtendedFunctionality.GetComponentInChildrenByName<Image>(gameObject, "img_criteria_noDeaths");
        }

        if (txt_levelPercentComplete == null)
        {
            txt_levelPercentComplete = ExtendedFunctionality.GetComponentInChildrenByName<Text>(gameObject, "txt_levelPercentComplete");

        }

        if (bgImg == null)
        {
            bgImg = gameObject.GetComponent<Image>();
        }

        if (animScr == null)
        {
            animScr = gameObject.GetComponent<UIAnimation>();
        }


        if (bgImg!=null && animScr != null && txt_levelPercentComplete != null && img_criteria_levelBeat != null && img_criteria_allCollectables != null && img_criteria_allEnemies != null && img_criteria_noDeaths != null && btn_playLevel != null && txt_levelName != null)
        {
            ret = true;
        }

        return ret;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(FindDependencies() && base.FindResources())
        {
            animScr.PlayAnimation_ManualOnly();

        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (FindDependencies() && base.FindResources())
        {
            animScr.CancelAnimationEffectsOnObject();

        }

    }
}
