using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CNV_LevelImportPreview : UIElemBase
{
    private GameObject cnt_levelPropertyModifier_prefab;

    //used when positioning each modifier, dont need to safedestroy this list as all the insts will also be in instsISpawned.
    private List<GameObject> propertyModifierInsts = new List<GameObject>();

    private RectTransform selfRect;

    [SerializeField] float hotbarHeight;

    private GameObject btn_uiOption_iconVariant_prefab;

    private Sprite testLvlIcon;

    private Sprite saveLvlIcon;

    private InputField inp_levelName;

    void ShiftThemeButton(int shiftAmt)
    {
        if (FindResources() && FindDependencies())
        {
            SheetMethod.LoadedLevelData.levelThemeType = LevelThemeCollection.ShiftToNextTypeInDictionary(SheetMethod.LoadedLevelData.levelThemeType, shiftAmt);

            SheetMethod.FullExecute();
        }

    }

    void ShiftMusicButton(int shiftAmt)
    {
        if(FindResources() && FindDependencies())
        {
            SheetMethod.LoadedLevelData.levelMusicType = MusicManager.ShiftTrackType(SheetMethod.LoadedLevelData.levelMusicType, shiftAmt);

            SheetMethod.FullExecute();
        }

    }


    void TestButton()
    {
        if(FindResources() && FindDependencies())
        {
            if (SheetMethod.LevelPassesEntityCheckForSavingToFile())
            {
                BTN_DelegateFunctions.CopyLevelFileThenGoToScene(E_SaveFileType.Creation, E_SaveFileType.Testing, "test");
            }
        }
    }
    void SaveButton()
    {
        if(FindResources() && FindDependencies())
        {
            if (inp_levelName.text.Replace(" ", "") != "" && SheetMethod.LevelPassesEntityCheckForSavingToFile())
            {
                LevelFileManager.SaveLevelFileFromLoadedLevelFiles(E_SaveFileType.Creation, inp_levelName.text);

                BTN_DelegateFunctions.GoToScene("LevelSelect", E_SaveFileType.Gameplay,E_LevelFolderToLoad.Custom);


                //old 'instant refresh' system
                //LevelFileManager.LoadLevelFile_FromSavedLevels(E_SaveFileType.Creation, inp_levelName.text);


                //SheetMethod.FullExecute();
            }



        }
    }

    protected override void FullSetup()
    {
        base.FullSetup();

        if (FindResources() && FindDependencies())
        {
            SpawnDecor(E_BGDecor.Border);
            CreateBackButton(E_BackButtonAlignment.TL, "ImportModeSelection", E_SaveFileType.Creation);

            //create property modifiers in order from left to right

            List<UnityEngine.Events.UnityAction> levelPlan_left_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate { BTN_DelegateFunctions_FileBrowser.OpenFileBrowser_LPSimple(E_FileBrowserSuccessType.LiveGenerate); }
            };

            List<UnityEngine.Events.UnityAction> levelPlan_right_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate { BTN_DelegateFunctions_FileBrowser.OpenFileBrowser_LPAdvanced(E_FileBrowserSuccessType.LiveGenerate); }
            };

            List<UnityEngine.Events.UnityAction> heightmap_right_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate{BTN_DelegateFunctions_FileBrowser.OpenFileBrowser_Heightmap(E_FileBrowserSuccessType.LiveGenerate); }
            };

            List<UnityEngine.Events.UnityAction> heightmap_left_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate{BTN_DelegateFunctions.RemoveHeightmapAndLiveRegen(); }
            };



            List<UnityEngine.Events.UnityAction> shiftTheme_right_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate{ShiftThemeButton(1); }
            };

            List<UnityEngine.Events.UnityAction> shiftTheme_left_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate{ShiftThemeButton(-1); }
            };


            List<UnityEngine.Events.UnityAction> shiftMusic_right_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate{ShiftMusicButton(1); }
            };

            List<UnityEngine.Events.UnityAction> shiftMusic_left_actions = new List<UnityEngine.Events.UnityAction>
            {
                delegate{ShiftMusicButton(-1); }
            };


            CreatePropertyModifier(
                "LEVEL PLAN",
                levelPlan_left_actions, 
                "FILE",
                levelPlan_right_actions,
                "FOLDER",
                12);


            CreatePropertyModifier(
                "HEIGHTMAP",
                heightmap_left_actions,
                "-",
                heightmap_right_actions,
                "+");

            CreatePropertyModifier(
                "THEME",
                shiftTheme_left_actions,
                "<",
                shiftTheme_right_actions,
                ">");

            CreatePropertyModifier(
                "MUSIC",
                shiftMusic_left_actions,
                "<",
                shiftMusic_right_actions,
                ">");



            PositionAllPropertyModifiers();





            List<UnityEngine.Events.UnityAction> saveBtnFuncs = new List<UnityEngine.Events.UnityAction>
                {
                    delegate{ SaveButton(); }
                };



            List<UnityEngine.Events.UnityAction> testBtnFuncs = new List<UnityEngine.Events.UnityAction>
                {
                    delegate{TestButton(); }
                };

            float saveBtnWidth = SetupARightHandButton(saveBtnFuncs, saveLvlIcon);
            SetupARightHandButton(testBtnFuncs, testLvlIcon,saveBtnWidth);


        }
    }

    float SetupARightHandButton(List<UnityEngine.Events.UnityAction> btnFuncs, Sprite btnIcon, float widthOffset=0)
    {
        float ret = 0;
        if (FindResources() && FindDependencies())
        {
            GameObject btnInst = SpawnObjectUnderThisElement(btn_uiOption_iconVariant_prefab);

            BTN_UIOption_IconVariant btnInst_scr = btnInst.GetComponent<BTN_UIOption_IconVariant>();

            RectTransform btnRect = btnInst.GetComponent<RectTransform>();


            if (btnInst_scr != null && btnRect != null)
            {





                btnInst_scr.SetupButton(new ButtonData(btnFuncs, null, btnIcon));



                ExtendedFunctionality_UI.SetupRectTransform(btnRect,
                new Vector3(-Mathf.Abs(widthOffset),0,0),
                new Vector2(hotbarHeight, hotbarHeight),
                new Vector2(1, 0f),
                new Vector2(1, 0f),
                new Vector2(1, 0f),
                Vector3.zero,
                Vector3.one);
            }
            else
            {
                ExtendedFunctionality.SafeDestroy(btnInst);
            }


            ret = btnRect.sizeDelta.x;





        }
        return ret;
    }








    void PositionAllPropertyModifiers()
    {
        if (FindResources() && FindDependencies())
        {

            for (int i = 0; i < propertyModifierInsts.Count; i++)
            {
                GameObject curObj = propertyModifierInsts[i];

                if (curObj != null)
                {
                    RectTransform curRect = curObj.GetComponent<RectTransform>();

                    if (curRect != null)
                    {
                        float containerWidth = curRect.sizeDelta.x;


                        //position and size r set weird here because it horizontally stretches the screen
                        ExtendedFunctionality_UI.SetupRectTransform(curRect,
                            new Vector3(containerWidth*i, 0,0),
                            new Vector2(containerWidth, hotbarHeight),
                            Vector2.zero,
                            Vector2.zero,
                            Vector2.zero,
                            Vector3.zero,
                            Vector3.one);
                    }
                    else
                    {
                        ExtendedFunctionality.SafeDestroy(curObj);
                    }
                }




            }
        }

    }

    void CreatePropertyModifier(string? titleTxt = null, List<UnityEngine.Events.UnityAction> allButtonFunctions_left = null, string? buttonTitleText_left = null, List<UnityEngine.Events.UnityAction> allButtonFunctions_right = null, string? buttonTitleText_right = null, int? fontSizeOverride=null)
    {
        if (FindResources() && FindDependencies())
        {
            GameObject pM_inst = SpawnObjectUnderThisElement(cnt_levelPropertyModifier_prefab);

            propertyModifierInsts.Add(pM_inst);

            CNT_LevelPropertyModifier lPM_scr = pM_inst.GetComponent<CNT_LevelPropertyModifier>();

            if (lPM_scr != null)
            {
                lPM_scr.SetupPropertyModifier(titleTxt, allButtonFunctions_left, buttonTitleText_left,allButtonFunctions_right,buttonTitleText_right, fontSizeOverride); 
            }
            else
            {
                ExtendedFunctionality.SafeDestroy(pM_inst);
            }
        }




    }

    bool FindDependencies()
    {
        bool ret = false;

        if (selfRect == null)
        {
            selfRect = gameObject.GetComponent<RectTransform>();
        }

        if(inp_levelName == null)
        {
            inp_levelName = ExtendedFunctionality.GetComponentInChildrenByName<InputField>(gameObject, "inp_levelName");
        }

        if (selfRect != null && inp_levelName!=null)
        {
            ret = true;
        }
        return ret;

    }


    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (cnt_levelPropertyModifier_prefab == null)
        {
            cnt_levelPropertyModifier_prefab = Resources.Load<GameObject>("UI/cnv_levelImportPreview/cnt_levelPropertyModifier");
        }

        if (btn_uiOption_iconVariant_prefab == null)
        {
            btn_uiOption_iconVariant_prefab = Resources.Load<GameObject>("UI/btn_uiOption_iconVariant");
        }

        if (testLvlIcon == null)
        {
            testLvlIcon = Resources.Load<Sprite>("UI/cnv_levelImportPreview/testLvlIcon");

        }

        if (saveLvlIcon == null)
        {
            saveLvlIcon = Resources.Load<Sprite>("UI/cnv_levelImportPreview/saveLvlIcon");
        }

        if (saveLvlIcon != null && parRet && testLvlIcon != null && btn_uiOption_iconVariant_prefab != null && cnt_levelPropertyModifier_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
