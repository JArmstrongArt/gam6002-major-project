﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;

[DefaultExecutionOrder(-1000)]
public class UIAnimationCombiner:MonoBehaviour
{

    private RectTransform myRect;

    public Vector3 OrigAnchoredPos { get; private set; }
    public Vector3 OrigEulerAngles { get; private set; }
    public Vector3 OrigScale { get; private set; }

    private void Awake()
    {
        if (FindDependencies())
        {
            SetOrigsFromCurrent();
        }

    }

    public void SetOrigsFromCurrent()
    {
        if (FindDependencies())
        {
            OrigAnchoredPos = myRect.anchoredPosition3D;
            OrigEulerAngles = myRect.eulerAngles;
            OrigScale = myRect.localScale;
        }
    }

    void PlayCombinedAnimation()
    {
        if (FindDependencies())
        {
            Vector3 finalPos = OrigAnchoredPos;
            Vector3 finalEulerAngles = OrigEulerAngles;
            Vector3 finalScale = OrigScale;

            UIAnimation[] allAnimators = gameObject.GetComponents<UIAnimation>();

            if (allAnimators != null)
            {
                List<UIAnimation> allAnimators_asList = allAnimators.ToList();


                if (allAnimators_asList.Count > 0)
                {
                    foreach (UIAnimation anim in allAnimators)
                    {
                        Vector3 relativePos = anim.AppliedAnchoredPos - OrigAnchoredPos;
                        Vector3 relativeEulerAngles = anim.AppliedEuler - OrigEulerAngles;
                        Vector3 relativeScale = anim.AppliedScale - OrigScale;

                        finalPos += relativePos;
                        finalEulerAngles += relativeEulerAngles;
                        finalScale += relativeScale;

                    }

                    myRect.anchoredPosition3D = finalPos;
                    myRect.eulerAngles = finalEulerAngles;
                    myRect.localScale = finalScale;
                }


            }
        }
    }

    private void Update()
    {
        if (FindDependencies())
        {
            PlayCombinedAnimation();
        }



    }

    bool FindDependencies()
    {
        bool ret = false;

        if (myRect == null)
        {
            myRect = gameObject.GetComponent<RectTransform>();
        }

        if (myRect != null)
        {
            ret = true;
        }

        return ret;
    }

}
