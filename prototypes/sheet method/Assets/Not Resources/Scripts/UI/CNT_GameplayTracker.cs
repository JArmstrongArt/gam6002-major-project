using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

//i wanted to make this abstract but it wouldn't work with some casting i do elsewhere
public class CNT_GameplayTracker:MonoBehaviour
{
    public object statSource;
    public string statSource_statFieldName;
    public string? statSource_statFieldName_outOf=null;
    public bool statIsPublic = false;
    public bool statIsPublic_outOf = false;


    protected Text txt_stat;
    protected Image img_icon;

    public string IconLocation { set; private get; }
    private Sprite iconImg;

    protected UIAnimation_Pulse pulseAnimScr;

    private Image bgImg;

    private bool firstTimeUpdate = false;

    public virtual void UpdateStat()
    {
        if (FindDependencies() && FindResources())
        {
            if (firstTimeUpdate == false)
            {
                img_icon.sprite = iconImg;

                bgImg.color = ExtendedFunctionality.RainbowRandomThisColour(bgImg.color);

                firstTimeUpdate = true;
            }


        }
    }

    protected bool FindDependencies()
    {
        bool ret = false;

        if (txt_stat == null)
        {
            txt_stat = ExtendedFunctionality.GetComponentInChildrenByName<Text>(gameObject, "txt_gT_tracker");
        }

        if (img_icon == null)
        {
            img_icon = ExtendedFunctionality.GetComponentInChildrenByName<Image>(gameObject, "img_gT_icon");
        }

        if (pulseAnimScr == null)
        {
            pulseAnimScr = ExtendedFunctionality.GetComponentInChildrenByName<UIAnimation_Pulse>(gameObject, "img_gT_icon");

        }

        if (bgImg == null)
        {
            bgImg = ExtendedFunctionality.GetComponentInChildrenByName<Image>(gameObject, "img_gT_bg");
        }



        if (bgImg!=null && pulseAnimScr != null && txt_stat != null && img_icon!=null)
        {
            ret = true;
        }

        return ret;
    }

    protected bool FindResources()
    {
        bool ret = false;

        if (iconImg == null)
        {
            iconImg = Resources.Load<Sprite>(IconLocation);
        }

        if (iconImg != null)
        {
            ret = true;
        }

        return ret;
    }

}
