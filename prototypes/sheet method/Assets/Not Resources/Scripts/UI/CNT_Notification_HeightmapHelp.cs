using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CNT_Notification_HeightmapHelp : CNT_Notification
{



    public override void FullSetup(string titleTxt, string notifTxt)
    {

        if (base.FindResources() && base.FindDependencies())
        {

            base.FullSetup(titleTxt, notifTxt);
            CreateBackButton(E_BackButtonAlignment.BR, "Notification_HeightmapSelection", E_SaveFileType.Creation);

            MusicManager.PlaySong(E_TrackList.MainMenu);
        }


    }

    protected override void CreateAllButtons()
    {


    }


}
