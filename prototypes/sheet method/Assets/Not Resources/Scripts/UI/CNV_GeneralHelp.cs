using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CNV_GeneralHelp : UIElemBase
{
    protected override void FullSetup()
    {
        base.FullSetup();

        if (base.FindResources())
        {
            SpawnDecor(E_BGDecor.Background, "Help - How to make Levels");
            CreateBackButton(E_BackButtonAlignment.BR, "MainMenu");
            MusicManager.PlaySong(E_TrackList.MainMenu);

        }

    }

}
