using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
class KeyframeData
{
    public Vector3 pos=Vector3.zero;
    public Vector3 eulerAng=Vector3.zero;
    public Vector3 scale=Vector3.one;
    public bool pos_relativeToOriginal =true;
    public bool eulerAng_relativeToOriginal = true;
    public bool scale_relativeToOriginal = true;
    public int lengthRelativeToAnimation=1;

}


struct KeyframeData_RandomizationData
{
    public Vector3 randomPosOffset;
    public Vector3 randomEulOffset;
    public Vector3 randomScaleOffset;

    public KeyframeData_RandomizationData(Vector3 p, Vector3 e, Vector3 s)
    {
        randomPosOffset = p;
        randomEulOffset = e;
        randomScaleOffset = s;
    }
}


public class UIAnimation_Keyframed : UIAnimation
{
    [SerializeField] KeyframeData[] allKeyframes;

    [SerializeField] float animationTime = 1.0f;
    private float animationTime_current = 0.0f;


    private List<(int index, KeyframeData_RandomizationData randData)> allKeyframes_indexPriority = new List<(int, KeyframeData_RandomizationData)>();

    [SerializeField] Vector3 pos_randomRangePowers = Vector3.zero;
    [SerializeField] Vector3 eulerAng_randomRangePowers = Vector3.zero;
    [SerializeField] Vector3 scale_randomRangePowers = Vector3.zero;

    protected override void InitAnimation(bool firstInitMode)
    {
        if (base.FindDependencies())
        {
            animationTime_current = 0.0f;

        }
    }

    protected override void Animate()
    {
        if (base.FindDependencies())
        {
            allKeyframes_indexPriority.Clear();

            for (int i = 0; i < allKeyframes.Length; i++)
            {

                Vector3 randomPos_current = new Vector3(Random.Range(-pos_randomRangePowers.x, pos_randomRangePowers.x), Random.Range(-pos_randomRangePowers.y, pos_randomRangePowers.y), Random.Range(-pos_randomRangePowers.z, pos_randomRangePowers.z));

                Vector3 randomEul_current= new Vector3(Random.Range(-eulerAng_randomRangePowers.x, eulerAng_randomRangePowers.x), Random.Range(-eulerAng_randomRangePowers.y, eulerAng_randomRangePowers.y), Random.Range(-eulerAng_randomRangePowers.z, eulerAng_randomRangePowers.z));

                Vector3 randomScale_current = new Vector3(Random.Range(-scale_randomRangePowers.x, scale_randomRangePowers.x), Random.Range(-scale_randomRangePowers.y, scale_randomRangePowers.y), Random.Range(-scale_randomRangePowers.z, scale_randomRangePowers.z));

                KeyframeData_RandomizationData newRand = new KeyframeData_RandomizationData(randomPos_current,randomEul_current,randomScale_current);

                for (int j = 0; j < allKeyframes[i].lengthRelativeToAnimation; j++)
                {
                    allKeyframes_indexPriority.Add((i, newRand));
                }
            }




            int keyframeCount = allKeyframes_indexPriority.Count;
            animationTime_current = Mathf.Clamp(animationTime_current, 0, animationTime);

            float animation_lerpProgress = Mathf.InverseLerp(0, animationTime, animationTime_current);

            int keyframe_closest_indexOfIndex = Mathf.Clamp(Mathf.FloorToInt(keyframeCount * animation_lerpProgress), 0, allKeyframes_indexPriority.Count - 1);

            int keyframe_closest = allKeyframes_indexPriority[keyframe_closest_indexOfIndex].index;
            KeyframeData_RandomizationData randData = allKeyframes_indexPriority[keyframe_closest_indexOfIndex].randData;


            Vector3 position_desired = allKeyframes[keyframe_closest].pos;

            if (allKeyframes[keyframe_closest].pos_relativeToOriginal)
            {
                position_desired += combinerScr.OrigAnchoredPos;
            }

            position_desired += randData.randomPosOffset;

            AppliedAnchoredPos = position_desired;





            Vector3 euler_desired = allKeyframes[keyframe_closest].eulerAng;

            if (allKeyframes[keyframe_closest].eulerAng_relativeToOriginal)
            {
                euler_desired += combinerScr.OrigEulerAngles;
            }

            euler_desired += randData.randomEulOffset;

            AppliedEuler = euler_desired;


            Vector3 scale_desired = allKeyframes[keyframe_closest].scale;

            if (allKeyframes[keyframe_closest].scale_relativeToOriginal)
            {
                scale_desired = new Vector3(scale_desired.x * combinerScr.OrigScale.x, scale_desired.y * combinerScr.OrigScale.y, scale_desired.z * combinerScr.OrigScale.z);
            }

            scale_desired += randData.randomScaleOffset;

            AppliedScale = scale_desired;




            animationTime_current += GetMyTimeScale();

            if (animationTime_current >= animationTime)
            {
                animationInProgress = false;
            }
        }

    }


}
