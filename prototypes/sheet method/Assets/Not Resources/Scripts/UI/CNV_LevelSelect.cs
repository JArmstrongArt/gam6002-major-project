using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

using UnityEngine.UI;

public enum E_LevelFolderToLoad
{
    Custom,
    PreMade
}

public class CNV_LevelSelect : UIElemBase
{
    [SerializeField] int levelsPerPageLimit = 4;
    public static E_LevelFolderToLoad folderToLoad = E_LevelFolderToLoad.PreMade;

    private GameObject levelSelectEntry_prefab;

    private Text txt_overallComplete;

    private int levelPageIndex = 0;

    private List<LevelFile> loadedLevelFiles = new List<LevelFile>();

    private BTN btn_previousPage;
    private BTN btn_nextPage;

    private RectTransform myRect;

    private int? myRectY_approxPrevious = null;
    void ShiftPageIndex(int shiftVal)
    {
        if (FindResources() && FindDependencies())
        {
            levelPageIndex += shiftVal;
            FullSetup();
        }

    }

    private void Update()
    {
        if(FindDependencies() && FindResources())
        {


            int newRectYApprox = Mathf.RoundToInt(myRect.sizeDelta.y);

            if (myRectY_approxPrevious==null||(myRectY_approxPrevious!=null && myRectY_approxPrevious != newRectYApprox))
            {
                myRectY_approxPrevious = newRectYApprox;
                FullSetup();
            }
        }
    }





    protected override void FullSetup()
    {
        base.FullSetup();

        if (FindResources() && FindDependencies())
        {
            CreateBackButton(E_BackButtonAlignment.BR, "LevelSelect_TypeSelect", E_SaveFileType.Gameplay);
            MusicManager.PlaySong(E_TrackList.MainMenu);

            List<UnityEngine.Events.UnityAction> previousFunc = new List<UnityEngine.Events.UnityAction>();
            previousFunc.Add(delegate { ShiftPageIndex(-1); });

            List<UnityEngine.Events.UnityAction> nextFunc = new List<UnityEngine.Events.UnityAction>();
            nextFunc.Add(delegate { ShiftPageIndex(1); });

            btn_previousPage.SetupButton(new ButtonData(previousFunc));
            btn_nextPage.SetupButton(new ButtonData(nextFunc));



            GameObject menuBG = SpawnDecor(E_BGDecor.Background);
            CNT_MenuBackground menuBG_scr = menuBG.GetComponent<CNT_MenuBackground>();

            if (menuBG_scr != null)
            {
                string menuTitle = "level select - ";
                switch (folderToLoad)
                {
                    case E_LevelFolderToLoad.PreMade:
                        menuTitle += "pre-made";
                        break;
                    case E_LevelFolderToLoad.Custom:
                        menuTitle += "custom";
                        break;
                }
                menuBG_scr.SetupMenuBG(menuTitle);
            }

            loadedLevelFiles = LevelFileManager.LoadLevelFileDirectory(folderToLoad);

            int pageCountLimit = Mathf.CeilToInt((float)loadedLevelFiles.Count / (float)levelsPerPageLimit);

            levelPageIndex = Mathf.Clamp(levelPageIndex, 0, pageCountLimit - 1);


            int firstIndex = levelPageIndex * levelsPerPageLimit;

            for (int i = firstIndex; i < firstIndex + levelsPerPageLimit; i++)
            {
                if (i >= 0 && i < loadedLevelFiles.Count)
                {
                    if (loadedLevelFiles[i] != null)
                    {
                        GameObject levelSelectEntry_inst = SpawnObjectUnderThisElement(levelSelectEntry_prefab);

                        RectTransform entryRect = levelSelectEntry_inst.GetComponent<RectTransform>();

                        if (entryRect != null)
                        {

                            float loadedFiles_progress = Mathf.Clamp01(Mathf.InverseLerp(firstIndex, (firstIndex + levelsPerPageLimit) - 1, i));



                            float canvHeight_squashMultiplier = Mathf.Abs(0.5f);

                            float canvHeight = Mathf.Abs(myRect.sizeDelta.y) * canvHeight_squashMultiplier;



                            float canvHeight_half = (float)canvHeight / (float)2;

                            float rectY = Mathf.Lerp(canvHeight_half, -canvHeight_half, loadedFiles_progress);


                            ExtendedFunctionality_UI.SetupRectTransform(
                                entryRect,
                                new Vector3(0, rectY, 0),
                                entryRect.sizeDelta,
                                new Vector2(0.5f, 0.5f),
                                new Vector2(0.5f, 0.5f),
                                new Vector2(0.5f, 0.5f),
                                Vector3.zero,
                                Vector3.one
                                );

                            ReadjustAttachedUIAnimations(levelSelectEntry_inst);
                        }
                        else
                        {
                            ExtendedFunctionality.SafeDestroy(levelSelectEntry_inst);
                        }

                        CNT_LevelSelectEntry lSE = levelSelectEntry_inst.GetComponent<CNT_LevelSelectEntry>();

                        if (lSE != null)
                        {
                            lSE.levelILoad = loadedLevelFiles[i];

                            lSE.levelProgress = LevelProgressManager.GetProgressByLevelID_Exposer(loadedLevelFiles[i].progressHookData);
                            lSE.FullSetup_PublicCall();
                        }
                        else
                        {
                            ExtendedFunctionality.SafeDestroy(levelSelectEntry_inst);
                        }
                    }
                }



            }

            txt_overallComplete.text = ExtendedFunctionality.ZeroOneValueAsPercent(LevelProgressManager.TotalCompletionProgress(folderToLoad));




        }
    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (levelSelectEntry_prefab == null)
        {
            levelSelectEntry_prefab = Resources.Load<GameObject>("UI/cnv_levelSelect/cnt_levelSelectEntry");
        }



        if(levelSelectEntry_prefab!=null && parRet)
        {
            ret = true;
        }

        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (txt_overallComplete == null)
        {
            txt_overallComplete = ExtendedFunctionality.GetComponentInChildrenByName<Text>(gameObject, "txt_overallComplete");
        }

        if (btn_previousPage == null)
        {
            btn_previousPage = ExtendedFunctionality.GetComponentInChildrenByName<BTN>(gameObject, "btn_previousPage");
        }

        if (btn_nextPage == null)
        {
            btn_nextPage = ExtendedFunctionality.GetComponentInChildrenByName<BTN>(gameObject, "btn_nextPage");
        }

        if (myRect == null)
        {
            myRect = gameObject.GetComponent<RectTransform>();
        }

        if (myRect!=null && btn_previousPage != null && btn_nextPage!=null && txt_overallComplete != null)
        {
            ret = true;
        }

        return ret;
    }
}
