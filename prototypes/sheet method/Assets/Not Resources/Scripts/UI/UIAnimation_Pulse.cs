using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UIAnimation_Pulse : UIAnimation
{

    [SerializeField] float scaleMultiplier = 1.3f;
    [SerializeField] float pulseComedownTime = 2.0f;
    private float pulseComedownTime_current = 0.0f;

    protected override void InitAnimation(bool firstInitMode)
    {
        if (base.FindDependencies())
        {
            pulseComedownTime_current = 0.0f;

        }

    }

    protected override void Animate()
    {
        if (base.FindDependencies())
        {
            pulseComedownTime_current += GetMyTimeScale();

            pulseComedownTime_current = Mathf.Clamp(pulseComedownTime_current, 0, pulseComedownTime);
            float pulse_lerpProgress = Mathf.InverseLerp(0, pulseComedownTime, pulseComedownTime_current);

            AppliedScale = Vector3.Lerp(combinerScr.OrigScale * scaleMultiplier, combinerScr.OrigScale, pulse_lerpProgress);

            if (pulse_lerpProgress >= 1)
            {
                animationInProgress = false;
            }
        }

    }


}
