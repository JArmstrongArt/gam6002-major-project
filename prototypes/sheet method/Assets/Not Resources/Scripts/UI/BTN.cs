using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;


using SimpleFileBrowser;



using System.IO;

using UnityEngine.EventSystems;

public class ButtonData
{
    public List<UnityEngine.Events.UnityAction> AllButtonFunctions { get; private set; }
    public string? ButtonTxt { get; private set; }
    public Sprite ButtonIcon { get; private set; }

    public int? FontSizeOverride { get; private set; }

    public ButtonData(List<UnityEngine.Events.UnityAction> aBF, string? bT=null,Sprite bI = null,int? fSO=null)
    {
        AllButtonFunctions = aBF;
        ButtonTxt = bT;
        ButtonIcon = bI;
        FontSizeOverride = fSO;
    }
}

public class BTN : MonoBehaviour,IPointerEnterHandler
{

    private Button btn_self;


    private RectTransform myRect;

    public Vector2? MyRect_SizeDelta
    {
        get
        {
            if(FindDependencies() && FindResources())
            {
                return myRect.sizeDelta;
            }
            else
            {
                return null;
            }
        }
    }



    private GameObject sE_btn_hover;

    private GameObject sE_btn_hover_inst=null;

    [Tooltip("Some buttons, when combined with UI animations, will cause the mouse to go in and out of the button hitbox each alternating frame, use this to prevent the sound from murdering your ears.")]
    [SerializeField] bool btnSoundSpamSafety = false;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (FindDependencies() && FindResources())
        {
            if((sE_btn_hover_inst==null && btnSoundSpamSafety) || !btnSoundSpamSafety)
            {
                sE_btn_hover_inst=Instantiate(sE_btn_hover);

            }

        }

    }

    //this function is expected to be ran by a canvas that spawns the button in
    //also, nameTxt does nothing in the parent but I rly needed it for this class' heavily used override in the child _UIOption.
    public virtual void SetupButton(ButtonData btnData, Vector2? btnSizeOverride = null)
    {
        if (FindDependencies() && FindResources())
        {

            btn_self.onClick.RemoveAllListeners();


            if (btnData.AllButtonFunctions != null)
            {
                foreach (UnityEngine.Events.UnityAction action in btnData.AllButtonFunctions)
                {
                    btn_self.onClick.AddListener(action);

                }
            }

            myRect.sizeDelta = btnSizeOverride ?? myRect.sizeDelta;

        }
    }

    protected virtual bool FindDependencies()
    {
        bool ret = false;

        if (btn_self == null)
        {
            btn_self = gameObject.GetComponent<Button>();
        }

        if (myRect == null)
        {
            myRect = gameObject.GetComponent<RectTransform>();
        }

        if (myRect != null && btn_self != null)
        {
            ret = true;
        }

        return ret;
    }

    protected virtual bool FindResources()
    {
        bool ret= false;

        if (sE_btn_hover == null)
        {
            sE_btn_hover = Resources.Load<GameObject>("Sound Effects/UI/sE_btn_hover/sE_btn_hover");
        }



        if (sE_btn_hover!= null)
        {
            ret = true;
        }
        return ret;
    }

}

public abstract class BTN_DelegateFunctions : MonoBehaviour
{
    public static void RemoveHeightmapFromCurrentGenFile(LevelFile curFileFoundOverride=null)
    {
        
        LevelFile curFile = LevelFileManager.GetLoadedLevelFileByType(E_SaveFileType.Creation);


        if (curFileFoundOverride != null)
        {
            curFile = curFileFoundOverride;
        }

        if (curFile == null)
        {
            curFile = new LevelFile();
        }

        if (curFile != null)
        {
            if (curFile.heightmapTex_asBytes != null)
            {
                curFile.heightmapTex_asBytes = null;
                LevelFileManager.SetLevelFile(E_SaveFileType.Creation, curFile);

            }
        }
    }

    public static void RemoveHeightmapAndLiveRegen()
    {
        LevelFile curFile = LevelFileManager.GetLoadedLevelFileByType(E_SaveFileType.Creation);
        if (curFile == null)
        {
            curFile = new LevelFile();
        }

        if (curFile != null)
        {
            RemoveHeightmapFromCurrentGenFile(curFile);
            LevelFileManager.SetLevelFile(E_SaveFileType.Creation, curFile);
            SheetMethod.currentGenerateType = E_SaveFileType.Creation;
            SheetMethod.FullExecute();
        }

    }


    public static void CopyLevelFileThenGoToScene(LevelFile sourceFile, E_SaveFileType destType, string sceneToLoad)
    {
        LevelFileManager.SafeAddToLoadedLevelFiles(destType, sourceFile);
        GoToScene(sceneToLoad, destType);
    }

    public static void CopyLevelFileThenGoToScene(E_SaveFileType sourceType, E_SaveFileType destType, string sceneToLoad)
    {
        LevelFileManager.CopyLoadedLevelFileToAnotherType(sourceType, destType);
        GoToScene(sceneToLoad, destType);
    }


    public static void GoToScene(string? btn_str = null, E_SaveFileType? changeGenerateTypeTo=null,E_LevelFolderToLoad? folderForLevelSelect=null)
    {

        if (btn_str != null)
        {
            string btn_str_nonNull = btn_str ?? string.Empty;

            if (SceneManager.GetSceneByName(btn_str_nonNull) != null)
            {
                SceneManager.LoadScene(btn_str_nonNull);

            }
        }
        SheetMethod.currentGenerateType = changeGenerateTypeTo ?? SheetMethod.currentGenerateType;
        CNV_LevelSelect.folderToLoad = folderForLevelSelect ?? CNV_LevelSelect.folderToLoad;
    }





}


public enum E_FileBrowserSuccessType
{
    Minimal,
    WithWarp,
    LiveGenerate
}

public abstract class BTN_DelegateFunctions_FileBrowser: BTN_DelegateFunctions
{



    static string OpenFileBrowser_Heightmap_Configure()
    {
        List<FileBrowser.Filter> heightmapFilters = new List<FileBrowser.Filter>
        {
            new FileBrowser.Filter("Rasterized Images",".jpg",".jpeg",".png")
        };



        List<string> heightmapFolder_pieces = new List<string> { ExternalDirectories.DIR_ROOT, ExternalDirectories.DIR_MAIN, ExternalDirectories.DIR_HEIGHTMAPS };
        string heightmapFolder = ExternalDirectories.EnsureFolderExists(heightmapFolder_pieces);

        FileBrowser.ClearQuickLinks();
        FileBrowser.AddQuickLink("HeightMap Collection", heightmapFolder);
        FileBrowser.SetFilters(false, heightmapFilters.ToArray());

        return heightmapFolder;
    }

    public static void OpenFileBrowser_Heightmap(E_FileBrowserSuccessType successAction)
    {
        string heightmapFolder = OpenFileBrowser_Heightmap_Configure();

        switch (successAction)
        {
            case E_FileBrowserSuccessType.WithWarp:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_Heightmap_Success_WithWarp, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Files, false, heightmapFolder, null, "Load a HeightMap that will alter the elevation of your level plan...", "Load");

                break;
            case E_FileBrowserSuccessType.LiveGenerate:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_Heightmap_Success_LiveGenerate, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Files, false, heightmapFolder, null, "Load a HeightMap that will alter the elevation of your level plan...", "Load");

                break;
        }

    
        
    
    }

    static void OpenFileBrowser_Heightmap_Success_Internal(string[] paths)
    {
        if (paths.Length > 0)
        {
            string pth = paths[0];

            Texture2D heightMapTex = ExternalDirectories.LoadTextureFromDisk(pth);

            LevelFile editorFile = LevelFileManager.CreateLevelFile(E_SaveFileType.Creation, false);


            editorFile.heightmapTex_asBytes = heightMapTex.EncodeToPNG();



            LevelFileManager.SetLevelFile(E_SaveFileType.Creation, editorFile);



        }
    }

    static void OpenFileBrowser_Heightmap_Success_WithWarp(string[] paths)
    {
        OpenFileBrowser_Heightmap_Success_Internal(paths);
        BTN_DelegateFunctions.GoToScene("LevelImportPreview", E_SaveFileType.Creation);

    }

    static void OpenFileBrowser_Heightmap_Success_LiveGenerate(string[] paths)
    {
        OpenFileBrowser_Heightmap_Success_Internal(paths);
        SheetMethod.FullExecute();

    }

    static string OpenFileBrowser_LPSimple_Configure()
    {
        List<FileBrowser.Filter> LPSimple_filters = new List<FileBrowser.Filter>
        {
            ExternalDirectories.FILTER_IMAGES
        };



        List<string> LPSimpleFolder_pieces = new List<string> { ExternalDirectories.DIR_ROOT, ExternalDirectories.DIR_MAIN, ExternalDirectories.DIR_LEVELPLANS };
        string LPSimpleFolder = ExternalDirectories.EnsureFolderExists(LPSimpleFolder_pieces);

        FileBrowser.ClearQuickLinks();
        FileBrowser.AddQuickLink("Level Plan Collection", LPSimpleFolder);
        FileBrowser.SetFilters(false, LPSimple_filters.ToArray());

        return LPSimpleFolder;
    }

    public static void OpenFileBrowser_LPSimple(E_FileBrowserSuccessType successAction)
    {
        string defaultFolder = OpenFileBrowser_LPSimple_Configure();


        switch (successAction)
        {
            case E_FileBrowserSuccessType.Minimal:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_LPSimple_Success, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Files, false, defaultFolder, null, "Load a level plan to base the design of your level around!", "Load");

                break;

            case E_FileBrowserSuccessType.WithWarp:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_LPSimple_Success_WithWarp, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Files, false, defaultFolder, null, "Load a level plan to base the design of your level around!", "Load");

                break;

            case E_FileBrowserSuccessType.LiveGenerate:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_LPSimple_Success_LiveGenerate, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Files, false, defaultFolder, null, "Load a level plan to base the design of your level around!", "Load");

                break;
        }

    }



    static void OpenFileBrowser_LPSimple_Success_Internal(string[] paths)
    {
        if (paths.Length > 0)
        {
            string pth = paths[0];

            Texture2D levelPlanTex = ExternalDirectories.LoadTextureFromDisk(pth);

            LevelFile editorFile = LevelFileManager.CreateLevelFile(E_SaveFileType.Creation, false);


            editorFile.levelPlanTex_asBytes_ALL = new List<(byte[],float)>
            {
                (levelPlanTex.EncodeToPNG(),0)
            };



            LevelFileManager.SetLevelFile(E_SaveFileType.Creation, editorFile);



        }
    }

    static void OpenFileBrowser_LPSimple_Success_WithWarp(string[] paths)
    {
        OpenFileBrowser_LPSimple_Success_Internal(paths);

        BTN_DelegateFunctions.GoToScene("Notification_HeightmapSelection",E_SaveFileType.Creation);

    }

    static void OpenFileBrowser_LPSimple_Success_LiveGenerate(string[] paths)
    {
        OpenFileBrowser_LPSimple_Success_Internal(paths);

        SheetMethod.FullExecute();


    }

    static void OpenFileBrowser_LPSimple_Success(string[] paths)
    {
        OpenFileBrowser_LPSimple_Success_Internal(paths);
    }


    static string OpenFileBrowser_LPAdvanced_Configure()
    {
        List<FileBrowser.Filter> LPAdvanced_filters = new List<FileBrowser.Filter>
        {
            ExternalDirectories.FILTER_IMAGES
        };



        List<string> LPAdvancedFolder_pieces = new List<string> { ExternalDirectories.DIR_ROOT, ExternalDirectories.DIR_MAIN, ExternalDirectories.DIR_LEVELPLANS };
        string LPAdvancedFolder = ExternalDirectories.EnsureFolderExists(LPAdvancedFolder_pieces);

        FileBrowser.ClearQuickLinks();
        FileBrowser.AddQuickLink("Level Plan Collection", LPAdvancedFolder);
        FileBrowser.SetFilters(false, LPAdvanced_filters.ToArray());

        return LPAdvancedFolder;
    }

    public static void OpenFileBrowser_LPAdvanced(E_FileBrowserSuccessType successAction)
    {
        string defaultFolder = OpenFileBrowser_LPAdvanced_Configure();


        switch (successAction)
        {
            case E_FileBrowserSuccessType.Minimal:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_LPAdvanced_Success, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Folders, false, defaultFolder, null, "Load a folder of level plans to get more control over your level design!", "Load");

                break;

            case E_FileBrowserSuccessType.WithWarp:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_LPAdvanced_Success_WithWarp, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Folders, false, defaultFolder, null, "Load a folder of level plans to get more control over your level design!", "Load");

                break;

            case E_FileBrowserSuccessType.LiveGenerate:
                FileBrowser.ShowLoadDialog(OpenFileBrowser_LPAdvanced_Success_LiveGenerate, OpenFileBrowser_EmptyCancel, FileBrowser.PickMode.Folders, false, defaultFolder, null, "Load a folder of level plans to get more control over your level design!", "Load");

                break;
        }

    }


    static void OpenFileBrowser_LPAdvanced_Success_Internal(string[] paths)
    {
        if (paths.Length > 0)
        {
            string pth = paths[0];


            DirectoryInfo pth_dirInfo = new DirectoryInfo(pth);
            var pth_fileInfo = pth_dirInfo.GetFiles();

            List<string> allImgPaths = new List<string>();

            List<float> imgBaseRaiseValues = new List<float>();

            foreach(FileInfo file in pth_fileInfo)
            {
                for(int i=0;i< ExternalDirectories.FILTER_IMAGES.extensions.Length; i++)
                {
                    if(file.Extension.ToLower()== ExternalDirectories.FILTER_IMAGES.extensions[i].ToLower())
                    {
                        string addPath = ExternalDirectories.SanitizePath(pth + "/"+ file.Name, true);
                        allImgPaths.Add(addPath);

                        try
                        {
                            int result = System.Int32.Parse(ExtendedFunctionality.MakeStringIntoOnlyNumbers( Path.GetFileNameWithoutExtension(file.Name)));
                            imgBaseRaiseValues.Add(result);

                        }
                        catch
                        {
                            imgBaseRaiseValues.Add(0);
                        }


                    }
                }
            }


            LevelFile editorFile = LevelFileManager.CreateLevelFile(E_SaveFileType.Creation, false);


            editorFile.levelPlanTex_asBytes_ALL = new List<(byte[],float)>();

            int biggestImgWidth = int.MinValue;
            int biggestImgHeight = int.MinValue;


            List<Texture2D> levelPlanTex_ALL = new List<Texture2D>();

            foreach (string imgPth in allImgPaths)
            {
                levelPlanTex_ALL.Add( ExternalDirectories.LoadTextureFromDisk(imgPth));


            }

            foreach(Texture2D tex in levelPlanTex_ALL)
            {
                if (tex.width > biggestImgWidth)
                {
                    biggestImgWidth = tex.width;
                }

                if (tex.height > biggestImgHeight)
                {
                    biggestImgHeight = tex.height;
                }
            }

            for(int i = 0; i < levelPlanTex_ALL.Count; i++)
            {
                Texture2D tex = levelPlanTex_ALL[i];
                ResizeTexture_TruePointFiltering_Class.ResizeTexture_TruePointFiltering(tex, (biggestImgWidth, biggestImgHeight));
                editorFile.levelPlanTex_asBytes_ALL.Add((tex.EncodeToPNG(),imgBaseRaiseValues[i]));
            }



            LevelFileManager.SetLevelFile(E_SaveFileType.Creation, editorFile);



        }
    }

    static void OpenFileBrowser_LPAdvanced_Success(string[] paths)
    {
        OpenFileBrowser_LPAdvanced_Success_Internal(paths);
    }

    static void OpenFileBrowser_LPAdvanced_Success_WithWarp(string[] paths)
    {
        OpenFileBrowser_LPAdvanced_Success_Internal(paths);

        BTN_DelegateFunctions.GoToScene("Notification_HeightmapSelection",E_SaveFileType.Creation);

    }

    static void OpenFileBrowser_LPAdvanced_Success_LiveGenerate(string[] paths)
    {
        OpenFileBrowser_LPAdvanced_Success_Internal(paths);

        SheetMethod.FullExecute();


    }


    static void OpenFileBrowser_EmptyCancel()
    {

    }

}