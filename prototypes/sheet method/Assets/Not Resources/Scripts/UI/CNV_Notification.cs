using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CNV_Notification : UIElemBase
{
    private GameObject notif_prefab;

    [Tooltip("Put in the name of a class that can spawn the button options associated with this notification. If the entered name is not a descendant of CNT_Notification, then no buttons will spawn.")]
    [SerializeField] string notifScr_className;


    [SerializeField] string notif_titleTxt;

    [TextArea(3, 10)]
    [SerializeField] string notif_dataTxt;

    private GameObject spawnedNotif;

    [SerializeField] bool overrideDataFontSize = false;
    [SerializeField] int dataFontSize_override = 0;


    [SerializeField] float btnWidthMultiplier = 1;
    protected override void FullSetup()
    {
        base.FullSetup();//keeping this outside of findresources because it erases instsispawned

        if (FindResources() )
        {

            GameObject menuBG = SpawnDecor(E_BGDecor.Background);
            CNT_MenuBackground menuBG_scr = menuBG.GetComponent<CNT_MenuBackground>();

            if (menuBG_scr != null)
            {
                menuBG_scr.SetupMenuBG("notification");
            }

            spawnedNotif = SpawnObjectUnderThisElement(notif_prefab);


            PositionNotification();

        }
    }


    void PositionNotification()
    {
        if (FindResources())
        {
            if (spawnedNotif != null)
            {
                RectTransform notifRect = spawnedNotif.GetComponent<RectTransform>();
                System.Type notifScr_type = System.Type.GetType(notifScr_className);

                if (notifRect != null && notifScr_type != null)
                {
                    ExtendedFunctionality_UI.SetupRectTransform(notifRect,
                    Vector3.zero,
                    null,
                    new Vector2(0.5f, 0.5f),
                    new Vector2(0.5f, 0.5f),
                    new Vector2(0.5f, 0.5f),
                    Vector3.zero,
                    Vector3.one);


                    Component notifScr_generic = spawnedNotif.AddComponent(notifScr_type);

                    CNT_Notification notifScr = (CNT_Notification)notifScr_generic;

                    if (notifScr != null)
                    {
                        if (overrideDataFontSize)
                        {
                            notifScr.OverrideFontSize = dataFontSize_override;
                        }
                        else
                        {
                            notifScr.OverrideFontSize = null;

                        }

                        notifScr.BtnWidthMultiplier = btnWidthMultiplier;


                        notifScr.FullSetup(notif_titleTxt, notif_dataTxt);

                    }
                    else
                    {
                        ExtendedFunctionality.SafeDestroy(notifScr_generic);
                    }



                }
                else
                {
                    ExtendedFunctionality.SafeDestroy(spawnedNotif);
                }
            }
        }


    }



    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (notif_prefab == null)
        {
            notif_prefab = Resources.Load<GameObject>("UI/cnt_notification");
        }

        if (parRet&&notif_prefab != null)
        {
            ret = true;
        }

        return ret;
    }


}
