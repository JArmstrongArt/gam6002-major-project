using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CNT_LevelPropertyModifier : UIElemBase
{

    private Text txt_title;


    private BTN_UIOption btnScr_left;
    private BTN_UIOption btnScr_right;



    public void SetupPropertyModifier(string? titleTxt=null, List<UnityEngine.Events.UnityAction> allButtonFunctions_left = null, string ? buttonTitleText_left = null, List<UnityEngine.Events.UnityAction> allButtonFunctions_right = null, string? buttonTitleText_right = null,int? fontSizeOverride=null)
    {
        if (FindDependencies())
        {
            txt_title.text = titleTxt ?? txt_title.text;


            btnScr_left.SetupButton(new ButtonData(allButtonFunctions_left, buttonTitleText_left, null,fontSizeOverride));
            btnScr_right.SetupButton(new ButtonData(allButtonFunctions_right, buttonTitleText_right, null, fontSizeOverride));
        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (txt_title == null)
        {
            txt_title = ExtendedFunctionality.GetComponentInChildrenByName<Text>(gameObject, "txt_propertyTitle");
        }



        if (btnScr_left == null)
        {
            btnScr_left = ExtendedFunctionality.GetComponentInChildrenByName<BTN_UIOption>(gameObject, "btn_changeFunc1");
        }


        if (btnScr_right == null)
        {
            btnScr_right = ExtendedFunctionality.GetComponentInChildrenByName<BTN_UIOption>(gameObject, "btn_changeFunc2");
        }


        if (txt_title!=null && btnScr_left != null && btnScr_right!=null)
        {
            ret = true;
        }

        return ret;
    }
}
