using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class CNT_Notification_Heightmap : CNT_Notification
{


    //Leave this empty or otherwise invalid to hide the help button.
    private string helpDestination= "Notification_HeightmapHelp";

    private GameObject btn_uiOption_iconVariant_prefab;

    private Sprite helpIcon;

    public override void FullSetup(string titleTxt, string notifTxt)
    {

        if (FindResources() && base.FindDependencies())
        {

            base.FullSetup(titleTxt, notifTxt);
            SetupHelpButton();
        }


    }

    void SetupHelpButton()
    {
        if (FindResources() && base.FindDependencies())
        {
            if (ExtendedFunctionality. SceneWithNameExists(helpDestination))
            {


                GameObject btnInst = SpawnObjectUnderThisElement(btn_uiOption_iconVariant_prefab, "cnt_notification_title");

                BTN_UIOption_IconVariant btnInst_scr = btnInst.GetComponent<BTN_UIOption_IconVariant>();

                RectTransform btnRect = btnInst.GetComponent<RectTransform>();


                if (btnInst_scr != null && btnRect != null)
                {



                    List<UnityEngine.Events.UnityAction> helpButtonFunc = new List<UnityEngine.Events.UnityAction>
                {
                    delegate{BTN_DelegateFunctions.GoToScene(helpDestination,E_SaveFileType.Creation); }
                };

                    btnInst_scr.SetupButton(new ButtonData(helpButtonFunc, null, helpIcon));



                    ExtendedFunctionality_UI.SetupRectTransform(btnRect,
                    new Vector3(-30, 0, 0),
                    new Vector2(35, 35),
                    new Vector2(1, 0.5f),
                    new Vector2(1, 0.5f),
                    new Vector2(0.5f, 0.5f),
                    Vector3.zero,
                    Vector3.one);
                }
                else
                {
                    ExtendedFunctionality.SafeDestroy(btnInst);
                }
            }








        }
    }




    protected override void CreateAllButtons()
    {
        if (FindResources() && base.FindDependencies())
        {



            //create yes button
            List<UnityEngine.Events.UnityAction> yesFuncs = new List<UnityEngine.Events.UnityAction>();
            yesFuncs.Add(delegate { BTN_DelegateFunctions_FileBrowser.OpenFileBrowser_Heightmap(E_FileBrowserSuccessType.WithWarp); });

            base.CreateButton("Yes", yesFuncs);

            
            //create no button
            List<UnityEngine.Events.UnityAction> noFuncs = new List<UnityEngine.Events.UnityAction>();
            noFuncs.Add(delegate { BTN_DelegateFunctions.RemoveHeightmapFromCurrentGenFile(); });
            noFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("LevelImportPreview", E_SaveFileType.Creation); });


            base.CreateButton("No", noFuncs);
            
        }

    }

    protected override bool FindResources()
    {
        bool parRet= base.FindResources();
        bool ret = false;

        if (btn_uiOption_iconVariant_prefab == null)
        {
            btn_uiOption_iconVariant_prefab = Resources.Load<GameObject>("UI/btn_uiOption_iconVariant");
        }

        if (helpIcon == null)
        {
            helpIcon = Resources.Load<Sprite>("UI/cnv_heightmapNotification/helpIcon");
        }

        if(helpIcon!=null && btn_uiOption_iconVariant_prefab != null && parRet)
        {
            ret = true;
        }

        return ret;
    }
}
