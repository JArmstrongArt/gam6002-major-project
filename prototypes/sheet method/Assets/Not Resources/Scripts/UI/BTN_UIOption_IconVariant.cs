using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UnityEngine.UI;


public class BTN_UIOption_IconVariant : BTN
{
    private Image img_icon;

    public override void SetupButton(ButtonData btnData,Vector2? btnSizeOverride=null)
    {
        if (FindDependencies() && base.FindResources())
        {
            base.SetupButton( btnData,btnSizeOverride);

            img_icon.sprite = btnData.ButtonIcon;

        }

    }


    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet = base.FindDependencies();



        if (img_icon == null)
        {
            img_icon = ExtendedFunctionality.GetComponentInChildrenByName<Image>(gameObject,"$btnIcon$");
        }

        if (img_icon != null && parRet)
        {
            ret = true;
        }

        return ret;
    }
}
