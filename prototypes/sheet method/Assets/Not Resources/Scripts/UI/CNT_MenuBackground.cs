using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CNT_MenuBackground : MonoBehaviour
{
    private Text txt_title;

    public void SetupMenuBG(string? titleTxt)
    {
        if (FindDependencies())
        {
            txt_title.text = titleTxt.ToUpper() ?? txt_title.text;
        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (txt_title == null)
        {
            txt_title = ExtendedFunctionality.GetComponentInChildrenByName<Text>(gameObject, "txt_importModeSelection_title");
        }

        if (txt_title != null)
        {
            ret = true;
        }

        return ret;
    }
}
