using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class CNT_LevelSelect_TypeSelect : CNT_Notification
{






    public override void FullSetup(string titleTxt, string notifTxt)
    {

        if (base.FindResources() && base.FindDependencies())
        {

            base.FullSetup(titleTxt, notifTxt);
            CreateBackButton(E_BackButtonAlignment.BR, "MainMenu", E_SaveFileType.Gameplay);
            MusicManager.PlaySong(E_TrackList.MainMenu);
        }


    }






    protected override void CreateAllButtons()
    {
        if (base.FindResources() && base.FindDependencies())
        {



            //create premade button
            List<UnityEngine.Events.UnityAction> premadeFuncs = new List<UnityEngine.Events.UnityAction>();
            premadeFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("LevelSelect", E_SaveFileType.Gameplay,E_LevelFolderToLoad.PreMade); });


            base.CreateButton("PRE-MADE", premadeFuncs);

            //create custom button
            List<UnityEngine.Events.UnityAction> customFuncs = new List<UnityEngine.Events.UnityAction>();
            customFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("LevelSelect",E_SaveFileType.Gameplay, E_LevelFolderToLoad.Custom); });

            base.CreateButton("CUSTOM", customFuncs);




        }

    }

}
