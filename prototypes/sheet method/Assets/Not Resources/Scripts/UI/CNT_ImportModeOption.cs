using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.EventSystems;

public class CNT_ImportModeOption : UIElemBase,IPointerEnterHandler, IPointerExitHandler
{

    private Text txt_title;

    private Text txt_fileTypes;

    private Text txt_desc;

    private Image img_icon;

    private UIAnimation animScr;


    public void OnPointerEnter(PointerEventData eventData)
    {
        if (FindDependencies() && base.FindResources())
        {
            animScr.PlayAnimation_ManualOnly();

        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (FindDependencies() && base.FindResources())
        {
            animScr.CancelAnimationEffectsOnObject();

        }

    }

    public void SetupContainer(string ?titleTxt=null,string ?descTxt=null,string ?typesTxt=null, Sprite iconSpr=null)
    {
        if (FindDependencies() && base.FindResources())
        {
            txt_title.text = titleTxt.ToUpper() ?? txt_title.text;
            txt_desc.text = descTxt.ToUpper() ?? txt_desc.text;

            txt_fileTypes.text = typesTxt.ToUpper() ?? txt_fileTypes.text;

            img_icon.sprite = iconSpr ?? default(Sprite);
            img_icon.color = Color.white;
        }


    }


    bool FindDependencies()
    {
        bool ret = false;
        if (txt_title == null)
        {
            txt_title = ExtendedFunctionality.GetComponentInChildrenByName<Text>(this.gameObject, "txt_importModeOption_title");

        }

        if (txt_fileTypes == null)
        {
            txt_fileTypes = ExtendedFunctionality.GetComponentInChildrenByName<Text>(this.gameObject, "txt_importModeOption_types");
        }

        if (txt_desc == null)
        {
            txt_desc = ExtendedFunctionality.GetComponentInChildrenByName<Text>(this.gameObject, "txt_importModeOption_desc");
        }


        if (img_icon == null)
        {
            img_icon = ExtendedFunctionality.GetComponentInChildrenByName<Image>(this.gameObject, "img_importModeOption_icon");

        }

        if (animScr == null)
        {
            animScr = gameObject.GetComponent<UIAnimation>();
        }


        if(animScr != null && txt_title != null && txt_fileTypes!=null && txt_desc!=null && img_icon != null)
        {
            ret = true;
        }

        return ret;
    }
}
