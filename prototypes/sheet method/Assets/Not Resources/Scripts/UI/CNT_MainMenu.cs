using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class CNT_MainMenu : CNT_Notification
{






    public override void FullSetup(string titleTxt, string notifTxt)
    {

        if (base.FindResources() && base.FindDependencies())
        {

            base.FullSetup(titleTxt, notifTxt);
            MusicManager.PlaySong(E_TrackList.MainMenu);
        }


    }






    protected override void CreateAllButtons()
    {
        if (base.FindResources() && base.FindDependencies())
        {



            //create play button
            List<UnityEngine.Events.UnityAction> playFuncs = new List<UnityEngine.Events.UnityAction>();
            playFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("LevelSelect_TypeSelect", E_SaveFileType.Gameplay); });


            base.CreateButton("PLAY", playFuncs);

            //create credits button
            List<UnityEngine.Events.UnityAction> creditsFuncs = new List<UnityEngine.Events.UnityAction>();
            creditsFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("Credits"); });

            base.CreateButton("CREDITS", creditsFuncs);

            //create controls button
            List<UnityEngine.Events.UnityAction> controlsFuncs = new List<UnityEngine.Events.UnityAction>();
            controlsFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("GameControlsHelp"); });

            base.CreateButton("CNTRLS", controlsFuncs);

            //create help button
            List<UnityEngine.Events.UnityAction> helpFuncs = new List<UnityEngine.Events.UnityAction>();
            helpFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("GeneralHelp"); });

            base.CreateButton("HELP", helpFuncs);

            //create quit button
            List<UnityEngine.Events.UnityAction> quitFuncs = new List<UnityEngine.Events.UnityAction>();
            quitFuncs.Add(delegate { Application.Quit(); });

            base.CreateButton("QUIT", quitFuncs);


            LevelProgressManager.LoadProgress();

            if (LevelProgressManager.TotalCompletionProgress(E_LevelFolderToLoad.PreMade) >= 1)
            {
                //create 100 percent button
                List<UnityEngine.Events.UnityAction> hunPercFuncs = new List<UnityEngine.Events.UnityAction>();
                hunPercFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("100Percent"); });

                base.CreateButton("BONUS", hunPercFuncs);
            }


            //create create button
            List<UnityEngine.Events.UnityAction> createFuncs = new List<UnityEngine.Events.UnityAction>();
            createFuncs.Add(delegate { BTN_DelegateFunctions.GoToScene("ImportModeSelection", E_SaveFileType.Creation); });

            base.CreateButton("CREATE", createFuncs);



        }

    }



}
