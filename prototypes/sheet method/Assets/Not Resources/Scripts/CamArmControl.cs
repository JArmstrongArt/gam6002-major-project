using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamArmControl : MonoBehaviour
{
    private Camera camScr = null;
    public Camera CamScr
    {
        get
        {
            return camScr;
        }
    }

    private float? origFOV=null;

    public float? OrigFOV
    {
        get
        {
            return origFOV;
        }
    }

    [SerializeField] GameObject trackObj = null;
    private InputManager inputScr;

    private LookAtObject camLookScr;
    [SerializeField] float camTurnPower_mouse =0.25f;
    [SerializeField] float camTurnPower_controller = 75.0f;
    public GameObject TrackObj
    {
        set
        {
            trackObj = value;
        }
    }

    private Player_GravityControl gravScr;

    private float getGrav_attemptTimer = 1.0f;
    private float getGrav_attemptTimer_current = 1.0f;

    private float? targetPos_previousY=null;

    private GameObject trackObj_hidden;

    public (Vector3 forDir, Vector3 sideDir) FlatDirs
    {
        get
        {
            Vector3 cam_flatForward = new Vector3(transform.forward.x, 0, transform.forward.z).normalized;
            Vector3 cam_flatRight = new Vector3(transform.right.x, 0, transform.right.z).normalized;

            return (cam_flatForward, cam_flatRight);
        }
    }

    private void OnDestroy()
    {
        ExtendedFunctionality.SafeDestroy(trackObj_hidden);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void Awake()
    {
        if (FindDependencies())
        {
            origFOV = camScr.fieldOfView;


        }
    }

    void LateUpdate()
    {
        if (FindDependencies())
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            if (trackObj != null)
            {
                if (trackObj_hidden == null)
                {
                    trackObj_hidden = new GameObject();
                    trackObj_hidden.name = gameObject.name.ToString() + "_trackObj_hidden";
                }

                if (trackObj_hidden != null)
                {
                    if (getGrav_attemptTimer_current >= getGrav_attemptTimer)
                    {
                        if (gravScr == null)
                        {
                            gravScr = trackObj.GetComponent<Player_GravityControl>();


                        }
                        getGrav_attemptTimer_current = 0.0f;
                    }
                    else
                    {
                        getGrav_attemptTimer_current += Time.deltaTime;
                    }

                    Vector3 targetPos = trackObj.transform.position;
                    Vector3 currentPos = targetPos;
                    if (targetPos_previousY == null)
                    {
                        targetPos_previousY = targetPos.y;
                    }


                    if (gravScr != null)
                    {
                        if (gravScr.GroundRay.RaySurface != null)
                        {
                            float yPos = Mathf.Lerp(gameObject.transform.position.y, targetPos.y, 5 * Time.deltaTime);

                            currentPos = new Vector3(targetPos.x, yPos, targetPos.z);
                            targetPos_previousY = yPos;

                        }
                        else
                        {
                            currentPos = new Vector3(targetPos.x, targetPos_previousY ?? default(float), targetPos.z);
                        }
                        trackObj_hidden.transform.position = new Vector3(trackObj.transform.position.x, targetPos_previousY ?? default(float), trackObj.transform.position.z);

                    }
                    else
                    {
                        trackObj_hidden.transform.position = trackObj.transform.position;
                    }

                    gameObject.transform.position = currentPos;
                }



            }

            camLookScr.LookTarget = trackObj_hidden;

            float yRot = gameObject.transform.eulerAngles.y;

            yRot += inputScr.GetInputByName("General", "Turn_Mouse") * camTurnPower_mouse;//no *deltaTime because its mouse input
            yRot += inputScr.GetInputByName("General", "Turn_Controller") * camTurnPower_controller * Time.deltaTime;
            gameObject.transform.rotation = Quaternion.Euler(new Vector3(0,yRot, 0));


        }



    }

    bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (camScr == null)
        {
            if (gameObject.transform.childCount > 0)
            {

                camScr = gameObject.transform.GetChild(0).gameObject.GetComponent<Camera>();

            }
        }

        if (camScr != null)
        {
            if (camLookScr == null)
            {
                camLookScr = camScr.GetComponent<LookAtObject>();
            }
        }



        if (inputScr != null && camLookScr!=null && camScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
