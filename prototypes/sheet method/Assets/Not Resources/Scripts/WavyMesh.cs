using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavyMesh : MonoBehaviour
{
    private MeshFilter filtComp;

    private Mesh target;
    private Vector3[] origVerts = null;



    [SerializeField] SineData waveData;
    public SineData WaveData
    {
        set
        {
            waveData = value;
        }
    }


    private void Update()
    {
        if (FindDependencies() && FindTargetMesh())
        {
            target.MarkDynamic();

            target.vertices = WavyMeshWaveEffect_Class.WavyMeshWaveEffect_ApplyToCopy(origVerts, waveData);
        }
    }

    bool FindTargetMesh()
    {
        bool ret = false;

        if (origVerts == null || target == null)
        {
            target = filtComp.mesh;

            if (target != null)
            {
                origVerts = target.vertices;
            }
        }

        if(origVerts!=null && target != null)
        {
            ret = true;
        }


        return ret;
    }


    bool FindDependencies()
    {
        bool ret = false;

        if (filtComp == null)
        {
            filtComp = gameObject.GetComponent<MeshFilter>();
        }

        if (filtComp != null)
        {
            ret = true;
        }

        return ret;
    }


}
