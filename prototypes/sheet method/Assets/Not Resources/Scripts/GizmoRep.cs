using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class GizmoRep : MonoBehaviour
{



    [SerializeField] float sphereRadius = 0.5f;
    public float SphereRadius
    {
        private get
        {
            return sphereRadius;
        }

        set
        {
            sphereRadius = value;
        }
    }
    [SerializeField] Color gizmoCol = Color.red;

    public Color GizmoCol
    {
        private get
        {
            return gizmoCol;
        }

        set
        {
            gizmoCol = value;
        }
    }

    [SerializeField] string gizmoText = "";
    public string GizmoText
    {
        private get
        {
            return gizmoText;
        }

        set
        {
            gizmoText = value;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = GizmoCol;
        Gizmos.DrawSphere(gameObject.transform.position, SphereRadius);



    }

}
