using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum E_ModelStates_Player
{
    Idle,
    Moving,
    Jumping,
    Falling,
    Dash
}

public class Player_ModelControl : ModelControl
{




    private Player_GravityControl gravScr;



    private Player_MoveControl moveScr;
    private InputManager inputScr;

    private Player_Dash dashScr;



    protected override void DecideAnimation_ApplyFromContext(int? animEnum)
    {
        if (base.FindResources() && FindDependencies())
        {
            int animEnum_nonNull = animEnum ?? (int)E_ModelStates_Player.Idle;

            E_ModelStates_Player modelState = (E_ModelStates_Player)animEnum_nonNull;


            switch (modelState)
            {

                case E_ModelStates_Player.Idle:
                default:

                    MyModel_Anim.Play("Idle");
                    break;

                case E_ModelStates_Player.Falling:

                    MyModel_Anim.Play("Falling");
                    break;

                case E_ModelStates_Player.Jumping:

                    MyModel_Anim.Play("Jumping");
                    break;

                case E_ModelStates_Player.Moving:

                    MyModel_Anim.Play("Moving");
                    break;
                case E_ModelStates_Player.Dash:

                    MyModel_Anim.Play("Dash");
                    break;
            }

            if (moveScr.ArmControlScr != null)
            {
                float inpForward = inputScr.GetInputByName("Movement", "Vertical");
                float inpSide = inputScr.GetInputByName("Movement", "Horizontal");

                Vector3 moveForward = moveScr.ArmControlScr.FlatDirs.forDir * inpForward;
                Vector3 moveRight = moveScr.ArmControlScr.FlatDirs.sideDir * inpSide;




                Vector3 moveCalc = moveForward + moveRight;

                Vector3 lookRot = new Vector3(moveCalc.x, 0, moveCalc.z).normalized;

                if (lookRot != Vector3.zero)
                {
                    MyModel_Inst.transform.rotation = Quaternion.LookRotation(new Vector3(moveCalc.x, 0, moveCalc.z).normalized);

                }


            }
            else
            {
                MyModel_Inst.transform.rotation = Quaternion.Euler(Vector3.zero);
            }
        }

    }

    protected override int? DecideAnimation_Contextual()
    {
        int? ret = null;
        if (base.FindResources() && FindDependencies())
        {




            E_ModelStates_Player animToPlay = E_ModelStates_Player.Idle;

            if (gravScr.GroundRay.RaySurface != null)
            {

                if (!dashScr.DashInProgress)
                {
                    if (moveScr.MoveCalc.magnitude > 0.1f)
                    {
                        animToPlay = E_ModelStates_Player.Moving;
                    }
                    else
                    {
                        animToPlay = E_ModelStates_Player.Idle;
                    }
                }
                else
                {
                    animToPlay = E_ModelStates_Player.Dash;

                }

            }
            else
            {
                if (gravScr.Gravity_Current > 0)
                {
                    animToPlay = E_ModelStates_Player.Jumping;
                }
                else
                {
                    animToPlay = E_ModelStates_Player.Falling;

                }
            }

            ret = (int)animToPlay;


        }
        return ret;
    }






    protected override bool FindDependencies()
    {
        bool parRet = base.FindDependencies();
        bool ret = false;

        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<Player_GravityControl>();
        }

        if (moveScr == null)
        {
            moveScr = gameObject.GetComponent<Player_MoveControl>();
        }

        if (dashScr == null)
        {
            dashScr = gameObject.GetComponent<Player_Dash>();
        }


        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }
        if(dashScr!=null && inputScr != null  && gravScr != null && moveScr != null && parRet)
        {
            ret = true;
        }

        return ret;
        
    }


}
