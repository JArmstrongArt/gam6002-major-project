using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-100)]
public class Player_GravityControl : MonoBehaviour
{
    private InputManager inputScr;
    public GenericRay GroundRay { get; private set; }
    private GenericRay enemyJumpRay;


    [SerializeField] float gravity_rate = 1.3f;
    private float gravity_rate_current;
    [SerializeField] float gravity_terminal = 1.3f;
    public float Gravity_Terminal
    {
        get
        {
            return gravity_terminal;
        }
    }
    public float Gravity_Current { get; set; }
    [SerializeField] float jumpPower = 0.49f;
    [SerializeField] float jumpPower_enemyJumpMultiplier = 1.3f;
    private float jumpPower_current;

    private CharacterController charConScr;
    private Player_Statistics statsScr;

    [SerializeField] float jump_shortStopGravity = 0.125f;

    private bool enemyJumpActive = false;

    private GameObject sE_jump_enemy;
    private GameObject sE_jump_regular;

    public void AbruptStopCurGravity()
    {
        if(FindDependencies() && FindResources())
        {
            Gravity_Current = 0.0f;

        }
    }


    private void FixedUpdate()
    {
        if (FindDependencies() && FindResources())
        {
            if (statsScr.WaterMode)
            {

                gravity_rate_current = gravity_rate * statsScr.WaterMode_GravRateMultiplier;
            }
            else
            {
                gravity_rate_current = gravity_rate;



            }

            if (statsScr.HazardousGoop_Active)
            {
                jumpPower_current = jumpPower * statsScr.HazardousGoop_JumpPower_Multiplier;
            }
            else
            {
                jumpPower_current = jumpPower;
            }


            bool jumpSignal = inputScr.GetInputByName("Movement", "Jump") > 0;



            bool normalJump = GroundRay.RaySurface != null && jumpSignal;
            bool enemyJump = Gravity_Current < 0 && enemyJumpRay.RaySurface != null;






            if (normalJump||enemyJump)
            {


                
                
                
                if (enemyJump)
                {
                    Enemy enemyScr = enemyJumpRay.RaySurface.GetComponent<Enemy>();

                    if (jumpSignal)
                    {
                        Gravity_Current = jumpPower_current * jumpPower_enemyJumpMultiplier;
                        Instantiate(sE_jump_enemy);


                        if (enemyScr != null)
                        {
                            enemyScr.SlowdownOnDeath = true;
                        }

                    }
                    else
                    {
                        Gravity_Current = jumpPower_current;
                        Instantiate(sE_jump_regular);

                    }


                    if (enemyScr != null)
                    {
                        enemyScr.SoundEffectOnDeath = true;
                    }
                    enemyJumpActive = true;




                    Destroy(enemyJumpRay.RaySurface);


                }
                else
                {
                    Instantiate(sE_jump_regular);

                    Gravity_Current = jumpPower_current;

                }
            }


            if (GroundRay.RaySurface == null || (GroundRay.RaySurface != null && jumpSignal))
            {
                if(Gravity_Current>0 && Gravity_Current> jump_shortStopGravity&&!jumpSignal &&!enemyJumpActive)
                {
                    Gravity_Current = jump_shortStopGravity;
                }

                Gravity_Current -= gravity_rate_current * Time.fixedDeltaTime;

                bool reachedTerminal = Gravity_Current < -Mathf.Abs(gravity_terminal);

                if (reachedTerminal)
                {
                    Gravity_Current = -Mathf.Abs(gravity_terminal);
                }
                charConScr.Move(new Vector3(0, Gravity_Current, 0));


            }
            else
            {
                enemyJumpActive = false;

                float correctGroundedDistance = GroundRay.RayHitPoint.y + ExtendedFunctionality.GetHalfMeasureOfGameObject(gameObject, E_WorldAxes.Y);

                if (Mathf.Abs(GroundRay.RayHitPoint.y-charConScr.transform.position.y)!= correctGroundedDistance)
                {

                    ExtendedFunctionality.DirectMoveCharacterController_FallbackIfNotPresent(gameObject, new Vector3(GroundRay.RayHitPoint.x, correctGroundedDistance, GroundRay.RayHitPoint.z));

                }


                Gravity_Current = 0.0f;


            }
        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (GroundRay == null)
        {
            GroundRay = ExtendedFunctionality.GetComponentInChildrenByName<GenericRay>(gameObject, "groundRay");
        }

        if (charConScr == null)
        {
            charConScr = gameObject.GetComponent<CharacterController>();
        }

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (statsScr == null)
        {
            statsScr = gameObject.GetComponent<Player_Statistics>();
        }

        if (enemyJumpRay == null)
        {
            enemyJumpRay = ExtendedFunctionality.GetComponentInChildrenByName<GenericRay>(gameObject, "enemyJumpRay");
        }

        if (enemyJumpRay != null && statsScr != null && GroundRay != null && charConScr!=null && inputScr!=null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_jump_enemy == null)
        {
            sE_jump_enemy = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_jump_enemy/sE_jump_enemy");
        }

        if (sE_jump_regular == null)
        {
            sE_jump_regular = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_jump_regular/sE_jump_regular");
        }

        if (sE_jump_enemy != null && sE_jump_regular != null)
        {
            ret = true;
        }

        return ret;
    }
}
