using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_TestModeEscapability : MonoBehaviour
{
    private InputManager inputScr;

    private bool exitTest_axisUsed = false;

    private GameObject cnv_testModeNotif_prefab;
    private GameObject cnv_testModeNotif_inst;
    // Start is called before the first frame update
    void Awake()
    {
        if(FindDependencies() && FindResources())
        {
            if (SheetMethod.currentGenerateType == E_SaveFileType.Testing)
            {
                cnv_testModeNotif_inst = Instantiate(cnv_testModeNotif_prefab);
            }
        }
    }

    private void OnDestroy()
    {
        ExtendedFunctionality.SafeDestroy(cnv_testModeNotif_inst);
    }

    // Update is called once per frame
    void Update()
    {
        if (FindDependencies() && FindResources())
        {

            if (inputScr.GetInputByName("TestMode", "EscapeTestMode") > 0)
            {
                if (!exitTest_axisUsed)
                {
                    switch (SheetMethod.currentGenerateType)
                    {
                        case E_SaveFileType.Testing:
                            BTN_DelegateFunctions.CopyLevelFileThenGoToScene(E_SaveFileType.Testing, E_SaveFileType.Creation, "LevelImportPreview");

                            break;
                        case E_SaveFileType.Gameplay:

                            BTN_DelegateFunctions.GoToScene("LevelSelect");
                            break;
                    }


                    exitTest_axisUsed = true;
                }

            }
            else
            {
                exitTest_axisUsed = false;
            }
        }


    }

    bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (inputScr != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (cnv_testModeNotif_prefab == null)
        {
            cnv_testModeNotif_prefab = Resources.Load<GameObject>("UI/Gameplay/cnv_testModeNotif");
        }

        if (cnv_testModeNotif_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
