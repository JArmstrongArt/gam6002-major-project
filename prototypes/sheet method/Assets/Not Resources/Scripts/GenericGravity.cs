using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericGravity : MonoBehaviour
{
    [SerializeField] float gravityRate=7.0f;
    public float GravityRate
    {
        set
        {
            gravityRate = value;
        }
    }

    [SerializeField] float gravityTerminal=4.0f;
    public float GravityTerminal
    {
        set
        {
            gravityTerminal = value;
        }
    }

    private float gravityCurrent = 0.0f;

    public float GravityCurrent
    {
        set
        {
            gravityCurrent = value;
        }
    }

    [SerializeField] bool selfMoving = true;
    public bool SelfMoving
    {
        set
        {
            selfMoving = value;
        }
    }





    public Vector3 UpdateGravityValue(float timeUnit)
    {
        if (gravityCurrent > -Mathf.Abs(gravityTerminal))
        {
            gravityCurrent -= timeUnit * Mathf.Abs(gravityRate);
        }
        else
        {
            gravityCurrent = -Mathf.Abs(gravityTerminal);
        }

        return new Vector3(0,gravityCurrent,0);
    }


    private void FixedUpdate()
    {


        if (selfMoving)
        {
            gameObject.transform.position += UpdateGravityValue(Time.fixedDeltaTime);
        }
    }


}
