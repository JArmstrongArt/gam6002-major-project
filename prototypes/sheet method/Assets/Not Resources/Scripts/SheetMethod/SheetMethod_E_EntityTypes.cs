using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_EntityTypes
{
    Player,
    Collectable,
    Finish,
    Enemy
}

abstract class EntityTypes_CommonFuncs
{
    private static readonly Dictionary<E_EntityTypes, Color> entitiesAndColours = new Dictionary<E_EntityTypes, Color>()
    {
        [E_EntityTypes.Player] = new Color(1.0f, 0.0f, 0.0f),
        [E_EntityTypes.Collectable] = new Color(1.0f, 0.9f, 0.0f),
        [E_EntityTypes.Finish] = new Color(0.0f, 1.0f, 0.0f),
        [E_EntityTypes.Enemy] = new Color(0, 0, 1.0f)


    };

    public static Color GetEntityColourByEntityType(E_EntityTypes entType)
    {
        if (entitiesAndColours.ContainsKey(entType))
        {
            return entitiesAndColours[entType];
        }
        else
        {
            return entitiesAndColours[E_EntityTypes.Player];
        }
    }

    public static E_EntityTypes GetEntityTypeByEntityColour(Color entColour)
    {
        E_EntityTypes ret = E_EntityTypes.Player;



        Color closestCol = FindClosestEntityColour(entColour).closestCol;

        foreach (KeyValuePair<E_EntityTypes, Color> pair in entitiesAndColours)
        {
            if (pair.Value == closestCol)
            {
                ret = pair.Key;
                break;
            }
        }
        return ret;
    }

    public static (Color closestCol, float closestCol_dist) FindClosestEntityColour(Color entColour)
    {
        Color ret_closestCol = entitiesAndColours[E_EntityTypes.Player];

        float ret_closestCol_dist = float.MaxValue;


        foreach (KeyValuePair<E_EntityTypes, Color> pair in entitiesAndColours)
        {
            Vector3 pointA = new Vector3(entColour.r, entColour.g, entColour.b);
            Vector3 pointB = new Vector3(pair.Value.r, pair.Value.g, pair.Value.b);

            float vecDistance = Vector3.Distance(pointA, pointB);
            if (vecDistance < ret_closestCol_dist)
            {
                ret_closestCol_dist = vecDistance;
                ret_closestCol = pair.Value;
            }
        }
        return (ret_closestCol, ret_closestCol_dist);
    }

    public static E_EntityTypes GetEntityTypeByEntityColour_FromColourSet(Color[,] colourSet, (int X, int Y) coord)
    {
        Color seekColour = Color.white;

        if (coord.X >= 0 && coord.X < colourSet.GetLength(0))
        {
            if (coord.Y >= 0 && coord.Y < colourSet.GetLength(1))
            {
                seekColour = colourSet[coord.X, coord.Y];
            }
        }

        return GetEntityTypeByEntityColour(seekColour);
    }
}