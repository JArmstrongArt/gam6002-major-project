using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetShape_Bounds
{
    public (int X, int Y) TopLeft { get; set; }
    public (int X, int Y) BottomRight { get; set; }

    public (int X, int Y) BaseZeroOffset { get; private set; }

    public SheetShape_Bounds((int, int) tL, (int, int) bR, (int, int) bZO)//wanted to make bZO optional but i can't find a suitable tuple compile time constant, (0,0) doesn't work.
    {
        BaseZeroOffset = bZO;

        TopLeft = tL;



        BottomRight = bR;


    }

    public (int width, int height) GetBoundsSize()
    {
        return (BottomRight.X - TopLeft.X, BottomRight.Y - TopLeft.Y);
    }

    public SheetShape_Bounds BaseZeroCopy()//this will make a deep copy of the bounds where the lowest value for X and Y is 0,0 and all other values have been shifted backwards to fit.
    {

        SheetShape_Bounds ret = new SheetShape_Bounds((TopLeft.X, TopLeft.Y), (BottomRight.X, BottomRight.Y), (0, 0));


        if (TopLeft.X != 0 || TopLeft.Y != 0)
        {
            int xBound_minus = ret.TopLeft.X;
            int yBound_minus = ret.TopLeft.Y;
            ret.BaseZeroOffset = (xBound_minus, yBound_minus);

            ret.TopLeft = (ret.TopLeft.X - xBound_minus, ret.TopLeft.Y - yBound_minus);

            ret.BottomRight = (ret.BottomRight.X - xBound_minus, ret.BottomRight.Y - yBound_minus);

        }



        return ret;


    }


}