using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetMesh_GridTile
{
    public (int tileWidth, int tileHeight) TileDimensions { get; private set; }
    public (int X, int Y) TopLeftCoord { get; set; }
    public E_SheetMesh_GridTileType GridTileType { set; get; }



    public SheetMesh_GridTile((int X, int Y) tLC, (int tileWidth, int tileHeight) tD, E_SheetMesh_GridTileType gTT=E_SheetMesh_GridTileType.Blank)
    {
        TopLeftCoord = tLC;
        TileDimensions = tD;

        GridTileType = gTT;
    }

}
