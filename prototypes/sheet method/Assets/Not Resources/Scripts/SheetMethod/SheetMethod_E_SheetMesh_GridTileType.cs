using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_SheetMesh_GridTileType
{
    Blank,
    Full,
    Mixed
}