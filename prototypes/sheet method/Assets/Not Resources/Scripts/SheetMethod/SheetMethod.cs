using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public static class SheetMethod
{

    //general
    static readonly int levelPlan_PixelLimit = 400;
    static readonly bool debugMeshBounds = false;


    //heightmap config
    static readonly float heightmap_power = 30.0f;


    //mesh decoration - general
    static readonly float levelThickness = 5.0f;
    static readonly float floorInsetPower = -2.25f;
    static readonly float floorInsetRaise = 3.0f;

    //mesh decoration - hazardous goop
    static readonly float goopThickness = 5.0f;
    static readonly float goopWavyInsetMinimum = 1.0f;
    static readonly float goopWavyInsetMaxmimum = 3.5f;

    //mesh decoration - ice
    static readonly float iceJaggedInsetMinimum = 0.0f;
    static readonly float iceJaggedInsetMaximum = 8.0f;




    //simplified mesh resolution calculation because the complexity thing really wasn't even that effective
    static float meshResolution_stepDownFromColourSetDivier = 40.0f;

    static (int lower, int higher) meshResolutionRange = (2, 10);








    static GameObject entity_player_prefab;
    static GameObject entity_collectable_prefab;
    static GameObject entity_finish_prefab;
    static GameObject entity_enemy_prefab;


    public static LevelFile LoadedLevelData { get; private set; }
    static Texture2D heightmap_tex = null;
    static List<(Texture2D levelPlanTex, float baseRaiseGeometry)> lvlPlan_tex_ALL = null;

    public static E_SaveFileType currentGenerateType;

    static GameObject creationCam_prefab;

    static List<GameObject> objectsToCleanUpIfRepeated = new List<GameObject>();

    static Dictionary<E_EntityTypes, int> entityCountByType = new Dictionary<E_EntityTypes, int>();


    public static bool LevelPassesEntityCheckForSavingToFile()
    {
        bool ret = false;
        List<E_EntityTypes> requiredEntities = new List<E_EntityTypes> { E_EntityTypes.Player, E_EntityTypes.Finish };

        bool allKeysExist = true;

        foreach(E_EntityTypes entTyp in requiredEntities)
        {
            if (!entityCountByType.ContainsKey(entTyp))
            {
                allKeysExist = false;
                break;
            }
        }

        if (allKeysExist)
        {
            bool allKeysMoreThan0 = true;

            foreach (E_EntityTypes entTyp in requiredEntities)
            {
                if (entityCountByType[entTyp]<=0)
                {
                    allKeysMoreThan0 = false;
                    break;
                }
            }

            if (allKeysMoreThan0)
            {
                ret = true;
            }
        }


        return ret;
    }


    static bool FullExecute_ValidDataCheck()
    {
        bool ret = false;
        if(FindResources_Entities() && FindResources())
        {

            LoadedLevelData = LevelFileManager.GetLoadedLevelFileByType(currentGenerateType);

            if(LoadedLevelData != null)
            {
                heightmap_tex = null;
                lvlPlan_tex_ALL = ExternalDirectories.LoadAllTexturesFromByteList(LoadedLevelData.levelPlanTex_asBytes_ALL);

                //dont check for heightmap null as it is optional
                if (lvlPlan_tex_ALL != null)
                {
                    heightmap_tex = ExternalDirectories.LoadTextureFromBytes(LoadedLevelData.heightmapTex_asBytes);

                    ret = true;
                }
            }
        }

        return ret;
    }


    static bool MeshResolutionTooBigForImage( (int X, int Y) dimensions)
    {
        int longestDimension = dimensions.X;

        if (dimensions.Y > dimensions.X)
        {
            longestDimension = dimensions.Y;
        }

        if (longestDimension <= meshResolutionRange.higher)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    static List<(Texture2D levelPlanTex, float baseRaiseGeometry)> LevelPlan_Tex_ALL_WithoutSmallImages(List<(Texture2D levelPlanTex, float baseRaiseGeometry)> orig)
    {
        List<(Texture2D levelPlanTex, float baseRaiseGeometry)> ret = new List<(Texture2D levelPlanTex, float baseRaiseGeometry)>();

        for (int i = 0; i < orig.Count; i++)
        {
            (int X, int Y) imgDimensions = (orig[i].levelPlanTex.width, orig[i].levelPlanTex.height);

            bool imageTooSmall = MeshResolutionTooBigForImage(imgDimensions);

            if (!imageTooSmall)
            {
                ret.Add(orig[i]);
            }
        }

        return ret;
    }

    //run this from any other function to generate a level
    public static void FullExecute()
    {


        if (FindResources_Entities() && FindResources())
        {
            if (FullExecute_ValidDataCheck())
            {

                SheetMethod_SheetMesh_MeshGeneration.FlushThemeItems();
                
                ExtendedFunctionality.SafeDestroy(objectsToCleanUpIfRepeated);


                List<(Texture2D levelPlanTex, float baseRaiseGeometry)> lvlPlan_tex_ALL_noSmallImages = LevelPlan_Tex_ALL_WithoutSmallImages(lvlPlan_tex_ALL);


                GameObject playerEnt_forRescaling = null;
                List<GameObject> spawnedEnts_forRescaling = new List<GameObject>();
                List<GameObject> spawnedMeshes_forRescaling = new List<GameObject>();

                List<float> spawnedEnts_baseRaiseValues = new List<float>();
                List<float> spawnedMeshes_baseRaiseValues = new List<float>();

                entityCountByType = new Dictionary<E_EntityTypes, int>();

                for (int i = 0; i < lvlPlan_tex_ALL_noSmallImages.Count; i++)
                {
                    Texture2D lvlPlan_current = lvlPlan_tex_ALL_noSmallImages[i].levelPlanTex;
                    float baseRaise_current = lvlPlan_tex_ALL_noSmallImages[i].baseRaiseGeometry;


                    bool patchOverEntities = lvlPlan_tex_ALL_noSmallImages.Count <= 1;

                    (Texture2D levelAfterPatchOver, List<GameObject> spawnedEntities, GameObject playerEnt) ent = GenerateEntities(lvlPlan_current, patchOverEntities);

                    List<GameObject> spawnedMeshes = GenerateModels(ent.levelAfterPatchOver);


                    

                    

                    if (ent.playerEnt != null)
                    {
                        playerEnt_forRescaling = ent.playerEnt;
                    }

                    for(int j=0;j< ent.spawnedEntities.Count;j++)
                    {
                        spawnedEnts_baseRaiseValues.Add(baseRaise_current);
                    }

                    for (int j = 0; j < spawnedMeshes.Count; j++)
                    {
                        spawnedMeshes_baseRaiseValues.Add(baseRaise_current);
                    }

                    spawnedEnts_forRescaling.AddRange(ent.spawnedEntities);
                    spawnedMeshes_forRescaling.AddRange(spawnedMeshes);

                }


                List<GameObject> allObjs = new List<GameObject>();
                allObjs.AddRange(spawnedEnts_forRescaling);
                allObjs.AddRange(spawnedMeshes_forRescaling);













                List<GameObject> unraisedEntities = RaiseEntitiesToGeometry(spawnedEnts_forRescaling);




                (float lowestY, float highestY) surfaceMeshYSpan = GetAxisSpanOfAllSurfaceMeshes(spawnedMeshes_forRescaling, E_WorldAxes.Y);

                SetEntityHeightToLevelAverage(unraisedEntities, surfaceMeshYSpan);

                
                if (playerEnt_forRescaling != null)
                {


                    Vector3 playerEnt_scale = playerEnt_forRescaling.transform.localScale;

                    RescaleAroundPlayerEntity(playerEnt_scale, allObjs, playerEnt_forRescaling.GetComponent<CharacterController>());

                }


                for(int i=0;i<spawnedEnts_forRescaling.Count;i++)
                {
                    ExtendedFunctionality.DirectMoveCharacterController_FallbackIfNotPresent(spawnedEnts_forRescaling[i], spawnedEnts_forRescaling[i].transform.position + new Vector3(0, spawnedEnts_baseRaiseValues[i], 0));

                }

                for (int i = 0; i < spawnedMeshes_forRescaling.Count; i++)
                {
                    spawnedMeshes_forRescaling[i].transform.position += new Vector3(0, spawnedMeshes_baseRaiseValues[i], 0);
                }



                SetupEntitiesIfNeeded(spawnedEnts_forRescaling, playerEnt_forRescaling);




                if (currentGenerateType == E_SaveFileType.Creation)
                {
                    MakeAllEntitiesStill(spawnedEnts_forRescaling);
                    CreateCreationCam(spawnedMeshes_forRescaling, 1, 20.0f);

                }




                objectsToCleanUpIfRepeated.AddRange(allObjs);




                SheetMethod_SheetMesh_MeshGeneration.SetSkyboxAndLighting();

                MusicManager.PlaySong(LoadedLevelData.levelMusicType);


            }



        }
    }

    static Vector3 GetCenterOfLevel(List<GameObject> allMeshes)
    {
        Vector3 ret = Vector3.zero;

        (float lowestX, float highestX) xSpan = GetAxisSpanOfAllSurfaceMeshes(allMeshes, E_WorldAxes.X);
        (float lowestY, float highestY) ySpan = GetAxisSpanOfAllSurfaceMeshes(allMeshes, E_WorldAxes.Y);
        (float lowestZ, float highestZ) zSpan = GetAxisSpanOfAllSurfaceMeshes(allMeshes, E_WorldAxes.Z);


        float xCenter = (float)(xSpan.lowestX + xSpan.highestX) / (float)2;
        float yCenter = (float)(ySpan.lowestY + ySpan.highestY) / (float)2;
        float zCenter = (float)(zSpan.lowestZ + zSpan.highestZ) / (float)2;

        return new Vector3(xCenter, yCenter, zCenter);
    }

    static void CreateCreationCam(List<GameObject> allMeshes, float camZoomOutMultiplier = 1.0f, float camRaise=0.0f,float spinSpeed=-20.0f)
    {
        if(FindResources() && FindResources_Entities())
        {
            (float lowestX, float highestX) xSpan = GetAxisSpanOfAllSurfaceMeshes(allMeshes, E_WorldAxes.X);


            Vector3 levelCenter = GetCenterOfLevel(allMeshes);

            float viewWidth = Mathf.Abs(xSpan.highestX - xSpan.lowestX)*camZoomOutMultiplier;


            Vector3 spawnPos_unadjusted = new Vector3(levelCenter.x, levelCenter.y, levelCenter.z);
            Vector3 spawnPos = new Vector3(spawnPos_unadjusted.x , spawnPos_unadjusted.y+ camRaise, spawnPos_unadjusted.z+ viewWidth);


            Vector3 lookDir = (spawnPos_unadjusted - spawnPos).normalized;


            GameObject camPivotInst = new GameObject();

            camPivotInst.transform.position = levelCenter;

            Spin camSpin = camPivotInst.AddComponent<Spin>();

            if (camSpin != null)
            {
                camSpin.SpinPower = new Vector3(0,spinSpeed, 0);
            }


            GameObject camInst= GameObject.Instantiate(creationCam_prefab, spawnPos, Quaternion.LookRotation(lookDir), camPivotInst.transform);

            objectsToCleanUpIfRepeated.Add(camPivotInst);
            objectsToCleanUpIfRepeated.Add(camInst);

        }

    }

    static void MakeAllEntitiesStill(List<GameObject> allEntities)
    {
        if(FindResources() && FindResources_Entities())
        {
            List<System.Type> componentWhitelist = new List<System.Type>
        {
            typeof(MeshRenderer),
            typeof(MeshFilter),
            typeof(SineMove),
            typeof(Spin),
            typeof(ModelControl)
        };


            foreach (GameObject ent in allEntities)
            {
                ExtendedFunctionality.GutComponentsFromGameObject(ent, componentWhitelist);

                ModelControl mdlEnt = ent.GetComponent<ModelControl>();

                if (mdlEnt != null)
                {
                    mdlEnt.IdleOverride = true;
                }
            }
        }

    }




    static void SetupMeshRepBySurfaceType(GameObject meshRepObj, E_ShapeSurfaceTypes surfaceType)
    {
        if(FindResources_Entities() && FindResources() )
        {
            switch (surfaceType)
            {
                case E_ShapeSurfaceTypes.Ground:
                    meshRepObj.layer = LayerMask.NameToLayer("EnemyBlockerSurface");
                    break;

                case E_ShapeSurfaceTypes.Hazard_Goop:
                case E_ShapeSurfaceTypes.Hazard_Lava:

                    Collider colScr = meshRepObj.GetComponent<Collider>();

                    if (colScr != null)
                    {
                        MeshCollider colScr_mesh = (MeshCollider)colScr;

                        if (colScr_mesh != null)
                        {
                            colScr_mesh.convex = true;
                        }
                        colScr.isTrigger = true;
                    }


                    break;
            }


            switch (surfaceType)
            {
                case E_ShapeSurfaceTypes.Hazard_Goop:
                    meshRepObj.AddComponent<HazardousGoop>();
                    break;
                case E_ShapeSurfaceTypes.Hazard_Lava:


                    WavyMesh waveScr = meshRepObj.AddComponent<WavyMesh>();

                    waveScr.WaveData = new SineData
                        (
                            Vector3.zero,
                            E_SineType.Sine,
                            E_WorldAxes.Y,
                            2,
                            2,
                            1
                        );


                    meshRepObj.AddComponent<DeathUponTouch>();


                    break;
                case E_ShapeSurfaceTypes.Ice:
                    meshRepObj.AddComponent<Ice>();

                    break;
            }
        }

    }



    static (float lowestUnit, float highestUnit) GetAxisSpanOfAllSurfaceMeshes(List<GameObject> surfaceMeshes,E_WorldAxes axisToSpan)
    {


        float lowestUnit = float.MaxValue;
        float highestUnit = float.MinValue;

        if (FindResources_Entities() && FindResources() )
        {
            foreach (GameObject mesh in surfaceMeshes)
            {


                float mesh_hM = ExtendedFunctionality.GetHalfMeasureOfGameObject(mesh, axisToSpan);
                

                float mesh_core = 0;



                Vector3? mesh_core_options_nullable = ExtendedFunctionality.AttemptWorldBoundCenterGet(mesh);

                if (mesh_core_options_nullable != null)
                {
                    Vector3 mesh_core_options = mesh_core_options_nullable??default(Vector3);
                    switch (axisToSpan)
                    {
                        case E_WorldAxes.X:
                            mesh_core = mesh_core_options.x;
                            break;
                        case E_WorldAxes.Y:
                            mesh_core = mesh_core_options.y;
                            break;
                        case E_WorldAxes.Z:
                            mesh_core = mesh_core_options.z;
                            break;
                    }


                    float mesh_lowest = mesh_core - mesh_hM;
                    float mesh_highest = mesh_core + mesh_hM;

                    if (mesh_lowest < lowestUnit)
                    {
                        lowestUnit = mesh_lowest;
                    }

                    if (mesh_highest > highestUnit)
                    {
                        highestUnit = mesh_highest;
                    }
                }



            }
        }

        return (lowestUnit, highestUnit);
    }

    static void SetEntityHeightToLevelAverage(List<GameObject> affectedEntities, (float lowestY, float highestY) refSpan)
    {
        if (FindResources_Entities() && FindResources() )
        {
            if (affectedEntities != null && affectedEntities.Count > 0)
            {



                float avgY = (float)(refSpan.lowestY + refSpan.highestY) / (float)2;

                foreach (GameObject ent in affectedEntities)
                {

                    Vector3 avgPos = new Vector3(ent.transform.position.x, avgY, ent.transform.position.z);

                    ExtendedFunctionality.DirectMoveCharacterController_FallbackIfNotPresent(ent, avgPos);


                }
            }
        }



    }

    static void SetupEntitiesIfNeeded(List<GameObject> allEntities,GameObject playerObj=null)
    {
        if (FindResources_Entities() && FindResources())
        {
            foreach (GameObject ent in allEntities)
            {
                IGeneratedEntityOrMeshInit[] allSpawnSetups = ent.GetComponents<MonoBehaviour>().OfType<IGeneratedEntityOrMeshInit>().ToArray();

                for (int i = 0; i < allSpawnSetups.Length; i++)
                {
                    allSpawnSetups[i].SetupEntityOrMesh(playerObj);


                }


            }
        }


    }

    //NOTE: the player is also rescaled during this function, they're just divided by their own scale making it always come out as vector3.one as it should.
    static void RescaleAroundPlayerEntity(Vector3 playerScale, List<GameObject> everyObj, CharacterController playerController=null)
    {

        if (FindResources_Entities() && FindResources())
        {

            if (playerController != null)
            {
                playerController.enabled = false;
            }

            List<Transform> oldPars = new List<Transform>();

            GameObject pivot = new GameObject();
            pivot.transform.position = Vector3.zero;


            foreach (GameObject obj in everyObj)
            {
                oldPars.Add(obj.transform.parent);
                obj.transform.parent = pivot.transform;
            }

            pivot.transform.localScale = new Vector3((float)pivot.transform.localScale.x / (float)playerScale.x, (float)pivot.transform.localScale.y / (float)playerScale.y, (float)pivot.transform.localScale.z / (float)playerScale.z); ;

            for (int i = 0; i < everyObj.Count; i++)
            {
                everyObj[i].transform.parent = oldPars[i];
            }



            ExtendedFunctionality.SafeDestroy(pivot);

            if (playerController != null)
            {
                playerController.enabled = true;
            }
        }



    }

    //returns a list of all the gO's that DIDNT move during this function (i.e., the ones not raycasting above any surface)
    static List<GameObject> RaiseEntitiesToGeometry(List<GameObject> affectedEntities)
    {


        List<GameObject> ret = new List<GameObject>();

        if (FindResources_Entities() && FindResources())
        {
            foreach (GameObject ent in affectedEntities)
            {
                if (ent != null)
                {
                    GenericRay ent_groundFinder = ent.AddComponent<GenericRay>();
                    ent_groundFinder.RayLength = 4000;
                    ent_groundFinder.CustomDirVector = Vector3.down;
                    ent_groundFinder.RayLayers = LayerMask.GetMask("Default","EnemyBlockerSurface");
                    ent_groundFinder.RayRoutine();



                    if (ent_groundFinder.RaySurface != null)
                    {

                        ExtendedFunctionality.DirectMoveCharacterController_FallbackIfNotPresent(ent, ent_groundFinder.RayHitPoint + new Vector3(0, ExtendedFunctionality.GetHalfMeasureOfGameObject(ent, E_WorldAxes.Y), 0));





                    }
                    else
                    {


                        ret.Add(ent);
                    }

                    GameObject.Destroy(ent_groundFinder);
                }
            }
        }

        return ret;
    }



    static (Texture2D levelAfterPatchOver, List<GameObject> spawnedEntities, GameObject playerEntity) GenerateEntities(Texture2D lvlPlan_tex, bool patchOverEntities=false)//returns the level plan resized to the pixel limit AFTER entities have been patched over if the patchOverEntities bool is true, otheriwse just returns the resized level plan.
    {
        if (FindResources_Entities() && FindResources())
        {

            //these are the entity types you're only allowed one of in a level plan
            List<E_EntityTypes> oneInstancePermittedTypes = new List<E_EntityTypes>
        {
            E_EntityTypes.Player

        };



            List<GameObject> spawnedEntities = new List<GameObject>();
            Texture2D levelPlan_file_copy = ResizeTexture_TruePointFiltering_Class.ResizeTexture_TruePointFiltering(lvlPlan_tex, (lvlPlan_tex.width, lvlPlan_tex.height));

            //SaveTexture2DToFile(levelPlan_file_copy, "/planResizes/", "levelPlan_forEntities");


            //unscaled level plan + conversion for entity shape generation

            ColourSetEntry[,] levelPlan_file_colourSet = LevelPlanToColourSet_Class.LevelPlanToColourSet(levelPlan_file_copy);



            List<SheetShape> allShapes_entities = GetAllShapesFromColourSet(levelPlan_file_colourSet, E_PixelType.Entity);


            List<SheetShape_EntityShape> allShapes_entities_typeConfirmed = new List<SheetShape_EntityShape>();

            foreach (SheetShape shape in allShapes_entities)
            {
                SheetShape_EntityShape shape_asType = (SheetShape_EntityShape)shape;

                if (shape_asType != null)
                {
                    allShapes_entities_typeConfirmed.Add(shape_asType);
                }
            }



            if (patchOverEntities)
            {
                foreach (SheetShape_EntityShape entityShape in allShapes_entities_typeConfirmed)
                {

                    E_ShapeSurfaceTypes? mostCommonSurfaceBelowMe = GetMostCommonSurfaceInColourSetInBounds(levelPlan_file_colourSet, entityShape.ShapeBounds);

                    if (mostCommonSurfaceBelowMe != null)
                    {


                        for (int i = 0; i < entityShape.BoolSet_PixelData.Count; i++)
                        {
                            for (int j = 0; j < entityShape.BoolSet_PixelData[i].Count; j++)
                            {
                                if (entityShape.BoolSet_PixelData[i][j])
                                {

                                    levelPlan_file_copy.SetPixel(i, j, ShapeSurfaceTypes_CommonFuncs.GetSurfaceColourBySurfaceType(mostCommonSurfaceBelowMe ?? default(E_ShapeSurfaceTypes)));
                                }
                            }
                        }
                    }
                }

                levelPlan_file_copy.Apply();

            }

            //the texture here is unaltered, this function with the false at the end just returns the scale divider if it WERE to be resized, needed for generatemodels later on
            (Texture2D regularSizeLevelPlan, float scaleDivideFactor) levelPlan_file_scaleIfResized = ResizeLevelPlanToBestFit(levelPlan_file_copy, levelPlan_PixelLimit, false);

            GameObject playerEnt = null;

            foreach (SheetShape_EntityShape entityShape in allShapes_entities_typeConfirmed)
            {
                bool oneInstancePermittedType_instancePermitted = !(entityCountByType.ContainsKey(entityShape.EntityType) && entityCountByType[entityShape.EntityType] > 0);
                if ((oneInstancePermittedType_instancePermitted && oneInstancePermittedTypes.Contains(entityShape.EntityType) )|| !oneInstancePermittedTypes.Contains(entityShape.EntityType))
                {
                    SheetShape_Bounds entityBounds = entityShape.ShapeBounds;
                    int worldPos_X = Mathf.RoundToInt(entityBounds.BottomRight.X - ((float)(entityBounds.BottomRight.X - entityBounds.TopLeft.X) / (float)2));
                    int worldPos_Y = Mathf.RoundToInt(entityBounds.BottomRight.Y - ((float)(entityBounds.BottomRight.Y - entityBounds.TopLeft.Y) / (float)2));

                    int worldPos_X_rescaled = Mathf.RoundToInt((float)worldPos_X / (float)levelPlan_file_scaleIfResized.scaleDivideFactor);
                    int worldPos_Y_rescaled = Mathf.RoundToInt((float)worldPos_Y / (float)levelPlan_file_scaleIfResized.scaleDivideFactor);

                    (int width, int height) entityScale = entityBounds.GetBoundsSize();
                    float entityScale_avg = (float)(entityScale.width + entityScale.height) / (float)2;
                    float entityScale_avg_rescaled = Mathf.RoundToInt((float)entityScale_avg / (float)levelPlan_file_scaleIfResized.scaleDivideFactor);

                    GameObject entity_new = SpawnEntityByType(entityShape.EntityType, new Vector3(worldPos_X_rescaled, 2000.0f, worldPos_Y_rescaled));

                    if (entity_new != null)
                    {
                        if (entityShape.EntityType == E_EntityTypes.Player)
                        {
                            playerEnt = entity_new;
                        }

                        if (entityCountByType.ContainsKey(entityShape.EntityType))
                        {
                            entityCountByType[entityShape.EntityType] += 1;
                        }
                        else
                        {
                            entityCountByType[entityShape.EntityType] = 1;
                        }

                        spawnedEntities.Add(entity_new);

                        entity_new.transform.localScale = new Vector3(entityScale_avg_rescaled, entityScale_avg_rescaled, entityScale_avg_rescaled);
                    }

                }

            }

            return (levelPlan_file_scaleIfResized.regularSizeLevelPlan, spawnedEntities, playerEnt);
        }
        else
        {
            return (lvlPlan_tex, new List<GameObject>(),null);
        }


    }

    static private GameObject SpawnEntityByType(E_EntityTypes entType, Vector3 spawnPos)
    {


        GameObject ret = null;

        if (FindResources_Entities() && FindResources())
        {
            switch (entType)
            {
                case E_EntityTypes.Player:
                    ret = GameObject.Instantiate(entity_player_prefab, spawnPos, Quaternion.Euler(Vector3.zero), null);

                    break;

                case E_EntityTypes.Finish:
                    ret = GameObject.Instantiate(entity_finish_prefab, spawnPos, Quaternion.Euler(Vector3.zero), null);

                    break;

                case E_EntityTypes.Collectable:
                    ret = GameObject.Instantiate(entity_collectable_prefab, spawnPos, Quaternion.Euler(Vector3.zero), null);



                    break;
                case E_EntityTypes.Enemy:
                    ret = GameObject.Instantiate(entity_enemy_prefab, spawnPos, Quaternion.Euler(Vector3.zero), null);


                    break;
            }
        }

        return ret;

    }


    static int GetMeshResolution((int x, int y) imageDimensions)
    {
        int meshResolution = Mathf.RoundToInt((float)imageDimensions.x/ (float)meshResolution_stepDownFromColourSetDivier);
        if (imageDimensions.y > imageDimensions.x)
        {
            meshResolution = Mathf.RoundToInt(imageDimensions.y / (float)meshResolution_stepDownFromColourSetDivier);

        }

        meshResolution = Mathf.Clamp(meshResolution, meshResolutionRange.lower, meshResolutionRange.higher);

        return meshResolution;
    }

    static List<GameObject> GenerateModels(Texture2D baseLevelPlan)
    {


        List<GameObject> spawnedMeshes = new List<GameObject>();

        if (FindResources_Entities() && FindResources())
        {
            //scaled level plan + conversion for surface shape generation
            (Texture2D resizedLevelPlan, float scaleDivideFactor) levelPlan_file_resized = ResizeLevelPlanToBestFit(baseLevelPlan, levelPlan_PixelLimit);

            //SaveTexture2DToFile(levelPlan_file_resized.resizedLevelPlan, "/planResizes/", "levelPlan_forMeshes");




            ColourSetEntry[,] levelPlan_file_resized_colourSet = LevelPlanToColourSet_Class.LevelPlanToColourSet(levelPlan_file_resized.resizedLevelPlan);



            List<SheetShape> allShapes_surfaces = GetAllShapesFromColourSet(levelPlan_file_resized_colourSet, E_PixelType.Surface);




            List<SheetShape_SurfaceShape> allShapes_surfaces_typeConfirmed = new List<SheetShape_SurfaceShape>();
            foreach (SheetShape shape in allShapes_surfaces)
            {
                SheetShape_SurfaceShape shape_asSurfaceType = (SheetShape_SurfaceShape)shape;
                if (shape_asSurfaceType != null)
                {
                    allShapes_surfaces_typeConfirmed.Add(shape_asSurfaceType);
                }
            }




            //generate meshes from the identified shapes
            for (int i = 0; i < allShapes_surfaces_typeConfirmed.Count; i++)
            {


                int meshResolution = GetMeshResolution((levelPlan_file_resized_colourSet.GetLength(0), levelPlan_file_resized_colourSet.GetLength(1)));


                //even though it is technically one mesh per shape, some shapes like the hazardous goop require a floor mesh to be rendered below it even so
                List<(Mesh mesh, List<Material> meshMats, E_ShapeSurfaceTypes meshSurfaceType)> generatedMeshes = new List<(Mesh mesh, List<Material> meshMats, E_ShapeSurfaceTypes meshSurfaceType)>();





                //individual surface type handling
                switch (allShapes_surfaces_typeConfirmed[i].SurfaceType)
                {


                    //floor mesh
                    case E_ShapeSurfaceTypes.Ground:
                    default:
                        generatedMeshes.Add(SheetMethod_SheetMesh_MeshGeneration.GenerateFloor(allShapes_surfaces_typeConfirmed[i], meshResolution, levelThickness, floorInsetPower, floorInsetRaise));


                        break;

                    //hazardous goop
                    case E_ShapeSurfaceTypes.Hazard_Goop:
                        generatedMeshes.Add(SheetMethod_SheetMesh_MeshGeneration.GenerateFloor(allShapes_surfaces_typeConfirmed[i], meshResolution, levelThickness, floorInsetPower, floorInsetRaise));
                        generatedMeshes.Add(SheetMethod_SheetMesh_MeshGeneration.GenerateHazardousGoop(allShapes_surfaces_typeConfirmed[i], meshResolution, levelThickness, goopThickness, (goopWavyInsetMinimum, goopWavyInsetMaxmimum)));

                        break;


                    case E_ShapeSurfaceTypes.Hazard_Lava:

                        float lavaHeight = Mathf.Abs(levelThickness) - ((float)Mathf.Abs(levelThickness) / (float)3);
                        generatedMeshes.Add(SheetMethod_SheetMesh_MeshGeneration.GenerateLava(allShapes_surfaces_typeConfirmed[i], meshResolution, lavaHeight));
                        break;



                    case E_ShapeSurfaceTypes.Ice:
                        generatedMeshes.Add(SheetMethod_SheetMesh_MeshGeneration.GenerateIce(allShapes_surfaces_typeConfirmed[i], meshResolution, levelThickness, (iceJaggedInsetMinimum, iceJaggedInsetMaximum)));

                        break;

                }

                //apply heightmap
                for (int j = 0; j < generatedMeshes.Count; j++)
                {

                    if (generatedMeshes[j].mesh != null)
                    {
                        (Mesh mesh, List<Material> meshMats, E_ShapeSurfaceTypes meshSurfaceType) mesh_heightmapAltered = ApplyHeightmap_Class.ApplyHeightmap(heightmap_tex, heightmap_power, generatedMeshes[j], levelPlan_file_resized.resizedLevelPlan);
                        generatedMeshes[j] = mesh_heightmapAltered;
                    }

                }


                foreach ((Mesh mesh, List<Material> meshMats, E_ShapeSurfaceTypes meshSurfaceType) genMesh in generatedMeshes)
                {

                    if (genMesh.mesh != null)
                    {
                        //mesh placed into world

                        GameObject meshRep = new GameObject();
                        meshRep.transform.position = Vector3.zero;

                        MeshFilter meshRep_filt = meshRep.AddComponent<MeshFilter>();
                        MeshRenderer meshRep_rend = meshRep.AddComponent<MeshRenderer>();
                        MeshCollider meshRep_coll = meshRep.AddComponent<MeshCollider>();
                        meshRep_filt.mesh = genMesh.mesh;
                        meshRep_coll.sharedMesh = meshRep_filt.sharedMesh;

                        //fsr you cant do the new material array instantiation directly on the renderer.materials property, just sets the mats to default no matter what, small bug i presume
                        Material[] rendMats = genMesh.meshMats.ToArray();

                        meshRep_rend.materials = rendMats;


                        string meshName = "meshRep_" + i.ToString();
                        meshRep.name = meshName;



                        SetupMeshRepBySurfaceType(meshRep, genMesh.meshSurfaceType);

                        spawnedMeshes.Add(meshRep);
                    }


                }






            }
        }

        return spawnedMeshes;

    }


    static E_ShapeSurfaceTypes? GetMostCommonSurfaceInColourSetInBounds(ColourSetEntry[,] colourSet, SheetShape_Bounds bounds)
    {
        E_ShapeSurfaceTypes? ret = null;

        if (FindResources_Entities() && FindResources() )
        {
            Dictionary<E_ShapeSurfaceTypes, int> commodityTracker = new Dictionary<E_ShapeSurfaceTypes, int>();
            int nothingCount = 0;
            int itCount = 0;
            for (int i = bounds.TopLeft.X; i < bounds.BottomRight.X + 1; i++)
            {
                for (int j = bounds.TopLeft.Y; j < bounds.BottomRight.Y + 1; j++)
                {

                    if (i >= 0 && i < colourSet.GetLength(0) && j >= 0 && j < colourSet.GetLength(1))
                    {
                        ColourSetEntry curColEntry = colourSet[i, j];

                        switch (curColEntry.pixelType)
                        {
                            case E_PixelType.Surface:


                                E_ShapeSurfaceTypes curColEntry_surfType_nonNullable = curColEntry.surfaceType ?? default(E_ShapeSurfaceTypes);
                                if (commodityTracker.ContainsKey(curColEntry_surfType_nonNullable))
                                {
                                    commodityTracker[curColEntry_surfType_nonNullable] += 1;
                                }
                                else
                                {
                                    commodityTracker[curColEntry_surfType_nonNullable] = 1;

                                }
                                break;
                            case E_PixelType.None:

                                nothingCount += 1;
                                break;

                        }


                    }
                    itCount += 1;
                }
            }


            int highestCommodity = int.MinValue;

            foreach (KeyValuePair<E_ShapeSurfaceTypes, int> entry in commodityTracker)
            {
                if (entry.Value > highestCommodity)
                {
                    highestCommodity = entry.Value;
                    ret = entry.Key;
                }
            }

            if (nothingCount > highestCommodity)
            {
                highestCommodity = nothingCount;
                ret = null;
            }
        }







        return ret;
    }


    
    static List<SheetShape> GetAllShapesFromColourSet(ColourSetEntry[,] colourSet, E_PixelType typeOfShape)
    {


        List<SheetShape> retShapes = new List<SheetShape>();

        if (FindResources_Entities() && FindResources())
        {
            //define all coords to run the breadth first search algorithm on
            List<(int X, int Y)> BFS_startingCoordinates = new List<(int X, int Y)>();


            //find all coords which have a colour different to the last one in the iteration and set that as a possible BFS starting coordinate
            for (int i = 0; i < colourSet.GetLength(0); i++)
            {



                for (int j = 0; j < colourSet.GetLength(1); j++)
                {
                    bool addCoord = false;

                    if(i==0 && j == 0)
                    {
                        addCoord = true;
                    }
                    else
                    {
                        if (j > 0 && colourSet[i, j].pixelColour != colourSet[i, j - 1].pixelColour)
                        {

                            addCoord = true;


                        }
                        else
                        {
                            if (j == 0)
                            {
                                if (i > 0 && colourSet[i, j].pixelColour != colourSet[i - 1, j].pixelColour)
                                {
                                    addCoord = true;
                                }
                            }
                        }
                    }



                    if (addCoord)
                    {

                        if (colourSet[i, j].pixelType == typeOfShape)
                        {
                            BFS_startingCoordinates.Add((i, j));

                        }
                    }

                }
            }









            //run the BFS algorithm on all BFS starting coordinates
            foreach ((int X, int Y) coord in BFS_startingCoordinates)
            {

                if (colourSet[coord.X, coord.Y].pixelType == typeOfShape)
                {

                    //determine if the shape has already been generated from running this iterator on another BFS starting point
                    bool shapeAlreadyExists = false;
                    foreach (SheetShape sS in retShapes)
                    {

                        if (sS != null && PixelExistsInCoordSet(sS.BoolSetToCoordSet(), coord.X, coord.Y))
                        {

                            shapeAlreadyExists = true;
                            break;
                        }


                    }



                    //create a new shape with breadth first search if this fill coord is not in any other shape
                    if (shapeAlreadyExists == false)
                    {
                        SheetShape foundShape = BreadthFirstSearch(colourSet, (coord.X, coord.Y));

                        switch (typeOfShape)
                        {
                            case E_PixelType.Surface:
                                retShapes.Add(new SheetShape_SurfaceShape(GetShapeBoundsFromShapePixels(foundShape), colourSet[coord.X, coord.Y].surfaceType ?? default(E_ShapeSurfaceTypes), foundShape.BoolSet_PixelData));

                                break;

                            case E_PixelType.Entity:
                                retShapes.Add(new SheetShape_EntityShape(GetShapeBoundsFromShapePixels(foundShape), colourSet[coord.X, coord.Y].entityType ?? default(E_EntityTypes), foundShape.BoolSet_PixelData));
                                break;

                            default:
                                retShapes.Add(new SheetShape(GetShapeBoundsFromShapePixels(foundShape), foundShape.BoolSet_PixelData));

                                break;
                        }

                    }


                }

            }
        }

        return retShapes;
    }
    





    static (Texture2D resizedLevelPlan, float scaleDivideFactor) ResizeLevelPlanToBestFit(Texture2D levelPlan, int dimensionLimit, bool actuallyResizeLevelPlan = true)
    {
        if (FindResources_Entities() && FindResources())
        {
            dimensionLimit = Mathf.Clamp(dimensionLimit, 1, int.MaxValue);
            int levelPlan_width = levelPlan.width;
            int levelPlan_Height = levelPlan.height;

            float scaleDivideFactor = (float)levelPlan_width / (float)dimensionLimit;

            if (levelPlan_Height > levelPlan_width)
            {
                scaleDivideFactor = (float)levelPlan_Height / (float)dimensionLimit;

            }

            if (actuallyResizeLevelPlan)
            {
                if (scaleDivideFactor > 1)
                {
                    levelPlan_width = Mathf.RoundToInt((float)levelPlan_width / (float)scaleDivideFactor);
                    levelPlan_Height = Mathf.RoundToInt((float)levelPlan_Height / (float)scaleDivideFactor);

                    return (ResizeTexture_TruePointFiltering_Class.ResizeTexture_TruePointFiltering(levelPlan, (levelPlan_width, levelPlan_Height)), scaleDivideFactor);

                }
                else
                {
                    return (levelPlan, 1);
                }
            }
            else
            {

                return (levelPlan, Mathf.Clamp(scaleDivideFactor, 1, scaleDivideFactor));
            }

        }
        else
        {
            return (levelPlan, 1);
        }



    }










    static bool PixelExistsInCoordSet(List<(int X, int Y)> coords, int x, int y)
    {


        bool ret = false;

        if (FindResources_Entities() && FindResources())
        {
            if (coords != null)
            {
                foreach ((int X, int Y) coord in coords)
                {
                    if (coord.X == x && coord.Y == y)
                    {
                        ret = true;
                        break;
                    }
                }
            }
        }

        return ret;
    }


    static SheetShape_Bounds GetShapeBoundsFromShapePixels(SheetShape pixels)
    {
        if (FindResources_Entities() && FindResources())
        {
            SheetShape_Bounds theBoundsOfAllBounds = new SheetShape_Bounds((int.MaxValue, int.MaxValue), (int.MinValue, int.MinValue), (0, 0));

            SheetShape_Bounds theBoundsOfAllBounds_origCopy = new SheetShape_Bounds((theBoundsOfAllBounds.TopLeft.X, theBoundsOfAllBounds.TopLeft.Y), (theBoundsOfAllBounds.BottomRight.X, theBoundsOfAllBounds.BottomRight.Y), (theBoundsOfAllBounds.BaseZeroOffset.X, theBoundsOfAllBounds.BaseZeroOffset.Y));//have to do a deep copy so it doesn't change through the method.

            bool incompleteData = false;

            if (pixels != null)
            {
                foreach ((int X, int Y) coord in pixels.BoolSetToCoordSet())
                {
                    if (coord.X > theBoundsOfAllBounds.BottomRight.X)
                    {
                        theBoundsOfAllBounds.BottomRight = (coord.X, theBoundsOfAllBounds.BottomRight.Y);
                    }

                    if (coord.X < theBoundsOfAllBounds.TopLeft.X)
                    {
                        theBoundsOfAllBounds.TopLeft = (coord.X, theBoundsOfAllBounds.TopLeft.Y);

                    }

                    if (coord.Y > theBoundsOfAllBounds.BottomRight.Y)
                    {
                        theBoundsOfAllBounds.BottomRight = (theBoundsOfAllBounds.BottomRight.X, coord.Y);

                    }

                    if (coord.Y < theBoundsOfAllBounds.TopLeft.Y)
                    {
                        theBoundsOfAllBounds.TopLeft = (theBoundsOfAllBounds.TopLeft.X, coord.Y);

                    }
                }

            }

            bool xBounds_invalid = theBoundsOfAllBounds.TopLeft.X == theBoundsOfAllBounds_origCopy.TopLeft.X || theBoundsOfAllBounds.BottomRight.X == theBoundsOfAllBounds_origCopy.BottomRight.X;
            bool yBounds_invalid = theBoundsOfAllBounds.TopLeft.Y == theBoundsOfAllBounds_origCopy.TopLeft.Y || theBoundsOfAllBounds.BottomRight.Y == theBoundsOfAllBounds_origCopy.BottomRight.Y;

            if (xBounds_invalid || yBounds_invalid)
            {
                incompleteData = true;
            }

            if (!incompleteData)
            {

                return theBoundsOfAllBounds;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }



    }


    static void SaveTexture2DToFile(Texture2D tex, string directory, string fileName = "tex2DToFile_output")
    {
        if (FindResources_Entities() && FindResources() )
        {
            byte[] writeFile_png = tex.EncodeToPNG();

            List<string> writeDir = new List<string> { Application.dataPath, directory.ToString() };

            string outputFolder = ExternalDirectories.EnsureFolderExists(writeDir);

            System.IO.File.WriteAllBytes(outputFolder + fileName + ".png", writeFile_png);
        }


    }


    static void DrawShapeToFile(SheetShape shape, string fileName = "DrawShapeToFile_output", SheetMesh_GridTile[,] gridTileMap=null)
    {
        if (FindResources_Entities() && FindResources())
        {
            if (shape != null && shape.ShapeBounds != null)
            {
                SheetShape_Bounds shapeToFile_dimensions = shape.ShapeBounds.BaseZeroCopy();

                Texture2D writeFile = new Texture2D(shapeToFile_dimensions.BottomRight.X + 1, shapeToFile_dimensions.BottomRight.Y + 1, TextureFormat.RGB24, false);//+1 because the coords count up from 0, so a shapeToFile_dimensions thats 86 indexes across has coords ranging from 0 to 85

                List<(int X, int Y)> writeCoords = shape.BoolSetToCoordSet(-shapeToFile_dimensions.BaseZeroOffset.X, -shapeToFile_dimensions.BaseZeroOffset.Y);


                foreach ((int X, int Y) coord in writeCoords)
                {
                    writeFile.SetPixel(coord.X, coord.Y, Color.red);
                }
                writeFile.Apply();

                SaveTexture2DToFile(writeFile, "/DrawShapeToFile_Output/", fileName);

            }

        }


    }










    static bool CanGetColourFromColourSet(ColourSetEntry[,] colourSet,(int X, int Y) coord)
    {
        if (FindResources_Entities() && FindResources())
        {
            bool xSafe = coord.X >= 0 && coord.X < colourSet.GetLength(0);
            bool ySafe = coord.Y >= 0 && coord.Y < colourSet.GetLength(1);

            if (xSafe && ySafe)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }


    }







    static bool BreadthFirstSearch_FillApplicable((int X, int Y) coord, List<List<bool>> boolSet, ColourSetEntry[,] colourSet, Color shapeFillColour)
    {
        bool ret = false;


        if (FindResources_Entities() && FindResources())
        {
            bool coord_valid = CanGetColourFromColourSet(colourSet, coord);

            if (coord_valid)
            {
                bool coord_sameCol = colourSet[coord.X, coord.Y].pixelColour == shapeFillColour;



                bool coord_alreadyFound = false;

                if (coord.X >= 0 && coord.X < boolSet.Count)
                {
                    if (coord.Y >= 0 && coord.Y < boolSet[coord.X].Count)
                    {
                        coord_alreadyFound = boolSet[coord.X][coord.Y];
                    }
                }

                if (coord_sameCol && !coord_alreadyFound)
                {
                    ret = true;
                }
            }
        }


        return ret;
    }





    
    static SheetShape BreadthFirstSearch(ColourSetEntry[,] colourSet, (int X, int Y) fillCoord)
    {




        SheetShape ret = new SheetShape(null, null);//the ?? is to convert nullable value types to non nullables

        if (FindResources_Entities() && FindResources())
        {
            bool fillCoord_valid = CanGetColourFromColourSet(colourSet, fillCoord);


            if (fillCoord_valid)
            {
                Color shapeFillColour = colourSet[fillCoord.X, fillCoord.Y].pixelColour;

                List<(int X, int Y)> pixelsToAssess = new List<(int X, int Y)>();

                (int X, int Y) startCoord = fillCoord;


                bool startCoord_fillValid = BreadthFirstSearch_FillApplicable(startCoord, ret.BoolSet_PixelData, colourSet, shapeFillColour);

                if (startCoord_fillValid)
                {
                    ret.ImplantCoordIntoBoolSet(startCoord);
                    pixelsToAssess.Add(startCoord);

                }

                List<(int X, int Y)> pixelsToAssess_nextIteration = new List<(int X, int Y)>();






                while (pixelsToAssess.Count > 0)
                {
                    pixelsToAssess_nextIteration.Clear();
                    foreach ((int X, int Y) coord in pixelsToAssess)
                    {



                        (int X, int Y) coord_up = (coord.X, coord.Y + 1);
                        (int X, int Y) coord_down = (coord.X, coord.Y - 1);
                        (int X, int Y) coord_left = (coord.X - 1, coord.Y);
                        (int X, int Y) coord_right = (coord.X + 1, coord.Y);




                        bool coord_fillValid_up = BreadthFirstSearch_FillApplicable(coord_up, ret.BoolSet_PixelData, colourSet, shapeFillColour);
                        bool coord_fillValid_down = BreadthFirstSearch_FillApplicable(coord_down, ret.BoolSet_PixelData, colourSet, shapeFillColour);
                        bool coord_fillValid_left = BreadthFirstSearch_FillApplicable(coord_left, ret.BoolSet_PixelData, colourSet, shapeFillColour);
                        bool coord_fillValid_right = BreadthFirstSearch_FillApplicable(coord_right, ret.BoolSet_PixelData, colourSet, shapeFillColour);

                        if (coord_fillValid_up)
                        {

                            pixelsToAssess_nextIteration.Add(coord_up);
                            ret.ImplantCoordIntoBoolSet(coord_up);
                        }

                        if (coord_fillValid_down)
                        {


                            pixelsToAssess_nextIteration.Add(coord_down);
                            ret.ImplantCoordIntoBoolSet(coord_down);

                        }

                        if (coord_fillValid_left)
                        {


                            pixelsToAssess_nextIteration.Add(coord_left);
                            ret.ImplantCoordIntoBoolSet(coord_left);

                        }

                        if (coord_fillValid_right)
                        {


                            pixelsToAssess_nextIteration.Add(coord_right);
                            ret.ImplantCoordIntoBoolSet(coord_right);

                        }


                    }

                    //in my head i dont see why i cant do the ret.addcoordtobooltable stuff right here but it gets stuck in a loop when i do.

                    //must copy elements one by one instead of doing '=' because i clear the nextIteration list at the start of the while loop, then clearing the regular assess list.
                    pixelsToAssess.Clear();

                    foreach ((int X, int Y) coord in pixelsToAssess_nextIteration)
                    {
                        pixelsToAssess.Add(coord);
                    }


                }

            }

        }


        return ret;
    }
    





    static bool FindResources_Entities()
    {
        bool ret = false;

        if (entity_player_prefab == null)
        {
            entity_player_prefab = Resources.Load<GameObject>("Entities/Player/player");
        }


        if (entity_finish_prefab == null)
        {
            entity_finish_prefab = Resources.Load<GameObject>("Entities/finish");
        }


        if (entity_collectable_prefab == null)
        {
            entity_collectable_prefab = Resources.Load<GameObject>("Entities/collectable");
        }

        if (entity_enemy_prefab == null)
        {
            entity_enemy_prefab = Resources.Load<GameObject>("Entities/Enemy/enemy");
        }

        if (entity_player_prefab != null && entity_finish_prefab!=null && entity_collectable_prefab!=null && entity_enemy_prefab!=null)
        {
            ret = true;
        }
        return ret;
    }

    static bool FindResources()
    {
        bool ret = false;

        if (creationCam_prefab == null)
        {
            creationCam_prefab = Resources.Load<GameObject>("cam_levelPreview");
        }

        if (creationCam_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
