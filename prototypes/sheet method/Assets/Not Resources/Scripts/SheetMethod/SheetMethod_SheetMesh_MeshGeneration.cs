using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public struct SheetMesh_ContextualizedVertex
{
    public Vector3 VertexPosition { get; set; }
    public (int X, int Y) VertexGridIndex { get; private set; }


    public SheetMesh_ContextualizedVertex(Vector3 pos, (int X, int Y) index)
    {
        VertexPosition = pos;
        VertexGridIndex = index;

    }
}

public abstract class SheetMethod_SheetMesh_MeshGeneration_Utilities : MonoBehaviour
{

    public static SheetMesh_ContextualizedVertex[] GetRangeOfContVerts(SheetMesh_ContextualizedVertex[] contVerts, List<int> permittedVerts)
    {
        SheetMesh_ContextualizedVertex[] ret = new SheetMesh_ContextualizedVertex[permittedVerts.Count];

        for(int i=0;i<permittedVerts.Count;i++)
        {
            ret[i] = contVerts[permittedVerts[i]];
        }
        return ret;
    }

    
    static List<(int X, int Y)> TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile(List<(int X, int Y)> coordSet, SheetMesh_GridTile tile)
    {
        List<(int X, int Y)> ret = new List<(int X, int Y)>();
        foreach ((int X, int Y) coord in coordSet)
        {
            bool xValid = coord.X >= tile.TopLeftCoord.X && coord.X < tile.TopLeftCoord.X + (tile.TileDimensions.tileWidth - 1);
            bool yValid = coord.Y >= tile.TopLeftCoord.Y && coord.Y < tile.TopLeftCoord.Y + (tile.TileDimensions.tileHeight - 1);
            if (xValid && yValid)
            {
                ret.Add(coord);
            }
        }

        return ret;
    }
    

    //really puts the "sheet method" in "sheet method"!
    public static SheetMesh_ContextualizedVertex[] TurnShapeIntoBaseContextualizedVertices(SheetShape baseShape, int resolution, bool lockSystemEmulation = false, bool moveCoords = true, bool onlyEmptySpaces = false)
    {

        List<SheetMesh_ContextualizedVertex> ret_list = new List<SheetMesh_ContextualizedVertex>();




        //define grid based on resolution and boundaries
        resolution = Mathf.Clamp(resolution, 2, int.MaxValue);//a value of 1 gives a completely unmodelled mesh because a grid tile cannot morph its vertices on an integer grid in a 1x1 area



        SheetShape_Bounds vertexGrid_Calc = baseShape.ShapeBounds.BaseZeroCopy();


        (int gridWidth, int gridHeight) vertexGrid_dimensions = (vertexGrid_Calc.BottomRight.X + 1, vertexGrid_Calc.BottomRight.Y + 1);//+1 because the coords count up from 0, so a shape thats 86 pixels across has coords ranging from 0 to 85

        //this line not only converts a boolset sheetshape to a coordset, but it also normalizes the coordinates of pixels in the shape with base 0,0 since it's easier to work in for this function.
        List<(int X, int Y)> boolSet_pixelData_occuppiedPixels = baseShape.BoolSetToCoordSet(-vertexGrid_Calc.BaseZeroOffset.X, -vertexGrid_Calc.BaseZeroOffset.Y);



        SheetMesh_GridTile[,] vertexGrid_tiles = new SheetMesh_GridTile[Mathf.CeilToInt((float)vertexGrid_dimensions.gridWidth / (float)resolution)+1, Mathf.CeilToInt((float)vertexGrid_dimensions.gridHeight / (float)resolution)+1];


        //check for more than length 1 because a grid with 1 tile will only have the upper left corner of the tile to work with for generation
        //this also works as effective prevention of anti aliasing confusing the system
        if(vertexGrid_tiles.GetLength(0)>1 && vertexGrid_tiles.GetLength(1) > 1)
        {
            for (int i = 0; i < vertexGrid_tiles.GetLength(0); i++)
            {
                for (int j = 0; j < vertexGrid_tiles.GetLength(1); j++)
                {
                    (int tileWidth, int tileHeight) tileDimensions = (resolution, resolution);

                    (int pixelX, int pixelY) pixelIndex = (i * resolution, j * resolution);


                    if (pixelIndex.pixelX + resolution > vertexGrid_dimensions.gridWidth)
                    {
                        tileDimensions = (vertexGrid_dimensions.gridWidth - pixelIndex.pixelX, resolution);
                    }

                    if (pixelIndex.pixelY + resolution > vertexGrid_dimensions.gridHeight)
                    {
                        tileDimensions = (resolution, vertexGrid_dimensions.gridHeight - pixelIndex.pixelY);
                    }

                    vertexGrid_tiles[i, j] = new SheetMesh_GridTile(pixelIndex, tileDimensions);

                }
            }





            //determine square types (white space = delete, black space = ignore, mix space = actively modified in generation)



            for (int i = 0; i < vertexGrid_tiles.GetLength(0); i++)
            {
                for (int j = 0; j < vertexGrid_tiles.GetLength(1); j++)
                {
                    SheetMesh_GridTile tile = vertexGrid_tiles[i, j];

                    int spacesTaken = TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile(boolSet_pixelData_occuppiedPixels, tile).Count;



                    if (spacesTaken <= 0)
                    {
                        tile.GridTileType = E_SheetMesh_GridTileType.Blank;

                    }
                    else if (spacesTaken > 0 && spacesTaken < tile.TileDimensions.tileWidth * tile.TileDimensions.tileHeight)//the subtraction at the end is the same as resolution^2 for most grid squares, this is to account for the ones that get cut off
                    {
                        tile.GridTileType = E_SheetMesh_GridTileType.Mixed;

                    }
                    else
                    {
                        tile.GridTileType = E_SheetMesh_GridTileType.Full;

                    }
                }
            }





            //i would remove blank tiles here but itll be easier to just ignore them when the actual mesh generation comes to happen




            List<(int X, int Y)> filledCoordsInMyTile = new List<(int X, int Y)>();

            List<(int gridIndex_X, int gridIndex_Y, int movePos_X, int movePos_Y, int influences)> moveTileInfluences = new List<(int gridIndex_X, int gridIndex_Y, int movePos_X, int movePos_Y, int influences)>();

            List<(int gridIndex_X, int gridIndex_Y)> vertexGrid_tiles_moveInfluenceIndexesToAssess = new List<(int gridIndex_X, int gridIndex_Y)>();


            if (moveCoords)
            {
                //morph mixed tiles & ignore full tiles
                for (int i = 0; i < vertexGrid_tiles.GetLength(0); i++)
                {
                    for (int j = 0; j < vertexGrid_tiles.GetLength(1); j++)
                    {
                        filledCoordsInMyTile.Clear();
                        vertexGrid_tiles_moveInfluenceIndexesToAssess.Clear();

                        (int X, int Y) tileIndex = (i, j);
                        SheetMesh_GridTile tile = vertexGrid_tiles[tileIndex.X, tileIndex.Y];

                        if (tile.GridTileType == E_SheetMesh_GridTileType.Mixed)
                        {


                            //find grid coords to check the movement influence of in this tile
                            vertexGrid_tiles_moveInfluenceIndexesToAssess.Add(tileIndex);


                            bool i_inRange = i + 1 < vertexGrid_tiles.GetLength(0);
                            bool j_inRange = j + 1 < vertexGrid_tiles.GetLength(1);

                            if (i_inRange)
                            {


                                vertexGrid_tiles_moveInfluenceIndexesToAssess.Add((i + 1, j));
                            }

                            if (j_inRange)
                            {

                                vertexGrid_tiles_moveInfluenceIndexesToAssess.Add((i, j + 1));

                            }

                            if (i_inRange && j_inRange)
                            {


                                vertexGrid_tiles_moveInfluenceIndexesToAssess.Add((i + 1, j + 1));


                            }


                            //find filled pixels + calculate movement influences
                            filledCoordsInMyTile = TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile(boolSet_pixelData_occuppiedPixels, tile);

                            foreach ((int gridX, int gridY) moveTileIndex in vertexGrid_tiles_moveInfluenceIndexesToAssess)
                            {
                                SheetMesh_GridTile moveTile = vertexGrid_tiles[moveTileIndex.gridX, moveTileIndex.gridY];
                                float nearestRealCoord_dist = float.MaxValue;
                                (int X, int Y) nearestRealCoord_coord = (-1, -1);

                                foreach ((int X, int Y) realCoord in filledCoordsInMyTile)
                                {
                                    float newDist = Vector2.Distance(new Vector2(moveTile.TopLeftCoord.X, moveTile.TopLeftCoord.Y), new Vector2(realCoord.X, realCoord.Y));

                                    if (newDist < nearestRealCoord_dist)
                                    {
                                        nearestRealCoord_dist = newDist;
                                        nearestRealCoord_coord = realCoord;

                                    }
                                }

                                int? moveInfluenceEntry_existIndex = null;
                                for (int k = 0; k < moveTileInfluences.Count; k++)
                                {

                                    if (moveTileInfluences[k].gridIndex_X == moveTileIndex.gridX && moveTileInfluences[k].gridIndex_Y == moveTileIndex.gridY)
                                    {

                                        moveInfluenceEntry_existIndex = k;
                                        break;
                                    }
                                }

                                (int gridIndex_X, int gridIndex_Y, int movePos_X, int movePos_Y, int influences) influenceNew = (moveTileIndex.gridX, moveTileIndex.gridY, nearestRealCoord_coord.X, nearestRealCoord_coord.Y, 1);

                                if (lockSystemEmulation == false)
                                {
                                    if (moveInfluenceEntry_existIndex != null)
                                    {
                                        (int gridIndex_X, int gridIndex_Y, int movePos_X, int movePos_Y, int influences) moveInfluenceEntry = moveTileInfluences[moveInfluenceEntry_existIndex ?? default(int)];
                                        moveInfluenceEntry.movePos_X += nearestRealCoord_coord.X;
                                        moveInfluenceEntry.movePos_Y += nearestRealCoord_coord.Y;
                                        moveInfluenceEntry.influences += 1;
                                        moveTileInfluences[moveInfluenceEntry_existIndex ?? default(int)] = moveInfluenceEntry;

                                    }
                                    else
                                    {
                                        moveTileInfluences.Add(influenceNew);

                                    }

                                }
                                else
                                {
                                    if (moveInfluenceEntry_existIndex == null)
                                    {
                                        moveTileInfluences.Add(influenceNew);

                                    }

                                }


                            }

                        }
                    }
                }


            }






            //actually act on the move influence calculations
            foreach ((int gridIndex_X, int gridIndex_Y, int movePos_X, int movePos_Y, int influences) moveInfluenceEntry in moveTileInfluences)
            {
                int newXPos = Mathf.RoundToInt((float)moveInfluenceEntry.movePos_X / (float)moveInfluenceEntry.influences);
                int newYPos = Mathf.RoundToInt((float)moveInfluenceEntry.movePos_Y / (float)moveInfluenceEntry.influences);



                vertexGrid_tiles[moveInfluenceEntry.gridIndex_X, moveInfluenceEntry.gridIndex_Y].TopLeftCoord = (newXPos, newYPos);
            }





            for (int y = 0; y < vertexGrid_tiles.GetLength(1); y++)
            {

                for (int x = 0; x < vertexGrid_tiles.GetLength(0); x++)
                {
                    if (x + 1 < vertexGrid_tiles.GetLength(0) && y + 1 < vertexGrid_tiles.GetLength(1))
                    {
                        SheetMesh_GridTile checkTile = vertexGrid_tiles[x, y];


                        bool emptyCheck = (onlyEmptySpaces == false && checkTile.GridTileType != E_SheetMesh_GridTileType.Blank) || (onlyEmptySpaces == true && checkTile.GridTileType == E_SheetMesh_GridTileType.Blank);


                        if (emptyCheck)
                        {
                            TurnShapeIntoBaseVertices_AddToRet(vertexGrid_tiles, (x, y), ret_list, (vertexGrid_Calc.BaseZeroOffset.X, vertexGrid_Calc.BaseZeroOffset.Y));
                            TurnShapeIntoBaseVertices_AddToRet(vertexGrid_tiles, (x + 1, y), ret_list, (vertexGrid_Calc.BaseZeroOffset.X, vertexGrid_Calc.BaseZeroOffset.Y));
                            TurnShapeIntoBaseVertices_AddToRet(vertexGrid_tiles, (x, y + 1), ret_list, (vertexGrid_Calc.BaseZeroOffset.X, vertexGrid_Calc.BaseZeroOffset.Y));
                            TurnShapeIntoBaseVertices_AddToRet(vertexGrid_tiles, (x + 1, y + 1), ret_list, (vertexGrid_Calc.BaseZeroOffset.X, vertexGrid_Calc.BaseZeroOffset.Y));
                        }

                    }
                }

            }
        }








        return ret_list.ToArray();
    }



    private static void TurnShapeIntoBaseVertices_AddToRet(SheetMesh_GridTile[,] grid, (int X, int Y) grid_coord, List<SheetMesh_ContextualizedVertex> ret, (int X, int Y) grid_coord_positionOffset)
    {
        SheetMesh_GridTile curTile = grid[grid_coord.X, grid_coord.Y];


        Vector3 coord_3D = new Vector3(curTile.TopLeftCoord.X + grid_coord_positionOffset.X, 0.0f, curTile.TopLeftCoord.Y + grid_coord_positionOffset.Y);
        ret.Add(new SheetMesh_ContextualizedVertex(coord_3D, (grid_coord.X, grid_coord.Y)));
    }








    public static List<int> MeshData_FindAllTriIndexesWithSharedVerts(List<(int vertA, int vertB, int vertC)> tris, int tris_searchCandidate)
    {
        List<int> ret = new List<int>();
        (int vertA, int vertB, int vertC) tri_cur = tris[tris_searchCandidate];

        for (int i = 0; i < tris.Count; i++)
        {
            (int vertA, int vertB, int vertC) tri = tris[i];
            bool vertA_shared = tri.vertA == tri_cur.vertA || tri.vertB == tri_cur.vertA || tri.vertC == tri_cur.vertA;
            bool vertB_shared = tri.vertA == tri_cur.vertB || tri.vertB == tri_cur.vertB || tri.vertC == tri_cur.vertB;
            bool vertC_shared = tri.vertA == tri_cur.vertC || tri.vertB == tri_cur.vertC || tri.vertC == tri_cur.vertC;

            if ((vertA_shared || vertB_shared || vertC_shared) && !ret.Contains(i) && i != tris_searchCandidate)
            {
                ret.Add(i);
            }
        }

        return ret;
    }


    //this doesnt work rn fsr, at least not in the way i want it to
    public static (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) MeshData_UniformCullingByIterativeDotComparison((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData, float compareMargin)
    {


        (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_ret = meshData;



        for (int i = 0; i < meshData_ret.tris.Count; i++)
        {



            List<int> linkedTriIndices = MeshData_FindAllTriIndexesWithSharedVerts(meshData_ret.tris, i);

            if (linkedTriIndices.Count > 0)
            {
                Vector3 tri_normal = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetNormalFromContextualizedTri(meshData_ret, i);

                Vector3 tri_normal_compare = Vector3.zero;

                foreach (int ind in linkedTriIndices)
                {
                    tri_normal_compare += SheetMethod_SheetMesh_MeshGeneration_Utilities.GetNormalFromContextualizedTri(meshData_ret, ind);
                }
                //used to divide by linkedTriIndices.count to get an average here but normalizing when all the normals are added together will do just as well.
                tri_normal_compare = tri_normal_compare.normalized;

                float normalCompare = Vector3.Dot(tri_normal, tri_normal_compare);


                if (normalCompare < compareMargin)
                {


                    int vertA_temp = meshData_ret.tris[i].vertA;
                    int vertC_temp = meshData_ret.tris[i].vertC;

                    meshData_ret.tris[i] = (vertC_temp, meshData_ret.tris[i].vertB, vertA_temp);




                }
            }



        }

        return meshData_ret;
    }







    //this function shouldn't need to exist, but right now it is a band-aid solution to a design flaw in the sheet method implementation where in extreme tile alteration, sometimes vertices can go through over vertices and flip the winding of the triangles it is a part of
    public static (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) MeshData_UniformCullingOnFlatMeshes((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData)
    {
        (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_ret = meshData;

        List<int> upNormalTris_indexes = new List<int>();
        List<int> downNormalTris_indexes = new List<int>();
        for (int i = 0; i < meshData_ret.tris.Count; i++)
        {
            Vector3 triNormal = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetNormalFromContextualizedTri(meshData_ret, i);

            if (triNormal.y > 0)
            {
                upNormalTris_indexes.Add(i);
            }
            else
            {
                downNormalTris_indexes.Add(i);

            }

        }

        List<int> iterateIndexes = null;

        if (upNormalTris_indexes.Count > 0 && downNormalTris_indexes.Count > 0)
        {
            if (upNormalTris_indexes.Count > downNormalTris_indexes.Count)
            {
                iterateIndexes = downNormalTris_indexes;

            }
            else
            {
                iterateIndexes = upNormalTris_indexes;
            }

            foreach (int itIndex in iterateIndexes)
            {
                (int vertA, int vertB, int vertC) tri_alter = meshData.tris[itIndex];


                int vertA_temp = tri_alter.vertA;
                int vertC_temp = tri_alter.vertC;

                tri_alter.vertA = vertC_temp;
                tri_alter.vertC = vertA_temp;

                meshData.tris[itIndex] = tri_alter;
            }
        }

        return meshData_ret;
    }

    public static bool Vertices_AreBasicallyTheSame(Vector3 vertPosA, Vector3 vertPosB, float marginOfError = 0.001f)
    {
        return Vector3.Distance(vertPosA, vertPosB) <= marginOfError;
    }

    public static (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) MeshData_MergeVerticesByPosition((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData)
    {
        List<int> equalIndexes = new List<int>();
        for (int i = 0; i < meshData.verts.Length; i++)
        {
            equalIndexes.Clear();
            Vector3 curVertPos = meshData.verts[i].VertexPosition;
            for (int j = 0; j < meshData.verts.Length; j++)
            {
                Vector3 compareVertPos = meshData.verts[j].VertexPosition;
                if (i != j && Vertices_AreBasicallyTheSame(curVertPos, compareVertPos))
                {
                    equalIndexes.Add(j);
                }
            }

            for (int j = 0; j < meshData.tris.Count; j++)
            {
                (int vertA, int vertB, int vertC) tri_cur = meshData.tris[j];


                if (equalIndexes.Contains(tri_cur.vertA))
                {
                    tri_cur.vertA = i;
                }

                if (equalIndexes.Contains(tri_cur.vertB))
                {
                    tri_cur.vertB = i;
                }

                if (equalIndexes.Contains(tri_cur.vertC))
                {
                    tri_cur.vertC = i;
                }

                meshData.tris[j] = tri_cur;
            }
        }

        return meshData;
    }

    public static void ContextualizedVerts_OffsetVerts(SheetMesh_ContextualizedVertex[] verts, Vector3 offset, SheetMesh_ContextualizedVertex[] permittedVerts=null)
    {
        for (int i = 0; i < verts.Length; i++)
        {
            bool vertPermitted = true;
            if (permittedVerts != null)
            {
                vertPermitted = false;

                for(int j = 0; j < permittedVerts.Length; j++)
                {
                    if (permittedVerts[j].VertexGridIndex == verts[i].VertexGridIndex)
                    {
                        vertPermitted = true;
                        break;
                    }
                }
            }
            if (vertPermitted)
            {
                verts[i].VertexPosition += offset;

            }
        }
    }

    public static SheetMesh_ContextualizedVertex[] ContextualizedVerts_CopyVerts(SheetMesh_ContextualizedVertex[] orig)
    {
        SheetMesh_ContextualizedVertex[] ret = new SheetMesh_ContextualizedVertex[orig.Length];

        for (int i = 0; i < orig.Length; i++)
        {
            ret[i] = new SheetMesh_ContextualizedVertex(orig[i].VertexPosition, orig[i].VertexGridIndex);
        }
        return ret;
    }

    public static List<(int vertA, int vertB, int vertC)> FlipTris(List<(int vertA, int vertB, int vertC)> tris, bool returnNew = false)
    {
        List<(int vertA, int vertB, int vertC)> ret = tris;
        if (returnNew)
        {
            ret = new List<(int vertA, int vertB, int vertC)>();

            foreach ((int vertA, int vertB, int vertC) tri in tris)
            {
                ret.Add(tri);
            }
        }

        for (int i = 0; i < ret.Count; i++)
        {
            (int vertA, int vertB, int vertC) tri_current_orig = ret[i];

            int vertA_new = tri_current_orig.vertC;
            int vertC_new = tri_current_orig.vertA;

            ret[i] = (vertA_new, tris[i].vertB, vertC_new);
        }
        return ret;
    }

    private static Vector2[] GenerateTopDownUVsFromVertices_Internal(Vector3[] verts, bool evenProportions = false)
    {
        //evenproportions: textures that need to look squarely mapped wont be stretched along one axis
        //worldStandardSizing: make the uv coords relative to a constant scale no matter the span of the vertices

        Vector2[] ret = new Vector2[verts.Length];

        float vert_lowestX = float.MaxValue;
        float vert_lowestZ = float.MaxValue;

        float vert_highestX = float.MinValue;
        float vert_highestZ = float.MinValue;




        for (int i = 0; i < verts.Length; i++)
        {
            Vector3 vertPos = verts[i];

            if (vertPos.x < vert_lowestX)
            {
                vert_lowestX = vertPos.x;
            }

            if (vertPos.x > vert_highestX)
            {
                vert_highestX = vertPos.x;
            }

            if (vertPos.z < vert_lowestZ)
            {
                vert_lowestZ = vertPos.z;
            }

            if (vertPos.z > vert_highestZ)
            {
                vert_highestZ = vertPos.z;
            }
        }






        for (int i = 0; i < ret.Length; i++)
        {
            Vector3 vertPos = verts[i];

            float UV_U = Mathf.Clamp01(Mathf.InverseLerp(vert_lowestX, vert_highestX, vertPos.x));
            float UV_V = Mathf.Clamp01(Mathf.InverseLerp(vert_lowestZ, vert_highestZ, vertPos.z));



            ret[i] = new Vector2(UV_U, UV_V);
        }

        if (evenProportions)
        {
            float xSpan = Mathf.Abs(vert_lowestX - vert_highestX);
            float zSpan = Mathf.Abs(vert_lowestZ - vert_highestZ);

            float proportionSquashFactor = 1.0f;
            if (xSpan > zSpan)
            {
                proportionSquashFactor = (float)zSpan / (float)xSpan;

                for (int i = 0; i < ret.Length; i++)
                {
                    ret[i] = new Vector2(ret[i].x, ret[i].y * proportionSquashFactor);

                }
            }
            else
            {
                proportionSquashFactor = (float)xSpan / (float)zSpan;


                for (int i = 0; i < ret.Length; i++)
                {
                    ret[i] = new Vector2(ret[i].x * proportionSquashFactor, ret[i].y);

                }
            }
        }

        return ret;
    }

    public static Vector2[] GenerateTopDownUVsFromVertices(SheetMesh_ContextualizedVertex[] verts, bool evenProportions = false)
    {
        Vector3[] verts_internal = new Vector3[verts.Length];

        for (int i = 0; i < verts.Length; i++)
        {
            verts_internal[i] = verts[i].VertexPosition;
        }

        return GenerateTopDownUVsFromVertices_Internal(verts_internal, evenProportions);
    }

    public static Vector2[] GenerateTopDownUVsFromVertices(Vector3[] verts, bool evenProportions = false)
    {

        return GenerateTopDownUVsFromVertices_Internal(verts, evenProportions);
    }



    public static (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) CombineMeshData(List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> multipleMeshDatas)
    {
        (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) ret;

        int vertsLen = 0;
        for (int i = 0; i < multipleMeshDatas.Count; i++)
        {
            vertsLen += multipleMeshDatas[i].verts.Length;
        }

        ret.verts = new SheetMesh_ContextualizedVertex[vertsLen];

        ret.tris = new List<(int vertA, int vertB, int vertC)>();


        int vertIndexOffset = 0;
        for (int i = 0; i < multipleMeshDatas.Count; i++)
        {
            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) curMeshData = multipleMeshDatas[i];

            foreach ((int vertA, int vertB, int vertC) contextTri in curMeshData.tris)
            {
                ret.tris.Add((contextTri.vertA + vertIndexOffset, contextTri.vertB + vertIndexOffset, contextTri.vertC + vertIndexOffset));
            }

            int vertIndexOffset_postAdd = 0;
            for (int j = 0; j < curMeshData.verts.Length; j++)
            {


                ret.verts[j + vertIndexOffset] = curMeshData.verts[j];
                vertIndexOffset_postAdd += 1;
            }
            vertIndexOffset += vertIndexOffset_postAdd;

        }
        return ret;
    }

    private static List<SheetMesh_ContextualizedVertex> GetNeighbourVerts(SheetMesh_ContextualizedVertex[] contVerts, int vertIndex)
    {
        vertIndex = Mathf.Clamp(vertIndex, 0, contVerts.Length - 1);
        List<SheetMesh_ContextualizedVertex> neighbourVerts = new List<SheetMesh_ContextualizedVertex>();

        SheetMesh_ContextualizedVertex vert_current = contVerts[vertIndex];
        for (int j = 0; j < contVerts.Length; j++)
        {

            SheetMesh_ContextualizedVertex vert_compare = contVerts[j];


            int neighbourDir_up = vert_current.VertexGridIndex.Y + 1;
            int neighbourDir_down = vert_current.VertexGridIndex.Y - 1;
            int neighbourDir_left = vert_current.VertexGridIndex.X - 1;
            int neighbourDir_right = vert_current.VertexGridIndex.X + 1;
            int neighbourDir_sameX = vert_current.VertexGridIndex.X;
            int neighbourDir_sameY = vert_current.VertexGridIndex.Y;

            bool neighbour_up = vert_compare.VertexGridIndex == (neighbourDir_sameX, neighbourDir_up);
            bool neighbour_down = vert_compare.VertexGridIndex == (neighbourDir_sameX, neighbourDir_down);
            bool neighbour_left = vert_compare.VertexGridIndex == (neighbourDir_left, neighbourDir_sameY);
            bool neighbour_right = vert_compare.VertexGridIndex == (neighbourDir_right, neighbourDir_sameY);

            bool neighbour_upLeft = vert_compare.VertexGridIndex == (neighbourDir_left, neighbourDir_up);
            bool neighbour_upRight = vert_compare.VertexGridIndex == (neighbourDir_right, neighbourDir_up);

            bool neighbour_downLeft = vert_compare.VertexGridIndex == (neighbourDir_left, neighbourDir_down);
            bool neighbour_downRight = vert_compare.VertexGridIndex == (neighbourDir_right, neighbourDir_down);

            bool isANeighbour = neighbour_up || neighbour_down || neighbour_left || neighbour_right || neighbour_upLeft || neighbour_upRight || neighbour_downLeft || neighbour_downRight;

            bool neighbourRegistered = false;

            foreach (SheetMesh_ContextualizedVertex vert in neighbourVerts)
            {
                if (vert.VertexGridIndex == vert_compare.VertexGridIndex)
                {
                    neighbourRegistered = true;
                    break;
                }
            }


            if (isANeighbour && !neighbourRegistered)
            {
                neighbourVerts.Add(vert_compare);
            }
        }
        return neighbourVerts;
    }

    public static SheetMesh_ContextualizedVertex[] ContVertSetA_Positions_To_ContVertSetB_Positions_WhenGridIndexMatches(SheetMesh_ContextualizedVertex[] contVertSetA, SheetMesh_ContextualizedVertex[] contVertSetB, List<int> contVertSetA_includeIndexes = null)
    {

        SheetMesh_ContextualizedVertex[] ret = new SheetMesh_ContextualizedVertex[contVertSetA.Length];

        for (int i = 0; i < contVertSetA.Length; i++)
        {
            SheetMesh_ContextualizedVertex vert_cur = contVertSetA[i];

            bool vertPermitted = false;

            if (contVertSetA_includeIndexes != null)
            {
                foreach (int vertInd in contVertSetA_includeIndexes)
                {
                    if (vertInd == i)
                    {
                        vertPermitted = true;
                        break;
                    }
                }
            }
            else
            {
                vertPermitted = true;
            }

            if (vertPermitted)
            {
                for (int j = 0; j < contVertSetB.Length; j++)
                {
                    if (contVertSetB[j].VertexGridIndex == vert_cur.VertexGridIndex)
                    {
                        vert_cur.VertexPosition = contVertSetB[j].VertexPosition;
                        break;
                    }
                }
            }

            ret[i] = vert_cur;
        }
        return ret;
    }


    public static SheetMesh_ContextualizedVertex[] PushContextualizedVerts(SheetMesh_ContextualizedVertex[] contVerts, float pushPower, SheetMesh_ContextualizedVertex[] permittedVerts = null)
    {
        SheetMesh_ContextualizedVertex[] ret = new SheetMesh_ContextualizedVertex[contVerts.Length];


        for (int i = 0; i < contVerts.Length; i++)
        {
            bool permitIteration = permittedVerts == null || (permittedVerts != null && FindGridPositionInArrayOfContextualVerts(contVerts[i].VertexGridIndex, permittedVerts) != null);

            if (permitIteration)
            {
                List<SheetMesh_ContextualizedVertex> neighbourVerts = GetNeighbourVerts(contVerts, i);



                Vector3 pushDir = Vector3.zero;

                foreach (SheetMesh_ContextualizedVertex vert in neighbourVerts)
                {
                    pushDir += contVerts[i].VertexPosition - vert.VertexPosition;
                }

                pushDir = pushDir.normalized;

                ret[i] = new SheetMesh_ContextualizedVertex(contVerts[i].VertexPosition + (pushDir * pushPower), contVerts[i].VertexGridIndex);
            }
            else
            {
                ret[i] = new SheetMesh_ContextualizedVertex(contVerts[i].VertexPosition, contVerts[i].VertexGridIndex);
            }



        }

        return ret;
    }

    public static SheetMesh_ContextualizedVertex[] GetOuterLoopsOfContextualizedVerts(SheetMesh_ContextualizedVertex[] contVerts)
    {


        List<SheetMesh_ContextualizedVertex> openEdgeVerts = new List<SheetMesh_ContextualizedVertex>();


        for (int i = 0; i < contVerts.Length; i++)
        {

            List<SheetMesh_ContextualizedVertex> neighbourVerts = GetNeighbourVerts(contVerts, i);



            if (neighbourVerts.Count < 8)
            {
                openEdgeVerts.Add(contVerts[i]);

            }
        }

        return openEdgeVerts.ToArray();
    }

    private static int? FindGridPositionInArrayOfContextualVerts((int gridX, int gridY) gridPos, SheetMesh_ContextualizedVertex[] verts, bool oneResultOnly = true)
    {
        int? ret = null;

        foreach (SheetMesh_ContextualizedVertex vert in verts)
        {
            if (vert.VertexGridIndex == gridPos)
            {
                if (ret == null)
                {
                    ret = 0;
                }
                else
                {
                    ret += 1;
                }

                if (oneResultOnly)
                {
                    break;

                }
            }
        }
        return ret;
    }

    public static Vector3 GetNormalFromContextualizedTri((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData, int triIndex)
    {
        Vector3 ret = Vector3.zero;

        if (triIndex >= 0 && triIndex < meshData.tris.Count)
        {
            (int vertA, int vertB, int vertC) tri = meshData.tris[triIndex];

            Vector3 tri_cornerA = meshData.verts[tri.vertA].VertexPosition;
            Vector3 tri_cornerB = meshData.verts[tri.vertB].VertexPosition;
            Vector3 tri_cornerC = meshData.verts[tri.vertC].VertexPosition;

            Vector3 tri_normal_partA = tri_cornerB - tri_cornerA;
            Vector3 tri_normal_partB = tri_cornerC - tri_cornerB;
            Vector3 tri_normal = -Vector3.Cross(tri_normal_partA, tri_normal_partB).normalized;

            ret = tri_normal;
        }
        return ret;

    }


    public static bool TriangleExistsInSet(List<(int vertA, int vertB, int vertC)> tris, (int vertA, int vertB, int vertC) compareTri)
    {
        bool ret = false;

        foreach ((int vertA, int vertB, int vertC) tri in tris)
        {
            bool vertA_match = compareTri.vertA == tri.vertA || compareTri.vertA == tri.vertB || compareTri.vertA == tri.vertC;
            bool vertB_match = compareTri.vertB == tri.vertA || compareTri.vertB == tri.vertB || compareTri.vertB == tri.vertC;
            bool vertC_match = compareTri.vertC == tri.vertA || compareTri.vertC == tri.vertB || compareTri.vertC == tri.vertC;

            if (vertA_match && vertB_match && vertC_match)
            {
                ret = true;
                break;
            }
        }

        return ret;
    }

    //this method is more wasteful than the old one, generating triangles that can't be seen, but at least it actually works every time and in every context
    public static ((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData, List<int> newVertIndexes) TurnBaseMeshDataIntoExtrusionWalls((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) baseMeshData, float extrusionPower, SheetMesh_ContextualizedVertex[] permittedVerts = null, bool checkIfTrisExist = false, bool minimalExtrusion = true, bool basicExtrusion = false)//minimal extrusion set to false will extrude literally every face even if in the end it wont be visible from any angle, basic extrusion will only extrude along the y axis no matter the surface normal.
    {

        List<SheetMesh_ContextualizedVertex> verts_extrude_asList = new List<SheetMesh_ContextualizedVertex>();
        List<int> verts_extrude_newVertIndexes = new List<int>();

        List<(int vertA, int vertB, int vertC)> tris_extrude = new List<(int vertA, int vertB, int vertC)>();


        List<SheetMesh_ContextualizedVertex> verts_extrude_current = new List<SheetMesh_ContextualizedVertex>();
        List<int> verts_extrude_newVertIndexes_current = new List<int>();
        List<(int vertA, int vertB, int vertC)> tris_extrude_current = new List<(int vertA, int vertB, int vertC)>();
        for (int i = 0; i < baseMeshData.tris.Count; i++)
        {
            (int vertA, int vertB, int vertC) tri = baseMeshData.tris[i];

            bool permitIteration = (permittedVerts == null || (permittedVerts != null && (FindGridPositionInArrayOfContextualVerts(baseMeshData.verts[tri.vertA].VertexGridIndex, permittedVerts) != null || FindGridPositionInArrayOfContextualVerts(baseMeshData.verts[tri.vertB].VertexGridIndex, permittedVerts) != null || FindGridPositionInArrayOfContextualVerts(baseMeshData.verts[tri.vertC].VertexGridIndex, permittedVerts) != null)));



            if (permitIteration || minimalExtrusion == false)
            {
                verts_extrude_current.Clear();
                tris_extrude_current.Clear();
                verts_extrude_newVertIndexes_current.Clear();





                Vector3 tri_base_cornerA = baseMeshData.verts[tri.vertA].VertexPosition;
                Vector3 tri_base_cornerB = baseMeshData.verts[tri.vertB].VertexPosition;
                Vector3 tri_base_cornerC = baseMeshData.verts[tri.vertC].VertexPosition;


                Vector3 tri_roof_cornerA = new Vector3(tri_base_cornerA.x, tri_base_cornerA.y + extrusionPower, tri_base_cornerA.z);
                Vector3 tri_roof_cornerB = new Vector3(tri_base_cornerB.x, tri_base_cornerB.y + extrusionPower, tri_base_cornerB.z);
                Vector3 tri_roof_cornerC = new Vector3(tri_base_cornerC.x, tri_base_cornerC.y + extrusionPower, tri_base_cornerC.z);


                if (basicExtrusion == false)
                {
                    Vector3 tri_base_normal = -SheetMethod_SheetMesh_MeshGeneration_Utilities.GetNormalFromContextualizedTri(baseMeshData, i);


                    tri_roof_cornerA = tri_base_cornerA + (tri_base_normal * -extrusionPower);
                    tri_roof_cornerB = tri_base_cornerB + (tri_base_normal * -extrusionPower);
                    tri_roof_cornerC = tri_base_cornerC + (tri_base_normal * -extrusionPower);
                }



                verts_extrude_current.Add(new SheetMesh_ContextualizedVertex(tri_base_cornerA, baseMeshData.verts[tri.vertA].VertexGridIndex));//0
                verts_extrude_current.Add(new SheetMesh_ContextualizedVertex(tri_base_cornerB, baseMeshData.verts[tri.vertB].VertexGridIndex));//1
                verts_extrude_current.Add(new SheetMesh_ContextualizedVertex(tri_base_cornerC, baseMeshData.verts[tri.vertC].VertexGridIndex));//2


                verts_extrude_current.Add(new SheetMesh_ContextualizedVertex(tri_roof_cornerA, baseMeshData.verts[tri.vertA].VertexGridIndex));//3
                verts_extrude_current.Add(new SheetMesh_ContextualizedVertex(tri_roof_cornerB, baseMeshData.verts[tri.vertB].VertexGridIndex));//4
                verts_extrude_current.Add(new SheetMesh_ContextualizedVertex(tri_roof_cornerC, baseMeshData.verts[tri.vertC].VertexGridIndex));//5

                //verts_extrude_current is applied in an addrange() to the end of verts_extrude_asList, so +3, +4, and +5 based on the values in verts_extrude_current above are the new verts only
                verts_extrude_newVertIndexes_current.Add(3 + verts_extrude_asList.Count);
                verts_extrude_newVertIndexes_current.Add(4 + verts_extrude_asList.Count);
                verts_extrude_newVertIndexes_current.Add(5 + verts_extrude_asList.Count);






                bool tri0_permitted = true;
                bool tri1_permitted = true;
                bool tri2_permitted = true;
                bool tri3_permitted = true;
                bool tri4_permitted = true;
                bool tri5_permitted = true;

                if (minimalExtrusion)
                {

                    bool vert0_inPermittedVerts = FindGridPositionInArrayOfContextualVerts(verts_extrude_current[0].VertexGridIndex, permittedVerts) != null;
                    bool vert1_inPermittedVerts = FindGridPositionInArrayOfContextualVerts(verts_extrude_current[1].VertexGridIndex, permittedVerts) != null;
                    bool vert2_inPermittedVerts = FindGridPositionInArrayOfContextualVerts(verts_extrude_current[2].VertexGridIndex, permittedVerts) != null;
                    bool vert3_inPermittedVerts = FindGridPositionInArrayOfContextualVerts(verts_extrude_current[3].VertexGridIndex, permittedVerts) != null;
                    bool vert4_inPermittedVerts = FindGridPositionInArrayOfContextualVerts(verts_extrude_current[4].VertexGridIndex, permittedVerts) != null;
                    bool vert5_inPermittedVerts = FindGridPositionInArrayOfContextualVerts(verts_extrude_current[5].VertexGridIndex, permittedVerts) != null;

                    tri0_permitted = vert1_inPermittedVerts && vert0_inPermittedVerts && vert3_inPermittedVerts;
                    tri1_permitted = vert3_inPermittedVerts && vert4_inPermittedVerts && vert1_inPermittedVerts;
                    tri2_permitted = vert2_inPermittedVerts && vert1_inPermittedVerts && vert4_inPermittedVerts;
                    tri3_permitted = vert4_inPermittedVerts && vert5_inPermittedVerts && vert2_inPermittedVerts;
                    tri4_permitted = vert3_inPermittedVerts && vert2_inPermittedVerts && vert5_inPermittedVerts;
                    tri5_permitted = vert5_inPermittedVerts && vert0_inPermittedVerts && vert3_inPermittedVerts;
                }

                (int vertA, int vertB, int vertC) tri0 = (1, 0, 3);
                (int vertA, int vertB, int vertC) tri1 = (3, 4, 1);
                (int vertA, int vertB, int vertC) tri2 = (2, 1, 4);
                (int vertA, int vertB, int vertC) tri3 = (4, 5, 2);
                (int vertA, int vertB, int vertC) tri4 = (3, 2, 5);
                (int vertA, int vertB, int vertC) tri5 = (5, 0, 3);


                if (checkIfTrisExist)
                {
                    tri0_permitted = tri0_permitted && !TriangleExistsInSet(tris_extrude, tri0);
                    tri1_permitted = tri1_permitted && !TriangleExistsInSet(tris_extrude, tri1);
                    tri2_permitted = tri2_permitted && !TriangleExistsInSet(tris_extrude, tri2);
                    tri3_permitted = tri3_permitted && !TriangleExistsInSet(tris_extrude, tri3);
                    tri4_permitted = tri4_permitted && !TriangleExistsInSet(tris_extrude, tri4);
                    tri5_permitted = tri5_permitted && !TriangleExistsInSet(tris_extrude, tri5);
                }




                if (tri0_permitted)
                {
                    tris_extrude_current.Add(tri0);
                }
                if (tri1_permitted)
                {
                    tris_extrude_current.Add(tri1);

                }



                if (tri2_permitted)
                {
                    tris_extrude_current.Add(tri2);

                }
                if (tri3_permitted)
                {
                    tris_extrude_current.Add(tri3);

                }



                if (tri4_permitted)
                {
                    tris_extrude_current.Add(tri4);

                }
                if (tri5_permitted)
                {
                    tris_extrude_current.Add(tri5);

                }







                for (int j = 0; j < tris_extrude_current.Count; j++)
                {
                    (int vertA, int vertB, int vertC) triCur = tris_extrude_current[j];
                    triCur.vertA += verts_extrude_asList.Count;
                    triCur.vertB += verts_extrude_asList.Count;
                    triCur.vertC += verts_extrude_asList.Count;

                    tris_extrude_current[j] = triCur;
                }

                verts_extrude_asList.AddRange(verts_extrude_current);
                tris_extrude.AddRange(tris_extrude_current);
                verts_extrude_newVertIndexes.AddRange(verts_extrude_newVertIndexes_current);
            }



        }

        return ((verts_extrude_asList.ToArray(), tris_extrude), verts_extrude_newVertIndexes);
    }



    public static (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) CopyMeshData((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) srcMesh)
    {
        (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) ret;
        ret.verts = new SheetMesh_ContextualizedVertex[srcMesh.verts.Length];
        ret.tris = new List<(int vertA, int vertB, int vertC)>();

        for (int i = 0; i < srcMesh.verts.Length; i++)
        {
            ret.verts[i] = srcMesh.verts[i];
        }

        foreach ((int vertA, int vertB, int vertC) tri in srcMesh.tris)
        {
            ret.tris.Add(tri);
        }

        return ret;

    }



    public static void PointCloudToggle(SheetMesh_ContextualizedVertex[] verts)
    {


        for (int i = 0; i < verts.Length; i++)
        {

            GameObject pointCloudObj = new GameObject();
            pointCloudObj.name = "pointCloud_" + i.ToString();
            GizmoRep sphereGiz = pointCloudObj.AddComponent<GizmoRep>();

            sphereGiz.GizmoCol = Color.red;
            sphereGiz.SphereRadius = 1.0f;
            pointCloudObj.transform.position = verts[i].VertexPosition;

            sphereGiz.GizmoText = "(" + verts[i].VertexGridIndex.X.ToString() + "," + verts[i].VertexGridIndex.Y.ToString() + ")";
        }
    }

    public static int[] ConvertContextualizedTrianglesToRubbishTriangles(List<(int vertA, int vertB, int vertC)> triangles)
    {
        int[] ret = new int[0];
        if (triangles != null)
        {
            System.Array.Resize(ref ret, triangles.Count * 3);

            for (int i = 0; i < triangles.Count; i++)
            {
                (int vertA, int vertB, int vertC) tri = triangles[i];

                int baseIndex = i * 3;

                ret[baseIndex] = tri.vertA;
                ret[baseIndex + 1] = tri.vertB;
                ret[baseIndex + 2] = tri.vertC;

            }
        }
        return ret;
    }

    public static List<(int vertA, int vertB, int vertC)> TurnContextualizedVerticesIntoContextualizedTriangles(SheetMesh_ContextualizedVertex[] verts, bool checkIfTrisExist = false)
    {
        List<(int vertA, int vertB, int vertC)> ret = new List<(int vertA, int vertB, int vertC)>();

        List<SheetMesh_ContextualizedVertex> verts_list = verts.ToList();

        while (verts_list.Count % 4 != 0)
        {
            verts_list.RemoveAt(verts_list.Count - 1);
        }
        verts = verts_list.ToArray();

        int vertIndex = 0;
        while (vertIndex < verts.Length)
        {
            (int vertA, int vertB, int vertC) triA = (vertIndex, vertIndex + 1, vertIndex + 3);
            (int vertA, int vertB, int vertC) triB = (vertIndex + 3, vertIndex + 2, vertIndex);

            if (checkIfTrisExist == false || (checkIfTrisExist && !SheetMethod_SheetMesh_MeshGeneration_Utilities.TriangleExistsInSet(ret, triA)))
            {
                ret.Add(triA);

            }
            if (checkIfTrisExist == false || (checkIfTrisExist && !SheetMethod_SheetMesh_MeshGeneration_Utilities.TriangleExistsInSet(ret, triB)))
            {
                ret.Add(triB);

            }

            vertIndex += 4;



        }

        return ret;
    }

    public static Vector3[] TurnContextualizedVerticesIntoGeneralVertices(SheetMesh_ContextualizedVertex[] contextualVerts)
    {
        List<Vector3> ret = new List<Vector3>();
        if (contextualVerts != null)
        {
            foreach (SheetMesh_ContextualizedVertex vert in contextualVerts)
            {
                ret.Add(vert.VertexPosition);
            }
        }
        return ret.ToArray();
    }


    public static SheetMesh_ContextualizedVertex[] GetAlternatingGridVertices(SheetMesh_ContextualizedVertex[] contextualVerts)
    {
        List<SheetMesh_ContextualizedVertex> ret_asList = new List<SheetMesh_ContextualizedVertex>();
        for(int i = 0; i < contextualVerts.Length; i++)
        {
            if(contextualVerts[i].VertexGridIndex.X%2==0 && contextualVerts[i].VertexGridIndex.Y % 2 == 0)
            {
                ret_asList.Add(contextualVerts[i]);
            }
        }

        return ret_asList.ToArray();
    }

}

public abstract class SheetMethod_SheetMesh_MeshGeneration : MonoBehaviour
{

    static Material goopMat = null;
    static Material lavaMat = null;

    static Material iceMat = null;


    static Material themeDependent_floorSidesMat = null;
    static Material themeDependent_floorRoofMat = null;
    static Material themeDependent_skyboxMat = null;





    static void DeleteAllLighting()
    {
        if (FindResources_All())
        {
            Light[] allLights = GameObject.FindObjectsOfType<Light>();

            for (int i = 0; i < allLights.Length; i++)
            {
                ExtendedFunctionality.SafeDestroy(allLights[i].gameObject);
            }
        }
    }
    public static void SetSkyboxAndLighting()
    {
        if (FindResources_All())
        {
            RenderSettings.skybox = themeDependent_skyboxMat;
            RenderSettings.subtractiveShadowColor = Color.black;
            RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;
            RenderSettings.ambientSkyColor = Color.white;
            RenderSettings.ambientGroundColor = Color.black;
            RenderSettings.ambientEquatorColor = new Color(0.7264151f, 0.7264151f, 0.7264151f);
            RenderSettings.sun = null;
            RenderSettings.subtractiveShadowColor = new Color(0.42f, 0.478f, 0.627f);

            DeleteAllLighting();


        }
    }

    static bool FindResources_All()
    {
        return FindResources() && FindResources_Theme(LevelThemeCollection.GetThemeFromDictionaryByType(SheetMethod.LoadedLevelData.levelThemeType));
    }

    public static void FlushThemeItems()
    {
        themeDependent_floorSidesMat = null;
        themeDependent_floorRoofMat = null;
        themeDependent_skyboxMat = null;
    }

    static bool FindResources_Theme(LevelTheme themeResources)
    {
        bool ret = false;



        if (themeDependent_floorSidesMat == null)
        {
            themeDependent_floorSidesMat = Resources.Load<Material>(themeResources.FloorMat_Sides_ResourcePath);
        }

        if (themeDependent_floorRoofMat == null)
        {
            themeDependent_floorRoofMat = Resources.Load<Material>(themeResources.FloorMat_Roof_ResourcePath);
        }

        if (themeDependent_skyboxMat == null)
        {
            themeDependent_skyboxMat = Resources.Load<Material>(themeResources.SkyboxMat_ResourcePath);
        }



        if (themeDependent_skyboxMat != null&& themeDependent_floorSidesMat != null && themeDependent_floorRoofMat != null)
        {
            ret = true;
        }



        return ret;
    }

    static bool FindResources()
    {
        bool ret = false;



        if (goopMat == null)
        {
            goopMat = Resources.Load<Material>("Materials/Themes/goopMat");

        }

        if (lavaMat == null)
        {
            lavaMat = Resources.Load<Material>("Materials/Themes/lavaMat");

        }



        if (iceMat == null)
        {
            iceMat = Resources.Load<Material>("Materials/Themes/iceMat");

        }


        if ( iceMat != null  && goopMat!=null && lavaMat != null)
        {
            ret = true;
        }
        return ret;
    }

    private static SheetMesh_ContextualizedVertex[] GenerateModelPermitted(SheetShape_SurfaceShape baseShape, int meshResolution)
    {

        SheetMesh_ContextualizedVertex[] meshData_base_vertData = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnShapeIntoBaseContextualizedVertices(baseShape,meshResolution);

        //for reasons im assuming are due to image resizing, sometimes the level plan has shapes that were not actually drawn there that have 0 assernable vertices, so I have to check this result is good.

        if (meshData_base_vertData.Length > 0)
        {
            return meshData_base_vertData;
        }
        else
        {
            return null;
        }
    }




    public static (Mesh createdMesh, List<Material> meshMats, E_ShapeSurfaceTypes surfaceType) GenerateHazardousGoop(SheetShape_SurfaceShape baseShape, int meshResolution, float heightOffset,float thickness, (float minPush, float maxPush) goopWaveRange)
    {
        SheetMesh_ContextualizedVertex[] baseVerts = GenerateModelPermitted(baseShape, meshResolution);
        List<Material> ret_mats = new List<Material>();
        Mesh ret_mesh = null;

        if (FindResources_All() && baseVerts!=null)
        {
            ret_mesh = new Mesh();
            //base mesh
            SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_OffsetVerts(baseVerts, new Vector3(0, heightOffset, 0));


            SheetMesh_ContextualizedVertex[] baseVerts_outerEdge = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetOuterLoopsOfContextualizedVerts(baseVerts);


            SheetMesh_ContextualizedVertex[] baseVerts_outerEdge_everyOtherVert = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetAlternatingGridVertices(baseVerts_outerEdge);


            for(int i=0;i< baseVerts_outerEdge_everyOtherVert.Length; i++)
            {
                SheetMesh_ContextualizedVertex[] curPermit = new SheetMesh_ContextualizedVertex[1];
                curPermit[0] = baseVerts_outerEdge_everyOtherVert[i];
                baseVerts = SheetMethod_SheetMesh_MeshGeneration_Utilities.PushContextualizedVerts(baseVerts, Random.Range(goopWaveRange.minPush, goopWaveRange.maxPush), curPermit);

            }


            List<(int vertA, int vertB, int vertC)> baseTris = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoContextualizedTriangles(baseVerts);

            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_base = (baseVerts, baseTris);
            meshData_base = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingOnFlatMeshes(meshData_base);

            SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_OffsetVerts(baseVerts, new Vector3(0, -thickness, 0), baseVerts_outerEdge_everyOtherVert);


            //roof mesh
            SheetMesh_ContextualizedVertex[] roofVerts = SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_CopyVerts(baseVerts);
            SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_OffsetVerts(roofVerts, new Vector3(0, thickness, 0));



            List<(int vertA, int vertB, int vertC)> roofTris = SheetMethod_SheetMesh_MeshGeneration_Utilities.FlipTris(baseTris, true);

            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_roof = (roofVerts, roofTris);


            meshData_roof = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingOnFlatMeshes(meshData_roof);


            //extrude mesh

            ((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData, List<int> newVertIndexes) meshData_extrude = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnBaseMeshDataIntoExtrusionWalls(meshData_base, thickness, baseVerts_outerEdge, false, true);


            //final mesh
            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_all = SheetMethod_SheetMesh_MeshGeneration_Utilities.CombineMeshData(new List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> { meshData_base,meshData_roof,meshData_extrude.meshData });

            meshData_all = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_MergeVerticesByPosition(meshData_all);



            
            ret_mesh.vertices = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoGeneralVertices(meshData_all.verts);
            ret_mesh.triangles = SheetMethod_SheetMesh_MeshGeneration_Utilities.ConvertContextualizedTrianglesToRubbishTriangles(meshData_all.tris);



            ret_mesh.RecalculateBounds();


            ret_mesh.RecalculateNormals();







            ret_mats.Add(goopMat);
        }


        return (ret_mesh, ret_mats,E_ShapeSurfaceTypes.Hazard_Goop);

    }







    public static (Mesh createdMesh, List<Material> meshMats, E_ShapeSurfaceTypes surfaceType) GenerateLava(SheetShape_SurfaceShape baseShape, int meshResolution, float heightOffset)
    {
        SheetMesh_ContextualizedVertex[] baseVerts = GenerateModelPermitted(baseShape, meshResolution);
        List<Material> ret_mats = new List<Material>();

        Mesh ret_mesh = null;
        if (FindResources_All() && baseVerts != null)
        {
            ret_mesh = new Mesh();

            SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_OffsetVerts(baseVerts, new Vector3(0, Mathf.Abs(heightOffset), 0));





            List<(int vertA, int vertB, int vertC)> meshData_base_triData = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoContextualizedTriangles(baseVerts);

            meshData_base_triData = SheetMethod_SheetMesh_MeshGeneration_Utilities.FlipTris(meshData_base_triData);



            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_base = (baseVerts, meshData_base_triData);



            meshData_base = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingOnFlatMeshes(meshData_base);



            meshData_base = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_MergeVerticesByPosition(meshData_base);






            ret_mesh.vertices = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoGeneralVertices(meshData_base.verts);
            ret_mesh.triangles = SheetMethod_SheetMesh_MeshGeneration_Utilities.ConvertContextualizedTrianglesToRubbishTriangles(meshData_base.tris);
            ret_mesh.uv = SheetMethod_SheetMesh_MeshGeneration_Utilities.GenerateTopDownUVsFromVertices(ret_mesh.vertices, true);


            ret_mesh.RecalculateBounds();


            ret_mesh.RecalculateNormals();



            ret_mats.Add(lavaMat);




        }



        return (ret_mesh, ret_mats, E_ShapeSurfaceTypes.Hazard_Lava);
    }

    public static (Mesh createdMesh, List<Material> meshMats, E_ShapeSurfaceTypes surfaceType) GenerateIce(SheetShape_SurfaceShape baseShape, int meshResolution, float levelThickness, (float minPush, float maxPush) iceJaggedRange)
    {
        SheetMesh_ContextualizedVertex[] baseVerts = GenerateModelPermitted(baseShape, meshResolution);
        List<Material> ret_mats = new List<Material>();
        Mesh ret_mesh = null;

        if (FindResources_All() && baseVerts != null)
        {
            ret_mesh = new Mesh();

            //base mesh


            SheetMesh_ContextualizedVertex[] baseVerts_outerEdge = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetOuterLoopsOfContextualizedVerts(baseVerts);


            SheetMesh_ContextualizedVertex[] baseVerts_outerEdge_everyOtherVert = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetAlternatingGridVertices(baseVerts_outerEdge);


            for (int i = 0; i < baseVerts_outerEdge_everyOtherVert.Length; i++)
            {
                SheetMesh_ContextualizedVertex[] curPermit = new SheetMesh_ContextualizedVertex[1];
                curPermit[0] = baseVerts_outerEdge_everyOtherVert[i];
                baseVerts = SheetMethod_SheetMesh_MeshGeneration_Utilities.PushContextualizedVerts(baseVerts, Random.Range(iceJaggedRange.minPush, iceJaggedRange.maxPush), curPermit);

            }


            List<(int vertA, int vertB, int vertC)> baseTris = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoContextualizedTriangles(baseVerts);

            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_base = (baseVerts, baseTris);
            meshData_base = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingOnFlatMeshes(meshData_base);


            //roof mesh
            SheetMesh_ContextualizedVertex[] roofVerts = SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_CopyVerts(baseVerts);
            SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_OffsetVerts(roofVerts, new Vector3(0, levelThickness, 0));



            List<(int vertA, int vertB, int vertC)> roofTris = SheetMethod_SheetMesh_MeshGeneration_Utilities.FlipTris(baseTris, true);

            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_roof = (roofVerts, roofTris);


            meshData_roof = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingOnFlatMeshes(meshData_roof);


            //extrude mesh

            ((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData, List<int> newVertIndexes) meshData_extrude = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnBaseMeshDataIntoExtrusionWalls(meshData_base, levelThickness, baseVerts_outerEdge, false, true);


            //final mesh
            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_all = SheetMethod_SheetMesh_MeshGeneration_Utilities.CombineMeshData(new List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> { meshData_base, meshData_roof, meshData_extrude.meshData });

            meshData_all = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_MergeVerticesByPosition(meshData_all);




            ret_mesh.vertices = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoGeneralVertices(meshData_all.verts);
            ret_mesh.triangles = SheetMethod_SheetMesh_MeshGeneration_Utilities.ConvertContextualizedTrianglesToRubbishTriangles(meshData_all.tris);



            ret_mesh.RecalculateBounds();


            ret_mesh.RecalculateNormals();







            ret_mats.Add(iceMat);
        }


        return (ret_mesh, ret_mats, E_ShapeSurfaceTypes.Ice);

    }

    public static (Mesh createdMesh, List<Material> meshMats,E_ShapeSurfaceTypes surfaceType) GenerateFloor(SheetShape_SurfaceShape baseShape,int meshResolution, float thickness, float insetPower, float insetRaise, bool debugMeshBounds=false)
    {






        SheetMesh_ContextualizedVertex[] baseVerts = GenerateModelPermitted(baseShape, meshResolution);




        List<Material> ret_mats = new List<Material>();
        Mesh ret_mesh = null;


        if (FindResources_All() && baseVerts!=null)
        {
            ret_mesh = new Mesh();





            List<(int vertA, int vertB, int vertC)> meshData_base_triData = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoContextualizedTriangles(baseVerts);




            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_base = (baseVerts, meshData_base_triData);


            meshData_base = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingOnFlatMeshes(meshData_base);



            //extrusion
            SheetMesh_ContextualizedVertex[] meshData_base_vertData_openEdgeVertsOnly = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetOuterLoopsOfContextualizedVerts(baseVerts);





            ((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData, List<int> newVertIndexes) extrudeData = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnBaseMeshDataIntoExtrusionWalls(meshData_base, thickness, meshData_base_vertData_openEdgeVertsOnly, false, true);




            //roof mesh
            SheetMesh_ContextualizedVertex[] meshData_roof_vertData = SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_CopyVerts(baseVerts);





            SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_OffsetVerts(meshData_roof_vertData, new Vector3(0, thickness, 0));





            SheetMesh_ContextualizedVertex[] meshData_roof_vertData_openEdgeVertsOnly = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetOuterLoopsOfContextualizedVerts(meshData_roof_vertData);



            meshData_roof_vertData = SheetMethod_SheetMesh_MeshGeneration_Utilities.PushContextualizedVerts(meshData_roof_vertData, insetPower, meshData_roof_vertData_openEdgeVertsOnly);




            SheetMethod_SheetMesh_MeshGeneration_Utilities.ContextualizedVerts_OffsetVerts(meshData_roof_vertData, new Vector3(0, insetRaise, 0));





            List<(int vertA, int vertB, int vertC)> meshData_roof_triData = SheetMethod_SheetMesh_MeshGeneration_Utilities.FlipTris(meshData_base_triData, true);



            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_roof = (meshData_roof_vertData, meshData_roof_triData);


            meshData_roof = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingOnFlatMeshes(meshData_roof);





            //sew line between roof and exctrusion after inset mesh
            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_temporaryFakeRoof = SheetMethod_SheetMesh_MeshGeneration_Utilities.CopyMeshData(meshData_roof);
            
            



            SheetMesh_ContextualizedVertex[] meshData_temporaryFakeRoof_openEdgeVertsOnly = SheetMethod_SheetMesh_MeshGeneration_Utilities.GetOuterLoopsOfContextualizedVerts(meshData_temporaryFakeRoof.verts);




            ((SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData, List<int> newVertIndexes) extrudeData_insetGap = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnBaseMeshDataIntoExtrusionWalls(meshData_temporaryFakeRoof, thickness, meshData_temporaryFakeRoof_openEdgeVertsOnly, false, true);




            extrudeData_insetGap.meshData.verts = SheetMethod_SheetMesh_MeshGeneration_Utilities.ContVertSetA_Positions_To_ContVertSetB_Positions_WhenGridIndexMatches(extrudeData_insetGap.meshData.verts, meshData_roof_vertData_openEdgeVertsOnly, extrudeData_insetGap.newVertIndexes);





            //mergeing sew line and extrusion for smooth normals
            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_sides = SheetMethod_SheetMesh_MeshGeneration_Utilities.CombineMeshData(new List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> { extrudeData_insetGap.meshData, extrudeData.meshData });





            meshData_sides = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_MergeVerticesByPosition(meshData_sides);




            meshData_sides = SheetMethod_SheetMesh_MeshGeneration_Utilities.MeshData_UniformCullingByIterativeDotComparison(meshData_sides, 0.5f);





            //combine for submeshes
            List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> combineData_notTheFloor = new List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> { meshData_base, meshData_sides };

            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_notTheFloor = SheetMethod_SheetMesh_MeshGeneration_Utilities.CombineMeshData(combineData_notTheFloor);

            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) meshData_theFloor = meshData_roof;

            List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> combineData_allCombine = new List<(SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris)> { meshData_notTheFloor, meshData_theFloor };

            (SheetMesh_ContextualizedVertex[] verts, List<(int vertA, int vertB, int vertC)> tris) allCombine = SheetMethod_SheetMesh_MeshGeneration_Utilities.CombineMeshData(combineData_allCombine);


            int meshData_notTheFloor_verts_offset = meshData_notTheFloor.verts.Length;

            int meshData_theFloor_verts_offset = meshData_theFloor.verts.Length;



            ret_mesh.vertices = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoGeneralVertices(allCombine.verts);

            ret_mesh.subMeshCount = 2;

            ret_mesh.SetTriangles(SheetMethod_SheetMesh_MeshGeneration_Utilities.ConvertContextualizedTrianglesToRubbishTriangles(meshData_notTheFloor.tris), 0, true, 0);

            ret_mesh.SetTriangles(SheetMethod_SheetMesh_MeshGeneration_Utilities.ConvertContextualizedTrianglesToRubbishTriangles(meshData_theFloor.tris), 1, true, meshData_notTheFloor_verts_offset);

            ret_mesh.uv = SheetMethod_SheetMesh_MeshGeneration_Utilities.GenerateTopDownUVsFromVertices(ret_mesh.vertices, true);




            /*
            ret_mesh.vertices = SheetMethod_SheetMesh_MeshGeneration_Utilities.TurnContextualizedVerticesIntoGeneralVertices(meshData_sides.verts);
            ret_mesh.triangles = SheetMethod_SheetMesh_MeshGeneration_Utilities.ConvertContextualizedTrianglesToRubbishTriangles(meshData_sides.tris);
            */


            if (debugMeshBounds)
            {
                ret_mesh.bounds = new Bounds(ret_mesh.bounds.center, Vector3.one * 2000);

            }
            else
            {
                ret_mesh.RecalculateBounds();
            }

            ret_mesh.RecalculateNormals();





            ret_mats.Add(themeDependent_floorSidesMat);
            ret_mats.Add(themeDependent_floorRoofMat);
        }



        return (ret_mesh, ret_mats,E_ShapeSurfaceTypes.Ground);
    }
}