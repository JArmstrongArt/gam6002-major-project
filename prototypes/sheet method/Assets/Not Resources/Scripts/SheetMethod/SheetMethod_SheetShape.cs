using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetShape
{

    public SheetShape_Bounds ShapeBounds { get; private set; }

    public List<List<bool>> BoolSet_PixelData { get; private set; }//true for 'pixel taken', false for 'pixel free'






    public SheetShape(SheetShape_Bounds sB,  List<List<bool>> pD = null)
    {

        if (pD != null)
        {
            BoolSet_PixelData = pD;
        }
        else
        {
            BoolSet_PixelData = new List<List<bool>>();
        }

        if (sB != null)
        {
            ShapeBounds = sB;
        }
        else
        {
            ShapeBounds = new SheetShape_Bounds((0, 0), (0, 0), (0, 0));
        }

    }




    private bool[,] BoolSet_GetTrimmedArrayCopy()
    {

        SheetShape_Bounds trimDimensions = ShapeBounds.BaseZeroCopy();

        bool[,] ret = new bool[trimDimensions.BottomRight.X + 1, trimDimensions.BottomRight.Y + 1];

        foreach ((int X, int Y) coord in BoolSetToCoordSet(-trimDimensions.BaseZeroOffset.X, -trimDimensions.BaseZeroOffset.Y))
        {
            ret[coord.X, coord.Y] = true;
        }

        return ret;
    }

    public static void VisualizeBoolSetArrayCopy(bool[,] boolSet)
    {
        GameObject boolSetIndexParentObj = new GameObject();

        for (int i = 0; i < boolSet.GetLength(0); i++)
        {
            for(int j = 0; j < boolSet.GetLength(1); j++)
            {
                GameObject boolSetIndexObj = new GameObject();
                boolSetIndexObj.name = "boolSetIndexObj_(" + i.ToString()+","+j.ToString()+")";
                GizmoRep sphereGiz = boolSetIndexObj.AddComponent<GizmoRep>();


                if (boolSet[i,j])
                {
                    sphereGiz.GizmoCol = Color.red;

                }
                else
                {
                    sphereGiz.GizmoCol = Color.blue;

                }
                sphereGiz.SphereRadius = 1.0f;
                boolSetIndexObj.transform.position = new Vector3(i, 0, j);
                boolSetIndexObj.transform.parent = boolSetIndexParentObj.transform;
            }
        }
    }

    //shape complexity = ratio of empty to filled space in a shape measured within it's bounding box, the more empty space the more complex it is, output is normalized
    public float GetShapeComplexity((int width, int height) levelPlanDimensions,  bool doVisualize=false, float fillCount_score_distFromCenter_exaggerationFactor = 1.0f, float distributionBalance_score_distFromCenter_exaggerationFactor = 1.0f)
    {

        //fillCount_score: a normalized 0 to 1 value to say how close to 100% of the shape bounds are filled pixels.
        //the closer to 100% it is, the more complex the shape is and the closer to 1 the outcome is. 0% of the shape being filled being the most complex because a bounding box covering the entire area of a shape and still getting 0% filled (or as close to it as possible) must mean its pixels are spread about a lot.
        //closer to 0% or 100% means closer to not complex as the bounding box is either entirely full or empty and therefore is just a box shape.
        //basically fillCount_score determines how close to a box a shape is.


        //distributionBalance_score: a normalized 0 to 1 value to show how evenly filled pixels are spread across 4 even quarters of the shapes bounding box.
        //perfect 25% distribution gives a score of 0 meaning its a simple shape.
        //as uneven as possible distribution gives a score of 1 meaning the pixels are all over the place.


        //gathering data for calculation
        bool[,] boolSet_trimmed= BoolSet_GetTrimmedArrayCopy();

        if (doVisualize)
        {
            VisualizeBoolSetArrayCopy(boolSet_trimmed);
            
        }

        (int TL, int TR, int BL, int BR) fillCount_byQuarter = (0, 0, 0, 0);


        for (int i = 0; i < boolSet_trimmed.GetLength(0); i++)
        {
            for (int j = 0; j < boolSet_trimmed.GetLength(1); j++)
            {
                if (boolSet_trimmed[i,j] == true)
                {

                    bool quarter_isLeft = i < (float)boolSet_trimmed.GetLength(0) / (float)2;
                    bool quarter_isTop = j < (float)boolSet_trimmed.GetLength(1) / (float)2;

                    bool quarter_isTL = quarter_isLeft && quarter_isTop;
                    bool quarter_isTR = !quarter_isLeft && quarter_isTop;
                    bool quarter_isBL = quarter_isLeft && !quarter_isTop;
                    bool quarter_isBR = !quarter_isLeft && !quarter_isTop;

                    if (quarter_isTL)
                    {
                        fillCount_byQuarter.TL += 1;

                    }

                    if (quarter_isTR)
                    {
                        fillCount_byQuarter.TR += 1;

                    }

                    if (quarter_isBL)
                    {
                        fillCount_byQuarter.BL += 1;

                    }

                    if (quarter_isBR)
                    {
                        fillCount_byQuarter.BR += 1;

                    }



                }
            }
        }



        //fillCount_score calculation
        int fillCount = fillCount_byQuarter.TL + fillCount_byQuarter.TR + fillCount_byQuarter.BL + fillCount_byQuarter.BR;



        (int XDim, int YDim) dim = (boolSet_trimmed.GetLength(0), boolSet_trimmed.GetLength(1));



        int fillCount_max = dim.XDim * dim.YDim;
        
        
        int fillCount_max_orig = fillCount_max;



        if (fillCount_byQuarter.TL <= 0)
        {
            fillCount_max -= Mathf.RoundToInt( (float)fillCount_max_orig / (float)4);
        }

        if (fillCount_byQuarter.TR <= 0)
        {
            fillCount_max -= Mathf.RoundToInt((float)fillCount_max_orig / (float)4);
        }

        if (fillCount_byQuarter.BL <= 0)
        {
            fillCount_max -= Mathf.RoundToInt((float)fillCount_max_orig / (float)4);
        }

        if (fillCount_byQuarter.BR <= 0)
        {
            fillCount_max -= Mathf.RoundToInt((float)fillCount_max_orig / (float)4);
        }


        







        float fillCount_score = Mathf.Clamp01( Mathf.InverseLerp(fillCount_max, 0, fillCount));



        float fillCount_score_distFromCenter = fillCount_score - 0.5f;
        fillCount_score_distFromCenter *= fillCount_score_distFromCenter_exaggerationFactor;
        fillCount_score = fillCount_score_distFromCenter+ 0.5f;


        //distributionBalance_score calculation
        int distributionBalance_totalPixels = fillCount;
        (int TL, int TR, int BL, int BR) distributonBalance_pixelDistribution = fillCount_byQuarter;

        (int TL, int TR, int BL, int BR) distributonBalance_pixelDistribution_asPercentOfTotalPixels;

        int distributionBalance_totalPixels_onePercent = Mathf.RoundToInt((float)distributionBalance_totalPixels / (float)100);

        distributonBalance_pixelDistribution_asPercentOfTotalPixels.TL = Mathf.RoundToInt( (float)distributonBalance_pixelDistribution.TL / (float)distributionBalance_totalPixels_onePercent );
        distributonBalance_pixelDistribution_asPercentOfTotalPixels.TR = Mathf.RoundToInt((float)distributonBalance_pixelDistribution.TR / (float)distributionBalance_totalPixels_onePercent);
        distributonBalance_pixelDistribution_asPercentOfTotalPixels.BL = Mathf.RoundToInt((float)distributonBalance_pixelDistribution.BL / (float)distributionBalance_totalPixels_onePercent);
        distributonBalance_pixelDistribution_asPercentOfTotalPixels.BR = Mathf.RoundToInt((float)distributonBalance_pixelDistribution.BR / (float)distributionBalance_totalPixels_onePercent);

        int distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent = 25;

        int distributionBalance_evenBalanceOffset = distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent;

        distributionBalance_evenBalanceOffset += distributonBalance_pixelDistribution_asPercentOfTotalPixels.TL - distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent;
        distributionBalance_evenBalanceOffset += distributonBalance_pixelDistribution_asPercentOfTotalPixels.TR - distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent;
        distributionBalance_evenBalanceOffset += distributonBalance_pixelDistribution_asPercentOfTotalPixels.BL - distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent;
        distributionBalance_evenBalanceOffset += distributonBalance_pixelDistribution_asPercentOfTotalPixels.TR - distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent;

        int distributionBalance_balance = Mathf.Abs(distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent - distributionBalance_evenBalanceOffset);

        float distributionBalance_score = Mathf.Clamp01( Mathf.InverseLerp(distributonBalance_pixelDistribution_asPercentOfTotalPixels_perfectBalancePercent, 0, distributionBalance_balance));

        distributionBalance_score = 1 - distributionBalance_score;

        float distributionBalance_score_distFromCenter = distributionBalance_score - 0.5f;
        distributionBalance_score_distFromCenter *= distributionBalance_score_distFromCenter_exaggerationFactor;
        distributionBalance_score = distributionBalance_score_distFromCenter + 0.5f;



        //final score calculation
        float complexityScore = (float)(fillCount_score + distributionBalance_score) / (float)2;



        return Mathf.Clamp01(complexityScore);
    }

    public List<(int X, int Y)> BoolSetToCoordSet(int xShift = 0, int yShift = 0)//xShift and yShift are for when I need these coords normalized like when writing the shapes to an image, can be mostly ignored.
    {
        List<(int X, int Y)> ret = new List<(int X, int Y)>();

        for (int i = 0; i < BoolSet_PixelData.Count; i++)
        {
            for (int j = 0; j < BoolSet_PixelData[i].Count; j++)
            {
                if (BoolSet_PixelData[i][j])
                {
                    ret.Add((i + xShift, j + yShift));
                }
            }
        }

        return ret;
    }

    public void ImplantCoordIntoBoolSet((int X, int Y) coord)
    {
        while (BoolSet_PixelData.Count < coord.X + 1)
        {
            BoolSet_PixelData.Add(new List<bool>());
        }



        while (BoolSet_PixelData[coord.X].Count < coord.Y + 1)
        {
            BoolSet_PixelData[coord.X].Add(false);

        }


        BoolSet_PixelData[coord.X][coord.Y] = true;
    }
}

public class SheetShape_SurfaceShape:SheetShape
{
    public E_ShapeSurfaceTypes SurfaceType { get; private set; }

    public SheetShape_SurfaceShape(SheetShape_Bounds sB, E_ShapeSurfaceTypes sT, List<List<bool>> pD = null):base(sB,pD)
    {
        SurfaceType = sT;
    }

}

class SheetShape_EntityShape : SheetShape
{
    public E_EntityTypes EntityType { get; private set; }

    public SheetShape_EntityShape(SheetShape_Bounds sB, E_EntityTypes eT, List<List<bool>> pD = null) : base(sB, pD)
    {
        EntityType = eT;
    }

}