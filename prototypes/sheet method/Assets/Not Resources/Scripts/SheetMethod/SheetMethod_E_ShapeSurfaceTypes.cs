using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_ShapeSurfaceTypes
{
    Ground,
    Hazard_Goop,
    Hazard_Lava,
    Ice
}

abstract class ShapeSurfaceTypes_CommonFuncs
{
    private static readonly Dictionary<E_ShapeSurfaceTypes, Color> surfaceTypesAndColours = new Dictionary<E_ShapeSurfaceTypes, Color>()
    {
        [E_ShapeSurfaceTypes.Ground] = new Color(0.0f, 0.0f, 0.0f),
        [E_ShapeSurfaceTypes.Hazard_Goop] = new Color(0.59f, 0.18f, 0.76f),
        [E_ShapeSurfaceTypes.Hazard_Lava] = new Color(1.0f, 0.5f, 0.15f),
        [E_ShapeSurfaceTypes.Ice] = new Color(0.8f, 0.824f, 0.933f)

    };

    public static Color GetSurfaceColourBySurfaceType(E_ShapeSurfaceTypes surfaceType)
    {
        if (surfaceTypesAndColours.ContainsKey(surfaceType))
        {
            return surfaceTypesAndColours[surfaceType];
        }
        else
        {
            return surfaceTypesAndColours[E_ShapeSurfaceTypes.Ground];
        }
    }

    public static E_ShapeSurfaceTypes GetSurfaceTypeBySurfaceColour(Color surfaceColour)
    {
        E_ShapeSurfaceTypes ret = E_ShapeSurfaceTypes.Ground;



        Color closestCol = FindClosestSurfaceColour(surfaceColour).closestCol;

        foreach (KeyValuePair<E_ShapeSurfaceTypes, Color> pair in surfaceTypesAndColours)
        {
            if (pair.Value == closestCol)
            {
                ret = pair.Key;
                break;
            }
        }
        return ret;
    }

    public static (Color closestCol, float closestCol_dist) FindClosestSurfaceColour(Color surfaceColour)
    {
        Color ret_closestCol = surfaceTypesAndColours[E_ShapeSurfaceTypes.Ground];

        float ret_closestCol_dist = float.MaxValue;


        foreach (KeyValuePair<E_ShapeSurfaceTypes, Color> pair in surfaceTypesAndColours)
        {
            Vector3 pointA = new Vector3(surfaceColour.r, surfaceColour.g, surfaceColour.b);
            Vector3 pointB = new Vector3(pair.Value.r, pair.Value.g, pair.Value.b);

            float vecDistance = Vector3.Distance(pointA, pointB);
            if (vecDistance < ret_closestCol_dist)
            {
                ret_closestCol_dist = vecDistance;
                ret_closestCol = pair.Value;
            }
        }
        return (ret_closestCol, ret_closestCol_dist);
    }

    public static E_ShapeSurfaceTypes GetSurfaceTypeBySurfaceColour_FromColourSet((E_PixelType pixelType, E_ShapeSurfaceTypes surfaceType, E_EntityTypes entityType, Color pixelColour)[,] colourSet, (int X, int Y) coord)
    {
        Color seekColour = Color.white;

        if (coord.X >= 0 && coord.X < colourSet.GetLength(0))
        {
            if (coord.Y >= 0 && coord.Y < colourSet.GetLength(1))
            {
                seekColour = colourSet[coord.X, coord.Y].pixelColour;
            }
        }

        return GetSurfaceTypeBySurfaceColour(seekColour);
    }
}