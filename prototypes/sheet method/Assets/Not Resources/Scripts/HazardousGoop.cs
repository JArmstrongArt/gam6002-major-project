using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardousGoop : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        TriggerInternal(other, true);

    }

    private void OnTriggerExit(Collider other)
    {
        TriggerInternal(other, false);
    }

    void TriggerInternal(Collider other, bool goopState)
    {
        Player_Statistics playerProof = other.GetComponent<Player_Statistics>();

        if (playerProof != null)
        {
            playerProof.HazardousGoop_Active = goopState;
        }
    }
}
