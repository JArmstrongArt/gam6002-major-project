using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;

public static class ApplyHeightmap_Class
{
    public static (Mesh mesh, List<Material> meshMats, E_ShapeSurfaceTypes meshSurfaceType) ApplyHeightmap(Texture2D heightmapTex, float heightmapPower, (Mesh mesh, List<Material> meshMats, E_ShapeSurfaceTypes meshSurfaceType) meshToAffect, Texture2D levelPlanTex)
    {
        if (heightmapTex != null)
        {
            Texture2D heightmap_file_resized = ResizeTexture_TruePointFiltering_Class.ResizeTexture_TruePointFiltering(heightmapTex, (levelPlanTex.width, levelPlanTex.height));



            NativeArray<Vector3> originalVerts_native = new NativeArray<Vector3>(meshToAffect.mesh.vertices, Allocator.Persistent);
            NativeArray<Vector3> heightmap_adjustedVerts_native = new NativeArray<Vector3>(originalVerts_native.Length, Allocator.Persistent);

            NativeArray<Color> heightmap_colours_native = new NativeArray<Color>(heightmap_file_resized.GetPixels(), Allocator.Persistent);

            ApplyHeightmap_Job applyJob = new ApplyHeightmap_Job()
            {
                originalVerts = originalVerts_native,
                heightmap_adjustedVerts = heightmap_adjustedVerts_native,
                heightmap_dimensions = (heightmap_file_resized.width, heightmap_file_resized.height),
                heightmap_colours = heightmap_colours_native,
                heightmap_power = heightmapPower

            };




            JobHandle applyJob_handle =
            applyJob.Schedule(heightmap_adjustedVerts_native.Length, 64);


            applyJob_handle.Complete();


            meshToAffect.mesh.vertices = applyJob.heightmap_adjustedVerts.ToArray();




            originalVerts_native.Dispose();
            heightmap_adjustedVerts_native.Dispose();
            heightmap_colours_native.Dispose();
        }




        return meshToAffect;

    }

    //[BurstCompile]
    private struct ApplyHeightmap_Job : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<Vector3> originalVerts;

        public NativeArray<Vector3> heightmap_adjustedVerts;

        [ReadOnly]
        public (int width, int height) heightmap_dimensions;

        [ReadOnly]
        public NativeArray<Color> heightmap_colours;

        [ReadOnly]
        public float heightmap_power;




        public void Execute(int i)
        {
            heightmap_adjustedVerts[i] = originalVerts[i];
            int heightmapCoord_closest_X = Mathf.RoundToInt(Mathf.Clamp(heightmap_adjustedVerts[i].x, 0, heightmap_dimensions.width));
            int heightmapCoord_closest_Y = Mathf.RoundToInt(Mathf.Clamp(heightmap_adjustedVerts[i].z, 0, heightmap_dimensions.height));


            int heightmapCoord_closest_1D = ExtendedFunctionality.Theoretical1DIndexFrom2DCoords((heightmapCoord_closest_X, heightmapCoord_closest_Y), heightmap_dimensions.width);
            Color heightmapCoord_colour = heightmap_colours[heightmapCoord_closest_1D];

            float hue, sat, luminosity;

            Color.RGBToHSV(heightmapCoord_colour, out hue, out sat, out luminosity);



            heightmap_adjustedVerts[i] = new Vector3(heightmap_adjustedVerts[i].x, heightmap_adjustedVerts[i].y + (luminosity * heightmap_power), heightmap_adjustedVerts[i].z);



        }

    }
}
