using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;

public static class WavyMeshWaveEffect_Class
{
    public static Vector3[] WavyMeshWaveEffect_ApplyToCopy(Vector3[] origVerts, SineData sineRef_native)
    {

        NativeArray<Vector3> waveVerts_native = new NativeArray<Vector3>(origVerts, Allocator.Persistent);

        WavyMeshWaveEffect_Job waveEffectJob = new WavyMeshWaveEffect_Job
        {
            sineRef = sineRef_native,
            waveVerts = waveVerts_native,
            sineTime = Time.time
        };


        JobHandle waveEffectJob_handle =
        waveEffectJob.Schedule(waveVerts_native.Length, 16);


        waveEffectJob_handle.Complete();



        Vector3[] newVerts = waveEffectJob.waveVerts.ToArray();


        waveVerts_native.Dispose();

        return newVerts;

    }


    //[BurstCompile]
    private struct WavyMeshWaveEffect_Job : IJobParallelFor
    {
        [ReadOnly]
        public SineData sineRef;


        public NativeArray<Vector3> waveVerts;

        //cant use Time.time in a job
        [ReadOnly]
        public float sineTime;

        public void Execute(int i)
        {
            //axis*delay


            SineData sineRef_individual = sineRef;


            sineRef_individual.sineDelayFromWorldTime = ExtendedFunctionality.GetSineDelayAsMultiplierOfAxis(waveVerts[i], new E_WorldAxes[2] { E_WorldAxes.X, E_WorldAxes.Z }, sineRef.sineDelayFromWorldTime); 


            waveVerts[i] = ExtendedFunctionality.GetSinePos(sineRef_individual, waveVerts[i],sineTime);


        }

    }

}
