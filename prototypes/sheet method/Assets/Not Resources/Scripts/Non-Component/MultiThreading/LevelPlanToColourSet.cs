using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;

public struct ColourSetEntry
{
    public E_PixelType pixelType { get; private set; }
    public E_ShapeSurfaceTypes? surfaceType;
    public E_EntityTypes? entityType;
    public Color pixelColour;

    public ColourSetEntry(Color pCol, E_PixelType pType = E_PixelType.None, E_ShapeSurfaceTypes? sType = null, E_EntityTypes? eType = null)
    {
        pixelColour = pCol;
        pixelType = pType;
        surfaceType = sType;
        entityType = eType;
    }
}

public static class LevelPlanToColourSet_Class
{
    private static readonly Color pixelType_nothing = new Color(1.0f, 1.0f, 1.0f);




    static float GetDistanceFromNothingColour(Color otherCol)
    {




        Vector3 pointA = new Vector3(otherCol.r, otherCol.g, otherCol.b);
        Vector3 pointB = new Vector3(pixelType_nothing.r, pixelType_nothing.g, pixelType_nothing.b);

        float ret = Vector3.Distance(pointA, pointB);

        return ret;
    }

    public static ColourSetEntry[,] LevelPlanToColourSet(Texture2D levelPlan)
    {
        Texture2D levelPlan_copyTexture = ResizeTexture_TruePointFiltering_Class.ResizeTexture_TruePointFiltering(levelPlan, (levelPlan.width, levelPlan.height));


        Color[] levelPlan_allColours = levelPlan_copyTexture.GetPixels();



        ColourSetEntry[,] allColours_formatted = new ColourSetEntry[levelPlan_copyTexture.width, levelPlan_copyTexture.height];


        NativeArray<Color> allColours_native = new NativeArray<Color>(levelPlan_allColours, Allocator.Persistent);
        NativeArray<ColourSetEntry> colourSet1D_out = new NativeArray<ColourSetEntry>(levelPlan_allColours.Length, Allocator.Persistent);

        LevelPlanToColourSet_Job determineEntryJob = new LevelPlanToColourSet_Job()
        {
            levelPlan_allColours = allColours_native,
            colourSet_1D = colourSet1D_out
        };

        JobHandle determineEntryJob_handle =
        determineEntryJob.Schedule(levelPlan_allColours.Length, 64);

        determineEntryJob_handle.Complete();



        for(int i=0;i< colourSet1D_out.Length; i++)
        {
            (int writeX, int writeY) writeCoords = ExtendedFunctionality.Theoretical2DCoordsFrom1DIndex(i, allColours_formatted.GetLength(0));
            allColours_formatted[writeCoords.writeX, writeCoords.writeY] = colourSet1D_out[i];
        }

        allColours_native.Dispose();
        colourSet1D_out.Dispose();


        return allColours_formatted;


    }



    //[BurstCompile]
    //no burstcompile bc this uses dictionaries which are a managed data type
    private struct LevelPlanToColourSet_Job : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<Color> levelPlan_allColours;

        public NativeArray<ColourSetEntry> colourSet_1D;

        public void Execute(int i)
        {
            Color levelPlan_allColours_curColour = levelPlan_allColours[i];

            if (levelPlan_allColours_curColour.a <= 0.5f)
            {
                levelPlan_allColours_curColour = pixelType_nothing;
            }


            (Color closestCol, float closestCol_dist) surfaceCol_dist = ShapeSurfaceTypes_CommonFuncs.FindClosestSurfaceColour(levelPlan_allColours_curColour);
            (Color closestCol, float closestCol_dist) entityCol_dist = EntityTypes_CommonFuncs.FindClosestEntityColour(levelPlan_allColours_curColour);
            float closestCol_toNothing = GetDistanceFromNothingColour(levelPlan_allColours_curColour);

            E_PixelType ret_pixelType = E_PixelType.None;
            E_ShapeSurfaceTypes ret_surfType = E_ShapeSurfaceTypes.Ground;
            E_EntityTypes ret_entType = E_EntityTypes.Player;
            Color ret_pixelColour = pixelType_nothing;

            if (surfaceCol_dist.closestCol_dist < entityCol_dist.closestCol_dist && surfaceCol_dist.closestCol_dist < closestCol_toNothing)
            {
                ret_pixelType = E_PixelType.Surface;
                ret_surfType = ShapeSurfaceTypes_CommonFuncs.GetSurfaceTypeBySurfaceColour(surfaceCol_dist.closestCol);
                ret_pixelColour = surfaceCol_dist.closestCol;
            }

            if (entityCol_dist.closestCol_dist < surfaceCol_dist.closestCol_dist && entityCol_dist.closestCol_dist < closestCol_toNothing)
            {
                ret_pixelType = E_PixelType.Entity;
                ret_entType = EntityTypes_CommonFuncs.GetEntityTypeByEntityColour(entityCol_dist.closestCol);
                ret_pixelColour = entityCol_dist.closestCol;
            }





            colourSet_1D[i] = new ColourSetEntry(ret_pixelColour, ret_pixelType, ret_surfType, ret_entType);


        }

    }
}
