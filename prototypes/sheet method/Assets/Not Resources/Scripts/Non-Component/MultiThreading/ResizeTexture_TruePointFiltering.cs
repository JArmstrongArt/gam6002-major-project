using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;

public static class ResizeTexture_TruePointFiltering_Class
{

    //after testing, setting filterMode on the Texture2D class to 'point' still produces distortion at extreme compressions, not good enough for this project, here's true point filtering
    public static Texture2D ResizeTexture_TruePointFiltering(Texture2D tex, (int targetWidth, int targetHeight) targetSize)
    {


        Texture2D tex_scaled = new Texture2D(targetSize.targetWidth, targetSize.targetHeight);

        float scalarX_in = (float)tex.width / (float)targetSize.targetWidth;//this is how much you have to multiply the already resized x by to get to the original
        float scalarY_in = (float)tex.height / (float)targetSize.targetHeight;

        Color[] texPixels = tex.GetPixels();

        int outSize = tex_scaled.width * tex_scaled.height;

        NativeArray<Color> beforePixels = new NativeArray<Color>(texPixels, Allocator.Persistent);
        NativeArray<Color> afterPixels = new NativeArray<Color>(outSize, Allocator.Persistent);


        ResizeTexture_TruePointFiltering_Job resizeJob = new ResizeTexture_TruePointFiltering_Job()
        {
            originalTexWidth = tex.width,
            resizedTexWidth = tex_scaled.width,
            imagePixels_in = beforePixels,
            imagePixels_out = afterPixels,
            scalarX = scalarX_in,
            scalarY = scalarY_in
        };




        JobHandle resizeJob_handle =
        resizeJob.Schedule(outSize, 64);


        resizeJob_handle.Complete();


        tex_scaled.SetPixels(resizeJob.imagePixels_out.ToArray());

        beforePixels.Dispose();
        afterPixels.Dispose();

        return tex_scaled;



    }

    [BurstCompile]
    private struct ResizeTexture_TruePointFiltering_Job : IJobParallelFor
    {
        [ReadOnly]
        public int originalTexWidth;

        [ReadOnly]
        public int resizedTexWidth;

        [ReadOnly]
        public NativeArray<Color> imagePixels_in;

        public NativeArray<Color> imagePixels_out;

        [ReadOnly]
        public float scalarX;

        [ReadOnly]
        public float scalarY;



        public void Execute(int i)
        {
            (int resizedX, int resizedY) resizedCoords = ExtendedFunctionality.Theoretical2DCoordsFrom1DIndex(i, resizedTexWidth);


            int normalSizeX = Mathf.RoundToInt(resizedCoords.resizedX * scalarX);
            int normalSizeY = Mathf.RoundToInt(resizedCoords.resizedY * scalarY);

            int normalSizeI = (normalSizeY * originalTexWidth) + normalSizeX;

            bool normalSizeI_valid = normalSizeI >= 0 && normalSizeI < imagePixels_in.Length;
            if (normalSizeI_valid)
            {



                imagePixels_out[i] = imagePixels_in[normalSizeI];

            }



        }

    }
}
