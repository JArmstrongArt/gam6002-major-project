﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;

using System.Linq;


//this somehow ended up SLOWER than just doing it on a single thread, probably due to my limited understanding of nativestream
//as this is the case, i have commented out the entire class BUT it's my best reference right now for writing to a nativestream since
//the only explanation of how nativestream works is in the collections folder, no online docs that r that substantial to speak of
/*
public static class TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile_Class
{
    public static List<(int X, int Y)> TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile(List<(int X, int Y)> coordSet, SheetMesh_GridTile tile)
    {


        NativeArray<(int X, int Y)> coordSet_native = new NativeArray<(int X, int Y)>(coordSet.ToArray(), Allocator.Persistent);
        NativeStream coordsWithinTile_native = new NativeStream(coordSet_native.Length, Allocator.Persistent);

        TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile_Job coordJob = new TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile_Job()
        {
            coordSet = coordSet_native,
            coordsWithinTile_writer = coordsWithinTile_native.AsWriter(),
            tile_topLeft = tile.TopLeftCoord,
            tile_dimensions = tile.TileDimensions

        };

        JobHandle coordJob_handle =
coordJob.Schedule(coordSet_native.Length, 16);


        coordJob_handle.Complete();

        NativeArray<(int X, int Y)> ret_asArray = coordsWithinTile_native.ToNativeArray<(int X, int Y)>(Allocator.Persistent);

        List<(int X, int Y)> ret = ret_asArray.ToList();



        coordSet_native.Dispose();
        coordsWithinTile_native.Dispose();
        ret_asArray.Dispose();

        return ret;
    }


    //[BurstCompile]
    private struct TurnShapeIntoBaseContextualizedVertices_GetAllCoordsWithinTile_Job : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<(int X, int Y)> coordSet;




        public NativeStream.Writer coordsWithinTile_writer;


        [ReadOnly]
        public (int X, int Y) tile_topLeft;

        [ReadOnly]
        public (int X, int Y) tile_dimensions;


        public void Execute(int i)
        {
            coordsWithinTile_writer.BeginForEachIndex(i);

            (int X, int Y) coord = coordSet[i];
            bool xValid = coord.X >= tile_topLeft.X && coord.X < tile_topLeft.X + (tile_dimensions.X - 1);
            bool yValid = coord.Y >= tile_topLeft.Y && coord.Y < tile_topLeft.Y + (tile_dimensions.Y - 1);
            if (xValid && yValid)
            {
                coordsWithinTile_writer.Write(coord);
            }

            coordsWithinTile_writer.EndForEachIndex();

        }

    }
}
*/