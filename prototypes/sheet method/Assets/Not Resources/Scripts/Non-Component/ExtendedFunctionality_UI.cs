using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public static class ExtendedFunctionality_UI
{

    public static void SetupRectTransform(RectTransform subject, Vector3? position = null, Vector2? dimensions = null, Vector2? anchorMin = null, Vector2? anchorMax = null, Vector2? pivot = null, Vector3? rotation = null, Vector3? scale = null)
    {
        if (subject != null)
        {


            if (anchorMin != null)
            {
                subject.anchorMin = anchorMin ?? default(Vector2);
            }

            if (anchorMax != null)
            {
                subject.anchorMax = anchorMax ?? default(Vector2);
            }

            if (pivot != null)
            {
                subject.pivot = pivot ?? default(Vector2);
            }

            if (position != null)
            {
                subject.anchoredPosition = position ?? default(Vector3);
            }

            if (dimensions != null)
            {
                subject.sizeDelta = dimensions ?? default(Vector2);
            }

            if (rotation != null)
            {
                subject.rotation = Quaternion.Euler(rotation ?? default(Vector3));
            }

            if (scale != null)
            {
                subject.localScale = scale ?? default(Vector3);
            }



        }
    }

}
