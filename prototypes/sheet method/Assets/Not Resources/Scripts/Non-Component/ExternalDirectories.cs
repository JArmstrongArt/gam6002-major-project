using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SimpleFileBrowser;

public static class ExternalDirectories
{
    public static readonly string DIR_ROOT = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);

    public static readonly string DIR_MAIN = "PlatforMagic";
    public static readonly string DIR_HEIGHTMAPS = "HeightMaps";
    public static readonly string DIR_LEVELPLANS = "Level Plans";
    public static readonly string DIR_SAVEDLEVELS = "Your Created Levels";

    public static readonly FileBrowser.Filter FILTER_IMAGES = new FileBrowser.Filter("Rasterized Images", ".jpg", ".jpeg", ".png");

    static readonly string DIR_PREMADELEVELS = "Levels";
    public static List<string> SAVEDLEVELSDIR_FULLPIECES
    {
        get
        {
            List<string> ret = new List<string>
            {
            ExternalDirectories.DIR_ROOT,
            ExternalDirectories.DIR_MAIN,
            ExternalDirectories.DIR_SAVEDLEVELS

            };

            ExternalDirectories.EnsureFolderExists(ret);
            return ret;
        }
    }

    public static List<string> PREMADELEVELSDIR_FULLPIECES
    {
        get
        {
            List<string> ret = new List<string>
            {
            Application.persistentDataPath,
            DIR_PREMADELEVELS

            };

            ExternalDirectories.EnsureFolderExists(ret);
            return ret;
        }
    }

    public static string EnsureFolderExists(List<string> directoryPieces)
    {



        string directory = ExternalDirectories.SanitizePath(directoryPieces);



        return EnsureFolderExists(directory);
        
    }

    static string EnsureFolderExists(string directory)
    {

        


        directory = ExternalDirectories.SanitizePath(directory);


        if (!System.IO.Directory.Exists(directory))
        {
            System.IO.Directory.CreateDirectory(directory);

        }



        return directory;
    }

    static string UnifySlashes(string path)
    {


        
        while (path.Contains("/"))
        {
            path = path.Replace("/", @"\");
        }

        while (path.Contains(@"\\"))
        {
            path = path.Replace(@"\\",@"\");
        }

        return path;
    }

    static string DirectifyString(string path, bool includeBeginningSlash=true, bool includeEndSlash=true)
    {
        if (path.Length > 0)
        {
            path = UnifySlashes(path);


            if (includeBeginningSlash)
            {
                if (path[0].ToString() != @"\")
                {
                    path = @"\" + path;
                }
            }
            else
            {
                if (path[0].ToString() == @"\")
                {
                    int subBeginIndex = 1;
                    int subEndIndex = path.Length;
                    int subLength = subEndIndex - subBeginIndex;
                    path = path.Substring(subBeginIndex, subLength);
                }
            }

            if (includeEndSlash)
            {
                if (path[path.Length - 1].ToString() != @"\")
                {
                    path += @"\";
                }
            }
            else
            {

                if (path[path.Length - 1].ToString() == @"\")
                {
                    int subBeginIndex = 0;
                    int subEndIndex = path.Length-1;
                    int subLength = subEndIndex - subBeginIndex;
                    path = path.Substring(subBeginIndex, subLength);


                }

            }

        }

        return path;
    }

    public static string SanitizePath(List<string> pathPieces, bool fileMode = false)
    {
        string finalPth = "";
        for(int i = 0; i < pathPieces.Count; i++)
        {
            string pthCur = pathPieces[i];


            finalPth += "/"+pthCur+"/";
        }


        return SanitizePath(finalPth,fileMode);


    }

    public static string SanitizePath(string fullPath, bool fileMode=false)
    {


        fullPath = DirectifyString(fullPath, false, !fileMode);
        fullPath = UnifySlashes(fullPath);


        return fullPath;

    }

    //snatched from https://answers.unity.com/questions/432655/loading-texture-file-from-pngjpg-file-on-disk.html
    public static Texture2D LoadTextureFromDisk(string filePath)
    {
        filePath = SanitizePath(filePath,true);

        Texture2D tex = null;


        if (System.IO.File.Exists(filePath))
        {
            byte[] fileData = System.IO.File.ReadAllBytes(filePath);
            tex = LoadTextureFromBytes(fileData);
        }
        return tex;
    }

    public static Texture2D LoadTextureFromBytes(byte[] imgBytes)
    {
        Texture2D ret = null;

        if (imgBytes != null)
        {
            ret = new Texture2D(2, 2);
            ret.LoadImage(imgBytes); //..this will auto-resize the texture dimensions.
        }

        return ret;
    }

    static Texture2D[] LoadAllTexturesFromByteList(List<byte[]> imgBytes_ALL)
    {
        Texture2D[] ret = null;

        if(imgBytes_ALL!=null && imgBytes_ALL.Count > 0)
        {
            ret = new Texture2D[imgBytes_ALL.Count];

            for (int i = 0; i < imgBytes_ALL.Count; i++)
            {
                ret[i] = LoadTextureFromBytes(imgBytes_ALL[i]);
            }
        }

        return ret;
    }

    public static List<(Texture2D lvlPlanTex, float baseRaise)> LoadAllTexturesFromByteList(List<(byte[] imgBytes, float baseRaise)> imgBytes_ALL)
    {
        List<(Texture2D lvlPlanTex, float baseRaise)> ret = null;

        if (imgBytes_ALL != null && imgBytes_ALL.Count > 0)
        {
            ret = new List<(Texture2D lvlPlanTex, float baseRaise)>();

            for (int i = 0; i < imgBytes_ALL.Count; i++)
            {
                (Texture2D lvlPlanTex, float baseRaise) addElem = (LoadTextureFromBytes(imgBytes_ALL[i].imgBytes), imgBytes_ALL[i].baseRaise);


                ret.Add(addElem);

            }
        }

        return ret;
    }
}
