using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using UnityEngine.SceneManagement;
public static class ExtendedFunctionality
{
    public static string ZeroOneValueAsPercent(float zeroOne)
    {
        zeroOne = Mathf.Clamp01(zeroOne);
        return Mathf.CeilToInt(zeroOne * 100).ToString() + "%";
    }


    //swiped from https://answers.unity.com/questions/1146685/how-to-determine-whether-a-scene-with-name-exists.html
    //no case sensetive sanitization because the loadscenebyname function needs to be exact case, it will just serve to cause problems
    public static bool SceneWithNameExists(string sceneName)
    {
        bool ret = false;

        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            string scenePath = SceneUtility.GetScenePathByBuildIndex(i);
            int lastSlash = scenePath.LastIndexOf("/");

            string sceneName_found = scenePath.Substring(lastSlash + 1, scenePath.LastIndexOf(".") - lastSlash - 1);






            if (sceneName_found == sceneName)
            {

                ret = true;
                break;
            }
        }

        return ret;
    }


    static float GetSine(E_SineType sineType, float sinePower, float sineFrequency, float sineDelayFromWorldTime = 0.0f, float? timeOverride=null)
    {
        float sineTime = timeOverride ?? Time.time;
        float sine = 0;
        switch (sineType)
        {
            case E_SineType.Sine:
                sine = Mathf.Sin((sineTime + sineDelayFromWorldTime) * sineFrequency) * sinePower;
                break;
            case E_SineType.Cosine:
                sine = Mathf.Cos((sineTime + sineDelayFromWorldTime) * sineFrequency) * sinePower;
                break;
        }

        return sine;
    }

    public static float GetSineDelayAsMultiplierOfAxis(Vector3 sineCandidates, E_WorldAxes[] axisToOperate, float sineDelay)
    {
        return GetSineDelayAsMultiplierOfAxis_Internal(sineCandidates, axisToOperate, sineDelay);



    }

    public static float GetSineDelayAsMultiplierOfAxis(Vector3 sineCandidates, E_WorldAxes axisToOperate, float sineDelay)
    {
        return GetSineDelayAsMultiplierOfAxis_Internal(sineCandidates, new E_WorldAxes[1] { axisToOperate}, sineDelay);



    }

    static float GetSineDelayAsMultiplierOfAxis_Internal(Vector3 sineCandidates, E_WorldAxes[] axisToOperate, float sineDelay)
    {
        float ret = 0;

        for(int i = 0; i < axisToOperate.Length; i++)
        {
            switch (axisToOperate[i])
            {
                case E_WorldAxes.X:
                    ret+= sineCandidates.x * sineDelay;
                    break;

                case E_WorldAxes.Y:
                    ret += sineCandidates.y * sineDelay;
                    break;


                case E_WorldAxes.Z:
                    ret += sineCandidates.z * sineDelay;
                    break;


                default:
                    ret += sineDelay;
                    break;

            }
        }

        return ret;



    }


    public static Vector3 GetSinePos(SineData sineRef, Vector3? rootPosOverride=null, float? timeOverride=null)
    {
        float sineTime = timeOverride ?? Time.time;
        Vector3 rootPos = rootPosOverride ?? sineRef.RootPos;


        Vector3 ret = Vector3.zero;



        float sineAdd = GetSine(sineRef.SineType, sineRef.SinePower, sineRef.SineFrequency, sineRef.sineDelayFromWorldTime, sineTime);


        switch (sineRef.SineAxis)
        {
            case E_WorldAxes.X:




                ret = new Vector3(rootPos.x + sineAdd, rootPos.y, rootPos.z);
                break;
            case E_WorldAxes.Y:


                ret = new Vector3(rootPos.x, rootPos.y + sineAdd, rootPos.z);
                break;
            case E_WorldAxes.Z:


                ret = new Vector3(rootPos.x, rootPos.y, rootPos.z + sineAdd);
                break;
        }

        return ret;
    }




    public static (int X, int Y) Theoretical2DCoordsFrom1DIndex(int index, int XDimensionOf2DArray)
    {
        int y = Mathf.FloorToInt((float)index / (float)XDimensionOf2DArray);
        int x = index - (y * XDimensionOf2DArray);

        return (x, y);
    }

    public static int Theoretical1DIndexFrom2DCoords((int x, int y) coord, int XDimensionOf2DArray)
    {
        return (coord.y * XDimensionOf2DArray) + coord.x;
    }


    public static float GetAverageOfVec3Dimensions(Vector3 dimensions)
    {
        return (float)(dimensions.x + dimensions.y + dimensions.z) / (float)3;
    }

    public static void SafeDestroy(GameObject obj)
    {


        if (obj != null)
        {

            if (Application.isEditor && !Application.isPlaying)
            {
                GameObject.DestroyImmediate(obj);
            }
            else
            {
                GameObject.Destroy(obj);

            }
        }
    }

    public static void SafeDestroy(Component comp)
    {


        if (comp != null)
        {

            if (Application.isEditor && !Application.isPlaying)
            {
                Component.DestroyImmediate(comp);
            }
            else
            {
                Component.Destroy(comp);

            }
        }
    }

    public static void SafeDestroy(List<GameObject> objs)
    {
        if (objs != null)
        {
            foreach (GameObject obj in objs)
            {
                ExtendedFunctionality.SafeDestroy(obj);
            }

            objs.Clear();

        }


    }



    //renderer.bounds is in world space whereas mesh.bounds is in local space, so i have to get renderer here instead of mesh since attempts to convert the mesh.bounds to world space were unsuccessful

    public static Vector3? AttemptWorldBoundCenterGet(GameObject subject)
    {
        Vector3? ret = null;
        Renderer rendCheck = subject.GetComponent<Renderer>();

        if (rendCheck != null)
        {
            ret = rendCheck.bounds.center;
        }
        return ret;
    }





    private static float GetHalfHeightOfGameObject_Internal(GameObject obj, E_WorldAxes axisToMeasure)
    {
        float ret = 0;
        Collider objCol = obj.GetComponent<Collider>();
        MeshFilter objMeshFilt = obj.GetComponent<MeshFilter>();

        Vector3 colSize = Vector3.zero;

        if (objCol != null)
        {
            colSize = objCol.bounds.size;

        }
        else
        {
            if (objMeshFilt != null)
            {
                colSize = objMeshFilt.mesh.bounds.size;
            }
        }

        switch (axisToMeasure)
        {
            case E_WorldAxes.X:
                ret = ((float)colSize.x * obj.transform.localScale.x) / (float)2;

                break;
            case E_WorldAxes.Y:
                ret = ((float)colSize.y * obj.transform.localScale.y) / (float)2;

                break;
            case E_WorldAxes.Z:
                ret = ((float)colSize.z * obj.transform.localScale.z) / (float)2;

                break;
        }

        return ret;


    }

    public static float GetHalfMeasureOfGameObject(GameObject obj,E_WorldAxes axisToMeasure)
    {
        CharacterController objChar = obj.GetComponent<CharacterController>();

        if (objChar != null)
        {
            return GetHalfHeightOfCharacterController(objChar, axisToMeasure);
        }
        else
        {
            return GetHalfHeightOfGameObject_Internal(obj, axisToMeasure);
        }
    }

    private static float GetHalfHeightOfCharacterController(CharacterController cont, E_WorldAxes axisToMeasure)
    {
        float ret = 0;
        switch (axisToMeasure)
        {
            case E_WorldAxes.X:
                ret = ((float)cont.radius * cont.transform.localScale.x) / (float)2;


                break;
            case E_WorldAxes.Y:
                ret = ((float)cont.height * cont.transform.localScale.y) / (float)2;


                break;
            case E_WorldAxes.Z:
                ret=((float)cont.radius * cont.transform.localScale.z) / (float)2;


                break;
        }
        return ret;


    }




    public static void GutComponentsFromGameObject(GameObject gO, List<System.Type> whitelistComponentTypes=null, bool workRecursively=true, bool keepChildrenOfWhitelistTypes=true)
    {

        List<GameObject> affectedObjs = new List<GameObject>();
        affectedObjs.Add(gO);


        if (workRecursively)
        {
            //relies upon GetComponentsInChildren working recursively, which it does
            List<Transform> recursiveChildren = gO.GetComponentsInChildren<Transform>(true).ToList();

            if (recursiveChildren != null)
            {
                foreach (Transform certainChild in recursiveChildren)
                {
                    affectedObjs.Add(certainChild.gameObject);
                }
            }

        }

        foreach(GameObject gO_current in affectedObjs)
        {
            List<Component> collectedObj_allComp = gO_current.GetComponents<Component>().ToList();





            if (whitelistComponentTypes == null)
            {
                whitelistComponentTypes = new List<System.Type>();
            }


            if (!whitelistComponentTypes.Contains(typeof(Transform)))
            {
                whitelistComponentTypes.Add(typeof(Transform));
            }

            for (int i = 0; i < collectedObj_allComp.Count; i++)
            {
                Component comp = collectedObj_allComp[i];

                bool castableComp = false;

                foreach (System.Type typ in whitelistComponentTypes)
                {
                    Type typ_base = comp.GetType();

                    if (keepChildrenOfWhitelistTypes)
                    {

                        while (typ_base != null)
                        {
                            if (typ_base == typ)
                            {
                                castableComp = true;
                                break;
                            }

                            typ_base = typ_base.BaseType;
                        }

                        if (castableComp)
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (typ_base == typ)
                        {
                            castableComp = true;
                            break;
                        }
                    }



                }

                if (!castableComp)
                {
                    Component.Destroy(collectedObj_allComp[i]);
                    collectedObj_allComp.RemoveAt(i);
                    i -= 1;
                }


            }
        }



    }

    public static void DirectMoveCharacterController_FallbackIfNotPresent(GameObject gO, Vector3 newPos)
    {
        CharacterController charCon = gO.GetComponent<CharacterController>();

        if (charCon != null)
        {
            charCon.enabled = false;
            gO.transform.position = newPos;
            charCon.enabled = true;
        }
        else
        {
            gO.transform.position = newPos;
        }

    }

    public static T GetComponentInChildrenByName<T>(GameObject subject, string childName, bool caseSensetive=false)
    {
        //this function relies upon GetComponentsInChildren returning the results in the same order no matter executing using () or <>
        //this is because i iterate using allComponents but return using allComponents_asT, they have to be the same length and order.
        T ret = default(T);
        Component[] allComponents = subject.GetComponentsInChildren(typeof(T));
        T[] allComponents_asT = subject.GetComponentsInChildren<T>();


        string thisName = childName;

        if (!caseSensetive)
        {
            thisName = thisName.ToLower();
        }

        for(int i=0;i<allComponents.Length;i++)
        {
            Component comp = allComponents[i];
            string compareName = comp.gameObject.name;

            if (!caseSensetive)
            {
                compareName = compareName.ToLower();
            }

            //safety check in case (clone) is appended
            if (compareName.Contains(thisName))
            {
                ret = allComponents_asT[i];
                break;
            }
        }
        return ret;
    }


    //only works on system.serializable classes
    //snatched from https://stackoverflow.com/questions/129389/how-do-you-do-a-deep-copy-of-an-object-in-net
    public static T DeepCopy<T>(T obj)
    {



        
        using (var ms = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, obj);
            ms.Position = 0;

            return (T)formatter.Deserialize(ms);
        }
    }

    //uses reflection, so its nice and unbelievably slow and bad
    //however, very useful
    //swiped from https://stackoverflow.com/questions/1196991/get-property-value-from-string-using-reflection
    public static T GetFieldFromClassByName<T>(object obj, string fieldName, bool publicOverride=false)
    {
        System.Reflection.BindingFlags accessorFlags = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;

        if (publicOverride)
        {
            accessorFlags = System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance;
        }


        return (T)obj.GetType().GetField(fieldName, accessorFlags).GetValue(obj);
    }

    public static T GetPropertyFromClassByName<T>(object obj, string propName, bool publicOverride = false)
    {
        System.Reflection.BindingFlags accessorFlags = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;

        if (publicOverride)
        {
            accessorFlags = System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance;
        }


        return (T)obj.GetType().GetProperty(propName, accessorFlags).GetValue(obj);
    }

    public static Color RainbowRandomThisColour(Color? baseCol=null)
    {
        Color baseColour_nonNull = baseCol ?? new Color(1, 0, 0);

        float baseCol_h, baseCol_s,baseCol_v;

        Color.RGBToHSV(baseColour_nonNull, out baseCol_h, out baseCol_s, out baseCol_v);

        baseCol_h = UnityEngine.Random.Range(0.0f, 1.0f);

        baseColour_nonNull = Color.HSVToRGB(baseCol_h, baseCol_s, baseCol_v);
        return baseColour_nonNull;

    }
    public static string MakeStringIntoOnlyNumbers(string baseString)
    {
        List<char> numWhitelist = new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        string ret = "";

        foreach(char cha in baseString)
        {
            if (numWhitelist.Contains(cha))
            {
                ret += cha;
            }
        }

        return ret;
    }

    public static string GenerateLevelID(string? levelName=null)
    {
        string levelName_nonNull = levelName ?? "unnnamedLevel";

        return levelName_nonNull + "_" + ExtendedFunctionality.MakeStringIntoOnlyNumbers(System.DateTime.Now.ToString());
    }
}
