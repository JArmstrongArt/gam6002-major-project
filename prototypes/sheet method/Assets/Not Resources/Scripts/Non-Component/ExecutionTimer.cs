using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExecutionTimer
{
    static Dictionary<System.Type, float> executionTimerTracker = new Dictionary<System.Type, float>();


    static float GetTimer(System.Type associatedScr)
    {
        float returnTime = 0.0f;

        foreach (KeyValuePair<System.Type, float> timer in executionTimerTracker)
        {
            if (timer.Key == associatedScr)
            {
                returnTime = timer.Value;
                break;
            }
        }

        return returnTime;
    }



    public static void PrintTime(System.Type associatedType, string taskName, bool[] activationConditions=null, bool relativeToLastPrint = true)
    {
        bool allowPrint = true;

        if (activationConditions != null)
        {


            for(int i = 0; i < activationConditions.Length; i++)
            {
                if (activationConditions[i] == false)
                {
                    allowPrint = false;
                    break;
                }
            }
        }

        if (allowPrint)
        {
            string timeToPrint = Time.realtimeSinceStartup.ToString();

            if (relativeToLastPrint)
            {
                timeToPrint = (Time.realtimeSinceStartup - GetTimer(associatedType)).ToString();
            }


            Debug.Log(associatedType.ToString() + ": " + taskName.ToString() + " - " + timeToPrint);
            executionTimerTracker[associatedType] = Time.realtimeSinceStartup;
        }




    }
}
