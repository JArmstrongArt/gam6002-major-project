using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

using System.IO;

using System.Runtime.Serialization.Formatters.Binary;


[System.Serializable]
public enum E_LevelProgress_CriteriaType
{
    LevelCleared,
    AllCollectables,
    AllEnemies,
    NoDeaths
}





[System.Serializable]
public sealed class LevelProgress
{

    private LevelFile_ProgressHookData progressData = new LevelFile_ProgressHookData();

    public LevelFile_ProgressHookData ProgressData
    {
        get
        {
            if (progressData == null)
            {
                return new LevelFile_ProgressHookData();
            }
            else
            {
                return progressData;
            }
        }

    }


    List<E_LevelProgress_CriteriaType> achievedCriteria = new List<E_LevelProgress_CriteriaType>();
    public LevelProgress(LevelFile_ProgressHookData pD)
    {
        if (pD != null)
        {
            progressData = pD;

        }


    }

    public void AddAchievedCriteria(E_LevelProgress_CriteriaType criteria, bool optionalCondition = true)
    {
        if (!achievedCriteria.Contains(criteria) && optionalCondition)
        {
            achievedCriteria.Add(criteria);
        }
    }

    public bool HasBeenAchieved(E_LevelProgress_CriteriaType criteria)
    {
        return achievedCriteria.Contains(criteria);
    }



    public float GetCompletionProgress()
    {
        int allCriteriaCount = System.Enum.GetValues(typeof(E_LevelProgress_CriteriaType)).Cast<E_LevelProgress_CriteriaType>().ToArray().Length;

        if (allCriteriaCount <= 0)
        {
            return 0;

        }
        else
        {
            return Mathf.Clamp01(((float)1 / (float)allCriteriaCount) * achievedCriteria.Count);

        }
    }


}
[System.Serializable]
public sealed class AllLevelProgress
{
    List<LevelProgress> progress = new List<LevelProgress>();




    public float GetCompletionProgress_Total(E_LevelFolderToLoad progressFilter)
    {
        int progressElements = 0;
        float totalProgress = 0;

        List<LevelFile> existingProgressComparisons = LevelFileManager.LoadLevelFileDirectory(progressFilter);

        foreach(LevelFile file in existingProgressComparisons)
        {
            if (file != null)
            {
                if (file.progressHookData != null)
                {
                    bool progressExists = false;
                    foreach (LevelProgress prog in progress)
                    {
                        if (prog != null)
                        {
                            if (prog.ProgressData != null)
                            {
                                if (prog.ProgressData.LevelID == file.progressHookData.LevelID)
                                {
                                    totalProgress += prog.GetCompletionProgress();
                                    progressElements += 1;
                                    progressExists = true;
                                    break;
                                }
                            }

                        }
                    }

                    if (progressExists == false)
                    {
                        totalProgress += 0;
                        progressElements += 1;
                    }
                }

            }
        }




        if (progressElements <= 0)
        {
            return 0;

        }
        else
        {
            return Mathf.Clamp01((float)totalProgress / (float)progressElements);

        }
    }

    public void SetProgressByLevelID(LevelProgress progToSet)
    {
        if (progToSet == null)
        {
            progToSet = new LevelProgress(new LevelFile_ProgressHookData());
        }

        bool progressExists = false;

        for(int i = 0; i < progress.Count; i++)
        {
            if (progress[i] != null)
            {
                if (progress[i].ProgressData.LevelID == progToSet.ProgressData.LevelID)
                {
                    progressExists = true;
                    progress[i] = ExtendedFunctionality.DeepCopy(progToSet);
                    break;
                }
            }
        }

        if (!progressExists)
        {
            progress.Add(progToSet);
        }


    }

    public LevelProgress GetProgressByLevelID(LevelFile_ProgressHookData progHookData)
    {
        LevelFile_ProgressHookData progHookData_new = ExtendedFunctionality.DeepCopy(progHookData);
        LevelProgress ret = new LevelProgress(ExtendedFunctionality.DeepCopy(progHookData_new));
        foreach(LevelProgress prog in progress)
        {
            if (prog != null)
            {
                if (prog.ProgressData.LevelID == progHookData_new.LevelID)
                {
                    ret = ExtendedFunctionality.DeepCopy(prog);
                    break;
                }
            }
        }
        return ret;
    }
}


public static class LevelProgressManager
{
    static readonly string progressExtension = ".jerma";
    static readonly List<string> progressPath = new List<string> { Application.persistentDataPath };
    static readonly List<string> progressPath_withFile = progressPath.Concat(new List<string> { "levelProgress"+ progressExtension }).ToList();

    static AllLevelProgress currentProgressFile = new AllLevelProgress();

    public static float TotalCompletionProgress(E_LevelFolderToLoad totalCompletionFolder)
    {
        return currentProgressFile.GetCompletionProgress_Total(totalCompletionFolder);
    }








    public static void SetProgressByLevelID_Exposer(LevelProgress progToSet, bool saveToFileAfterwards=true)
    {
        currentProgressFile.SetProgressByLevelID(progToSet);

        if (saveToFileAfterwards)
        {
            SaveProgress();
        }
    }

    public static LevelProgress GetProgressByLevelID_Exposer(LevelFile_ProgressHookData progData)
    {
        return currentProgressFile.GetProgressByLevelID(progData);
    }




    public static void SaveProgress()
    {
        ExternalDirectories.EnsureFolderExists(progressPath);

        string savePath = ExternalDirectories.SanitizePath(progressPath_withFile, true);




        FileStream progressFile_stream = new FileStream(savePath, FileMode.Create);

        BinaryFormatter progressFile_converter = new BinaryFormatter();

        if (currentProgressFile != null)
        {
            progressFile_converter.Serialize(progressFile_stream, currentProgressFile);

        }
        else
        {
            progressFile_converter.Serialize(progressFile_stream, new AllLevelProgress());

        }

        progressFile_stream.Close();
    }

    public static void LoadProgress()
    {
        ExternalDirectories.EnsureFolderExists(progressPath);

        string loadPath = ExternalDirectories.SanitizePath(progressPath_withFile, true);


        if (File.Exists(loadPath))
        {
            FileStream progressFile_stream = new FileStream(loadPath, FileMode.Open);

            BinaryFormatter progressFile_converter = new BinaryFormatter();
            currentProgressFile = progressFile_converter.Deserialize(progressFile_stream) as AllLevelProgress;

            progressFile_stream.Close();

        }
        else
        {
            currentProgressFile = new AllLevelProgress();
        }
    }
}
