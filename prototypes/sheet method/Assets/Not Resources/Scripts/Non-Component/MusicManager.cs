using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;

public enum E_TrackList
{
    Basic,
    Jungle,
    Snow,
    Desert,
    Factory,
    Space,
    Sweets,
    MainMenu
}

public static class MusicManager
{

    static Dictionary<E_TrackList, AudioClip> allTracks = new Dictionary<E_TrackList, AudioClip>();


    static GameObject musicPlayer_host=null;
    static AudioSource musicPlayer = null;







    static void InitPlayer()
    {


        if (musicPlayer_host == null)
        {

            musicPlayer_host = new GameObject();
            musicPlayer_host.name = "musicPlayer_host";
        }

        if (musicPlayer_host != null)
        {
            GameObject.DontDestroyOnLoad(musicPlayer_host);

            AudioSource getAttempt = musicPlayer_host.GetComponent<AudioSource>();

            if (getAttempt != null)
            {
                musicPlayer = getAttempt;
            }
            else
            {
                musicPlayer = musicPlayer_host.AddComponent<AudioSource>();

            }

            Component.DontDestroyOnLoad(musicPlayer);



            musicPlayer.volume = 1.0f;
            musicPlayer.loop = true;
            musicPlayer.playOnAwake = false;
        }


    }



    static void LoadSongs(string musicDirectory = "Music/")
    {
        if (allTracks == null)
        {
            allTracks = new Dictionary<E_TrackList, AudioClip>();

        }
        else
        {
            allTracks.Clear();

        }

        List<E_TrackList> allTrackTypes = System.Enum.GetValues(typeof(E_TrackList)).Cast<E_TrackList>().ToList();

        Dictionary<E_TrackList, AudioClip> allTracks_loop = new Dictionary<E_TrackList, AudioClip>();

        foreach (E_TrackList type in allTrackTypes)
        {
            allTracks.Add(type, null);
            allTracks_loop.Add(type, null);
        }

        foreach(KeyValuePair<E_TrackList,AudioClip> track in allTracks_loop)
        {
            switch (track.Key)
            {
                case E_TrackList.Factory:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "factory");
                    break;

                case E_TrackList.Snow:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "snow");
                    break;

                case E_TrackList.Basic:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "basic");
                    break;

                case E_TrackList.Desert:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "desert");
                    break;
                case E_TrackList.Jungle:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "jungle");
                    break;
                case E_TrackList.Space:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "space");
                    break;
                case E_TrackList.Sweets:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "sweets");
                    break;
                case E_TrackList.MainMenu:
                    allTracks[track.Key] = Resources.Load<AudioClip>(musicDirectory + "mainMenu");

                    break;
            }
        }


    }

    public static void PlaySong(E_TrackList trackToPlay, bool stopOnError=true)
    {

        InitPlayer();

        if(allTracks==null || (allTracks!=null && allTracks.Count<=0))
        {
            LoadSongs();

        }

        bool playerExists = musicPlayer != null;
        bool keyExists = allTracks.ContainsKey(trackToPlay);
        bool clipExists = allTracks[trackToPlay] != null;

        bool readyToPlay = playerExists && keyExists && clipExists;

        if (readyToPlay)
        {
            musicPlayer.clip = allTracks[trackToPlay];

            if (!musicPlayer.isPlaying)
            {
                musicPlayer.Play();
            }

        }
        else
        {
            if (stopOnError)
            {
                if (musicPlayer.isPlaying)
                {
                    musicPlayer.Stop();
                }
            }

        }

    }

    public static void StopSong()
    {
        if (musicPlayer != null)
        {
            if (musicPlayer.isPlaying)
            {
                musicPlayer.Stop();
            }

        }

    }

    public static E_TrackList ShiftTrackType(E_TrackList baseType, int shiftAmt)
    {
        int curEnum_asInt = 0;

        List<E_TrackList> allEnumValues = System.Enum.GetValues(typeof(E_TrackList)).Cast<E_TrackList>().ToList();

        foreach (E_TrackList type in allEnumValues)
        {


            if (type == baseType)
            {
                curEnum_asInt = (int)type;
                break;
            }
        }

        int newIndex_noModulation = (curEnum_asInt + shiftAmt);
        int newIndex = Mathf.Abs(newIndex_noModulation % allEnumValues.Count);

        E_TrackList newType = allEnumValues[newIndex];

        if (newIndex_noModulation < 0)
        {
            newType = allEnumValues[allEnumValues.Count - newIndex];
        }



        return newType;
    }
}
