using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;



[System.Serializable]
public sealed class LevelFile_ProgressHookData
{
    public string LevelID { get; private set; }//this is used for tracking progress in a level on a user's machine, it is just the level name + _ + date and time's numerics.







    public LevelFile_ProgressHookData(string id)
    {
        LevelID = id;

    }

    public LevelFile_ProgressHookData()
    {
        LevelID = ExtendedFunctionality.GenerateLevelID();

    }
}


[System.Serializable]
public sealed class LevelFile
{


    public string levelName="Unnamed Level";
    public byte[] heightmapTex_asBytes=null;
    public List<(byte[] texAsBytes, float baseRaiseGeometry)> levelPlanTex_asBytes_ALL=null;

    public E_LevelThemeTypes levelThemeType=E_LevelThemeTypes.Basic;
    public E_TrackList levelMusicType = E_TrackList.Basic;


    public LevelFile_ProgressHookData progressHookData = new LevelFile_ProgressHookData();

}

public enum E_SaveFileType
{
    Gameplay,
    Creation,
    Testing
}

public static class LevelFileManager
{

    static readonly string SAVEFILE_EXTENSION = ".magic".ToLower();

    static Dictionary<E_SaveFileType, LevelFile> loadedLevelFiles = new Dictionary<E_SaveFileType, LevelFile>();


    public static void CopyLoadedLevelFileToAnotherType(E_SaveFileType sourceType, E_SaveFileType destType, bool nullCheck=false)
    {
        bool sourceExists = loadedLevelFiles.ContainsKey(sourceType);
        bool sourceIsNullSafe = false;

        if (sourceExists)
        {
            sourceIsNullSafe = loadedLevelFiles[sourceType] != null;
        }


        if((sourceIsNullSafe&& nullCheck) || !nullCheck)
        {
            LevelFile copyData = ExtendedFunctionality.DeepCopy<LevelFile>(loadedLevelFiles[sourceType]);
            SafeAddToLoadedLevelFiles(destType, copyData);
        }
    }


    public static void SafeAddToLoadedLevelFiles(E_SaveFileType saveFileType, LevelFile fileData)
    {
        if (loadedLevelFiles.ContainsKey(saveFileType))
        {
            loadedLevelFiles[saveFileType] = fileData;
        }
        else
        {
            loadedLevelFiles.Add(saveFileType, fileData);
        }
    }


    public static void SetLevelFile(E_SaveFileType saveFileType, LevelFile fileData, bool nullCheck=false)
    {
        if((nullCheck && fileData!=null) || !nullCheck)
        {
            SafeAddToLoadedLevelFiles(saveFileType, fileData);
        }
    }

    //similar to GetLoadedLevelFileByType if overwrite is false, but this function will guarantee a non-null output.
    public static LevelFile CreateLevelFile(E_SaveFileType saveFileType, bool overwrite)
    {



        bool keyExists = loadedLevelFiles.ContainsKey(saveFileType);

        bool keyIsValid = false;

        if (keyExists)
        {
            keyIsValid = loadedLevelFiles[saveFileType] != null;
        }

        bool nonNullDataExists = keyExists && keyIsValid;


        if(overwrite || (!overwrite && !nonNullDataExists))
        {
            SafeAddToLoadedLevelFiles(saveFileType, new LevelFile());


        }

   
        return loadedLevelFiles[saveFileType];

    }

    public static LevelFile GetLoadedLevelFileByType(E_SaveFileType saveFileType)
    {
        LevelFile ret = null;

        if (loadedLevelFiles.ContainsKey(saveFileType))
        {
            ret = loadedLevelFiles[saveFileType];
        }

        return ret;
    }

    public static void SaveLevelFileFromLoadedLevelFiles(E_SaveFileType saveFileType, string saveName="unnamedLevel")
    {


        if (loadedLevelFiles.ContainsKey(saveFileType))
        {
            LevelFile saveFile = loadedLevelFiles[saveFileType];
            SaveLevelFile(saveFile, saveName);

        }

    }


    public static LevelFile LoadLevelFile_FromSavedLevels(E_SaveFileType slotToLoadInto, string levelName)
    {
        List<string> loadPieces= GetPathPiecesToSavedLevelByName(levelName);

        if (loadPieces != null)
        {
            return LoadLevelFile(slotToLoadInto, loadPieces);

        }
        else
        {

            return CreateLevelFile(slotToLoadInto,false);

        }


    }

    static LevelFile LoadLevelFile(E_SaveFileType slotToLoadInto, List<string> loadPath_pieces)
    {
        string loadPath = ExternalDirectories.SanitizePath(loadPath_pieces, true);
        LevelFile ret = null;

        if (!((loadPath.ToLower()).EndsWith(SAVEFILE_EXTENSION)))
        {
            loadPath += SAVEFILE_EXTENSION;
        }

        if (File.Exists(loadPath))
        {
            FileStream levelSaveFile_stream = new FileStream(loadPath, FileMode.Open);

            BinaryFormatter levelSaveFile_converter = new BinaryFormatter();
            ret = levelSaveFile_converter.Deserialize(levelSaveFile_stream) as LevelFile;

            levelSaveFile_stream.Close();


            SafeAddToLoadedLevelFiles(slotToLoadInto, ret);

        }


        return ret;


    }

    public static List<LevelFile> LoadLevelFileDirectory(E_LevelFolderToLoad folderToLoad)
    {
        LevelProgressManager.LoadProgress();


        List<string> folderPieces = new List<string>();
        switch (folderToLoad)
        {
            case E_LevelFolderToLoad.Custom:

                folderPieces = ExternalDirectories.SAVEDLEVELSDIR_FULLPIECES;

                break;
            case E_LevelFolderToLoad.PreMade:
            default:

                folderPieces = ExternalDirectories.PREMADELEVELSDIR_FULLPIECES;

                break;
        }


        string[] allFilePaths = Directory.GetFiles(ExternalDirectories.SanitizePath(folderPieces));


        List<LevelFile>  loadedLevelFiles = new List<LevelFile>();

        for (int i = 0; i < allFilePaths.Length; i++)
        {
            loadedLevelFiles.Add(LevelFileManager.LooseLoadLevelFile(allFilePaths[i]));
        }

        return loadedLevelFiles;
    }




    public static LevelFile LooseLoadLevelFile(string sanitizedLevelPath)
    {
        LevelFile ret = null;

        if (File.Exists(sanitizedLevelPath))
        {
            FileStream levelSaveFile_stream = new FileStream(sanitizedLevelPath, FileMode.Open);

            BinaryFormatter levelSaveFile_converter = new BinaryFormatter();
            ret = levelSaveFile_converter.Deserialize(levelSaveFile_stream) as LevelFile;

            levelSaveFile_stream.Close();

        }


        return ret;


    }

    static List<string> GetPathPiecesToSavedLevelByName(string levelName)
    {
        List<string> ret = null;

        if(levelName.Replace(" ", "") != "")
        {
            ret = ExternalDirectories.SAVEDLEVELSDIR_FULLPIECES;

            if (levelName.ToLower().EndsWith(SAVEFILE_EXTENSION))
            {
                int cutoffIndex = levelName.ToLower().LastIndexOf(SAVEFILE_EXTENSION);
                levelName = levelName.Substring(0, cutoffIndex);
            }

            ret.Add(levelName + SAVEFILE_EXTENSION);
        }


        return ret;
    }

    static void SaveLevelFile(LevelFile saveData, string saveName="unnamedLevel")
    {
        if (saveData != null)
        {
            saveData.levelName = saveName;
            saveData.progressHookData = new LevelFile_ProgressHookData(ExtendedFunctionality.GenerateLevelID(saveName));


            List<string> savePath_pieces = GetPathPiecesToSavedLevelByName(saveName);


            if (savePath_pieces!=null)
            {

                string savePath = ExternalDirectories.SanitizePath(savePath_pieces, true);




                FileStream levelSaveFile_stream = new FileStream(savePath, FileMode.Create);

                BinaryFormatter levelSaveFile_converter = new BinaryFormatter();
                levelSaveFile_converter.Serialize(levelSaveFile_stream, saveData);

                levelSaveFile_stream.Close();

            }



        }

    }

}
