using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;

public enum E_LevelThemeTypes
{
    Basic,
    Jungle,
    Snow,
    Desert,
    Factory,
    Space,
    Sweets
}

[System.Serializable]
public sealed class LevelTheme
{
    public string FloorMat_Sides_ResourcePath { get; private set; }
    public string FloorMat_Roof_ResourcePath { get; private set; }
    public string SkyboxMat_ResourcePath { get; private set; }

    public LevelTheme(string sidesPath, string roofPath, string skyPath)
    {
        FloorMat_Sides_ResourcePath = sidesPath;
        FloorMat_Roof_ResourcePath = roofPath;
        SkyboxMat_ResourcePath = skyPath;
    }
}

public static class LevelThemeCollection
{
    static readonly LevelTheme FallbackTheme = new LevelTheme("Materials/Themes/Basic/floorSidesMat", "Materials/Themes/Basic/floorRoofMat", "Skyboxes/sky_basic");

    static readonly Dictionary<E_LevelThemeTypes,LevelTheme> AllLevelThemes = new Dictionary<E_LevelThemeTypes, LevelTheme>        {
            { E_LevelThemeTypes.Basic, FallbackTheme },
            { E_LevelThemeTypes.Desert, new LevelTheme("Materials/Themes/Desert/floorSidesMat","Materials/Themes/Desert/floorRoofMat","Skyboxes/sky_desert") },
            { E_LevelThemeTypes.Jungle, new LevelTheme("Materials/Themes/Jungle/floorSidesMat","Materials/Themes/Jungle/floorRoofMat","Skyboxes/sky_jungle") },
            { E_LevelThemeTypes.Snow, new LevelTheme("Materials/Themes/Snow/floorSidesMat","Materials/Themes/Snow/floorRoofMat","Skyboxes/sky_snow") },
            { E_LevelThemeTypes.Sweets, new LevelTheme("Materials/Themes/Sweets/floorSidesMat","Materials/Themes/Sweets/floorRoofMat","Skyboxes/sky_sweets") },
            { E_LevelThemeTypes.Factory, new LevelTheme("Materials/Themes/Factory/floorSidesMat","Materials/Themes/Factory/floorRoofMat","Skyboxes/sky_factory") },
            { E_LevelThemeTypes.Space, new LevelTheme("Materials/Themes/Space/floorSidesMat","Materials/Themes/Space/floorRoofMat","Skyboxes/sky_space") }
        };



    //for calritys sake: i know you can cast an enum to an int, but in case a programmer set their enum values to custom ints, this wont work, so i will use their place in the generated list as surrogate ints.
    //use a shift amount of 0 to get a levelthem by its type!
    public static LevelTheme GetThemeFromDictionaryByType(E_LevelThemeTypes themeType)
    {
        if (AllLevelThemes.ContainsKey(themeType))
        {
            return AllLevelThemes[themeType];
        }
        else
        {
            return FallbackTheme;
        }

    }

    public static E_LevelThemeTypes ShiftToNextTypeInDictionary(E_LevelThemeTypes themeType_current, int initialShift)
    {
        E_LevelThemeTypes newType = GetThemeTypeAtShiftOffsetFromCurrentThemeType(themeType_current,initialShift);
        if (AllLevelThemes.Count > 0)
        {
            while (!AllLevelThemes.ContainsKey(newType))
            {
                if (initialShift>0)
                {
                    newType = GetThemeTypeAtShiftOffsetFromCurrentThemeType(newType, 1);
                }
                else
                {

                    newType = GetThemeTypeAtShiftOffsetFromCurrentThemeType(newType, -1);

                }
            }
            return newType;
        }
        else
        {
            return newType;
        }
    }

    static E_LevelThemeTypes GetThemeTypeAtShiftOffsetFromCurrentThemeType(E_LevelThemeTypes themeType_current, int shiftAmt)
    {
        int curEnum_asInt = 0;

        List<E_LevelThemeTypes> allEnumValues = System.Enum.GetValues(typeof(E_LevelThemeTypes)).Cast<E_LevelThemeTypes>().ToList();

        foreach (E_LevelThemeTypes type in allEnumValues)
        {


            if (type == themeType_current)
            {
                curEnum_asInt = (int)type;
                break;
            }
        }



        int newIndex_noModulation = (curEnum_asInt + shiftAmt);
        int newIndex = Mathf.Abs(newIndex_noModulation % allEnumValues.Count);

        E_LevelThemeTypes newType = allEnumValues[newIndex];

        //the modulo should be enough but for some reason according to online modulo calculators but ITS NOT sooo
        if (newIndex_noModulation < 0)
        {
            newType = allEnumValues[allEnumValues.Count - newIndex];
        }








        return newType;
    }

}
