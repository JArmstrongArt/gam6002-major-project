using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Statistics : MonoBehaviour,IGeneratedEntityOrMeshInit
{
    private int score_max = 0;
    private int enemyKillCount_max = 0;


    public int Score_Max
    {
        get
        {
            return score_max;
        }
    }

    public int EnemyKillCount_Max
    {
        get
        {
            return enemyKillCount_max;
        }
    }


    private int score = 0;

    public int Score
    {
        set
        {


            value = Mathf.Clamp(value, 0, value);

            score = value;
        }

        get
        {
            return score;
        }
    }

    public int enemyKillCount = 0;
    public int deathCount=0;

    [SerializeField] float fallToDeathWorldHeight = 50.0f;
    public float FallToDeathWorldHeight
    {
        get
        {
            return fallToDeathWorldHeight;
        }
    }

    [SerializeField] float hazardousGoop_moveSpeed_multiplier = 0.45f;
    [SerializeField] float hazardousGoop_jumpPower_multiplier = 1.0f;



    [SerializeField] float waterMode_gravRateMultiplier = 0.25f;
    [SerializeField] float waterMode_movementDriftDivider = 2.0f;

    public float WaterMode_MovementDriftDivider
    {
        get
        {
            return waterMode_movementDriftDivider;
        }
    }



    public float WaterMode_GravRateMultiplier
    {
        get
        {
            return waterMode_gravRateMultiplier;
        }
    }


    public float HazardousGoop_MoveSpeed_Multiplier
    {
        get
        {
            return hazardousGoop_moveSpeed_multiplier;
        }
    }

    public float HazardousGoop_JumpPower_Multiplier
    {
        get
        {
            return hazardousGoop_jumpPower_multiplier;
        }
    }

    public bool HazardousGoop_Active { set; get; }



    public bool WaterMode { set; get; }

    private GameObject hud_prefab;

    private void Awake()
    {
        if (FindResources())
        {

            switch (SheetMethod.currentGenerateType)
            {
                case E_SaveFileType.Gameplay:
                case E_SaveFileType.Testing:
                    SpawnHUD();
                    break;
            }

        }
    }


    void SpawnHUD()
    {
        if (FindResources())
        {
            GameObject hud_inst = Instantiate(hud_prefab, null);
            CNV_MainHUD hudScr = hud_inst.GetComponent<CNV_MainHUD>();

            if (hudScr == null)
            {
                ExtendedFunctionality.SafeDestroy(hud_inst);
            }
            else
            {
                hudScr.StatScr = this;
            }
        }
    }

    bool FindResources()
    {
        bool ret = false;

        if (hud_prefab == null)
        {
            hud_prefab = Resources.Load<GameObject>("UI/Gameplay/cnv_mainHUD/cnv_mainHUD");
        }

        if (hud_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

    public void SetupEntityOrMesh(GameObject playerObj = null)
    {
        if (FindResources())
        {
            score_max = GameObject.FindObjectsOfType<Collectable>().Length;
            enemyKillCount_max = GameObject.FindObjectsOfType<Enemy>().Length;
        }
    }
}
