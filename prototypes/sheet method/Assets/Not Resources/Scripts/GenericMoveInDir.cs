using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericMoveInDir : MonoBehaviour
{
    [SerializeField] float moveSpeed=1;
    public float MoveSpeed
    {
        set
        {
            moveSpeed = value;
        }
    }

    [SerializeField] Vector3 moveDir;
    public Vector3 MoveDir
    {
        set
        {
            moveDir = value;
        }
    }



    public Vector3 GetNextPosition(float timeUnit)
    {

        return moveDir.normalized * moveSpeed * timeUnit;
    }

    [SerializeField] bool selfMoving = true;

    public bool SelfMoving
    {
        set
        {
            selfMoving = value;
        }
    }


    private void FixedUpdate()
    {
        if (selfMoving)
        {
            gameObject.transform.position += GetNextPosition(Time.fixedDeltaTime);
        }
    }

}
