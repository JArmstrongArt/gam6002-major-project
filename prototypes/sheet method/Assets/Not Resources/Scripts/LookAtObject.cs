using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObject : MonoBehaviour
{
    [SerializeField] GameObject lookTarget = null;
    public GameObject LookTarget
    {
        set
        {
            lookTarget = value;
        }
    }


    // Update is called once per frame
    void LateUpdate()
    {
        if (lookTarget != null)
        {
            Vector3 lookDir = (lookTarget.transform.position - gameObject.transform.position).normalized;
            gameObject.transform.rotation = Quaternion.LookRotation(lookDir, Vector3.up);
        }

    }
}
