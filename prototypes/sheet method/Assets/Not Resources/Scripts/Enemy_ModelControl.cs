using System.Collections;
using System.Collections.Generic;
using UnityEngine;


enum E_ModelStates_Enemy
{
    Idle
}

public class Enemy_ModelControl : ModelControl
{
    protected override void DecideAnimation_ApplyFromContext(int? animEnum)
    {
        if (base.FindResources() && base.FindDependencies())
        {
            int animEnum_nonNull = animEnum ?? (int)E_ModelStates_Enemy.Idle;

            E_ModelStates_Enemy modelState = (E_ModelStates_Enemy)animEnum_nonNull;


            switch (modelState)
            {

                case E_ModelStates_Enemy.Idle:
                default:

                    MyModel_Anim.Play("Idle");
                    break;


            }

            MyModel_Inst.transform.rotation = gameObject.transform.rotation;

        }

    }

    protected override int? DecideAnimation_Contextual()
    {
        int? ret = null;
        if (base.FindResources() && base.FindDependencies())
        {




            E_ModelStates_Enemy animToPlay = E_ModelStates_Enemy.Idle;



            ret = (int)animToPlay;


        }
        return ret;
    }
}
