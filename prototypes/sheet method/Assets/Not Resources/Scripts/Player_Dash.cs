using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Dash : MonoBehaviour
{

    private Player_GravityControl gravScr;
    [SerializeField] float dashMultiplier = 2.0f;
    public float DashMultiplier
    {
        get
        {
            return dashMultiplier;
        }
    }

    [SerializeField] float dashDuration = 0.5f;

    public float DashDuration
    {
        get
        {
            return dashDuration;
        }
    }

    private float dashDuration_current = 0.5f;

    public float DashDuration_Current
    {
        get
        {
            return dashDuration_current;
        }
    }

    private InputManager inputScr;

    private bool dash_axisUsed;

    public bool DashInProgress
    {
        get
        {
            return dashDuration_current < dashDuration;
        }
    }

    private GameObject sE_player_dash;

    [SerializeField] float minMoveForceToDash = 0.1f;

    [SerializeField] float enemyKillRadius = 1.3f;

    private Enemy[] allEnemies = null;
    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies() && FindResources())
        {
            dashDuration_current = dashDuration;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (FindDependencies() && FindResources())
        {
            if (DashInProgress)
            {
                dashDuration_current += Time.deltaTime;

                if (allEnemies != null)
                {

                    Enemy closestEnemy = null;
                    foreach (Enemy enemy in allEnemies)
                    {
                        if (enemy != null)
                        {
                            float enemyDist = Vector3.Distance(gameObject.transform.position, enemy.transform.position);

                            if (enemyDist <= enemyKillRadius)
                            {
                                closestEnemy = enemy;
                                break;
                            }
                        }

                    }

                    if (closestEnemy != null)
                    {
                        closestEnemy.SlowdownOnDeath = true;
                        dashDuration_current = 0.0f;

                        Destroy(closestEnemy.gameObject);
                    }
                }
            }
            else
            {
                dashDuration_current = dashDuration;


                float moveInput = Mathf.Clamp01(Mathf.Abs( inputScr.GetInputByName("Movement", "Horizontal"))+Mathf.Abs( inputScr.GetInputByName("Movement", "Vertical")));

                if (moveInput >= Mathf.Clamp01(minMoveForceToDash))
                {
                    if (inputScr.GetInputByName("Movement", "Dash") > 0)
                    {
                        if (gravScr.GroundRay.RaySurface != null)
                        {
                            if (dash_axisUsed == false)
                            {
                                allEnemies = GameObject.FindObjectsOfType<Enemy>();
                                Instantiate(sE_player_dash);
                                dashDuration_current = 0.0f;
                                dash_axisUsed = true;
                            }
                        }
                        else
                        {
                            dash_axisUsed = true;
                        }

                    }
                    else
                    {
                        dash_axisUsed = false;
                    }
                }
                else
                {
                    dash_axisUsed = false;
                }

            }


        }
    }

    bool FindDependencies()
    {
        bool ret = false;



        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<Player_GravityControl>();
        }

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (inputScr!=null && gravScr!=null)
        {
            ret = true;
        }
        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_player_dash == null)
        {
            sE_player_dash = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_player_dash/sE_player_dash");
        }

        if (sE_player_dash != null)
        {
            ret = true;
        }

        return ret;
    }
}
