using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour
{
    private GameObject sE_level_win;

    private Player_Statistics playerStats;


    private void Update()
    {
        if (FindResources())
        {
            if (playerStats == null)
            {
                playerStats = GameObject.FindObjectOfType<Player_Statistics>();
            }

            if (playerStats != null)
            {
                Vector3 playerPos_flat = new Vector3(playerStats.transform.position.x, gameObject.transform.position.y, playerStats.transform.position.z);
                Vector3 faceToPlayer = (playerPos_flat - gameObject.transform.position).normalized;

                if (faceToPlayer != Vector3.zero)
                {
                    gameObject.transform.rotation = Quaternion.LookRotation(faceToPlayer);

                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (FindResources())
        {
            Player_Statistics playerProof = other.GetComponent<Player_Statistics>();

            if (playerProof != null)
            {

                switch (SheetMethod.currentGenerateType)
                {
                    case E_SaveFileType.Testing:
                        BTN_DelegateFunctions.CopyLevelFileThenGoToScene(E_SaveFileType.Testing, E_SaveFileType.Creation, "LevelImportPreview");
                        break;

                    case E_SaveFileType.Gameplay:
                        RecordProgressOfCurrentLevel(playerProof);
                        BTN_DelegateFunctions.GoToScene("LevelSelect", E_SaveFileType.Gameplay);
                        break;
                }

                Instantiate(sE_level_win);
            }
        }


    }

    void RecordProgressOfCurrentLevel(Player_Statistics statScr)
    {
        if (FindResources())
        {

            LevelProgress newProg = LevelProgressManager.GetProgressByLevelID_Exposer(SheetMethod.LoadedLevelData.progressHookData);

            newProg.AddAchievedCriteria(E_LevelProgress_CriteriaType.LevelCleared);
            newProg.AddAchievedCriteria(E_LevelProgress_CriteriaType.AllCollectables, statScr.Score >= statScr.Score_Max);
            newProg.AddAchievedCriteria(E_LevelProgress_CriteriaType.AllEnemies, statScr.enemyKillCount >= statScr.EnemyKillCount_Max);
            newProg.AddAchievedCriteria(E_LevelProgress_CriteriaType.NoDeaths, statScr.deathCount <= 0);


            LevelProgressManager.SetProgressByLevelID_Exposer(newProg);
        }





    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_level_win == null)
        {
            sE_level_win = Resources.Load<GameObject>("Sound Effects/Gameplay/sE_level_win/sE_level_win");
        }

        if (sE_level_win != null)
        {
            ret = true;
        }

        return ret;
    }
}
