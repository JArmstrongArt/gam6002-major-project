using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this is a script i pass around on a lot of my projects that allows you to make a raycast on any gameobject with highly customizable settings, it's far easier than setting up raycast code time and time again.


//the possible directions the ray can go, custom is a special mode that grabs the direction from the customDir_vec variable.

public enum E_RayDirections
{
    V3Up,
    V3Down,
    V3Left,
    V3Right,
    V3Forward,
    V3Backward,
    TransformUp,
    TransformDown,
    TransformLeft,
    TransformRight,
    TransformForward,
    TransformBackward,
    Custom


}


//the possible update cycles this raycast will operate under.

public enum E_UpdateType
{
    Regular,
    Fixed,
    Late,
    Manual
}



public class GenericRay : MonoBehaviour
{

    [SerializeField] E_UpdateType updateType = E_UpdateType.Manual;


    private Vector3 targetDir;//the actual vector3 direction this ray will move in based on 'direction' enum instance.
    [SerializeField] E_RayDirections rayDirection=E_RayDirections.Custom;
    [SerializeField] float rayLength;
    public float RayLength
    {
        get
        {
            return rayLength;
        }

        set
        {
            rayLength = value;
        }
    }
    private Ray ray;


    [SerializeField] bool detectTriggers=false;
    [SerializeField] LayerMask rayLayers;//the layers which the ray can collide with.

    public LayerMask RayLayers
    {
        set
        {
            rayLayers = value;
        }
    }

    public GameObject RaySurface
    {
        get; private set;
    }

    public Vector3 RayHitPoint
    {
        get; private set;
    }





    public Vector3 CustomDirVector
    {
        set; get;
    }
    [SerializeField] Color rayCol=Color.blue;

    private RaycastHit? currentRayHit;
    [SerializeField] List<string> triggerClassIncludeNames;//if this list is not empty, then detectTriggers will only include triggers that have these script types attached.
    private bool temporaryDisabled = false;
    public bool TemporarilyDisabled
    {
        set
        {
            temporaryDisabled = value;
        }
    }
    // Use this for initialization
    void Awake()
    {

        RayHitPoint = Vector3.zero;
    }

    void FindOutTargetDir()
    {
        switch (rayDirection)
        {
            case E_RayDirections.V3Up:
                targetDir = Vector3.up;
                break;
            case E_RayDirections.V3Down:
                targetDir = Vector3.down;
                break;
            case E_RayDirections.V3Left:
                targetDir = Vector3.left;
                break;
            case E_RayDirections.V3Right:
                targetDir = Vector3.right;
                break;
            case E_RayDirections.V3Forward:
                targetDir = Vector3.forward;
                break;
            case E_RayDirections.V3Backward:
                targetDir = Vector3.back;
                break;
            case E_RayDirections.TransformBackward:
                targetDir = -transform.forward;
                break;
            case E_RayDirections.TransformForward:
                targetDir = transform.forward;
                break;
            case E_RayDirections.TransformDown:
                targetDir = -transform.up;
                break;
            case E_RayDirections.TransformUp:
                targetDir = transform.up;
                break;
            case E_RayDirections.TransformRight:
                targetDir = transform.right;
                break;
            case E_RayDirections.TransformLeft:
                targetDir = -transform.right;
                break;
            case E_RayDirections.Custom:
                targetDir = CustomDirVector;
                break;
        }
    }

    bool TriggerFilteringByClass(GameObject sourceObj)
    {
        bool ret = false;
        List<string> validClassNames = new List<string>();

        if (triggerClassIncludeNames != null)
        {
            foreach (string className in triggerClassIncludeNames)
            {
                System.Type typeFromString = System.Type.GetType(className);
                if (typeFromString != null)
                {
                    validClassNames.Add(className);
                }
            }

            if (validClassNames.Count > 0)
            {
                foreach (string className in validClassNames)
                {
                    System.Type typeFromString = System.Type.GetType(className);

                    Component compExists = sourceObj.GetComponent(typeFromString);
                    if (compExists != null)
                    {
                        ret = true;
                        break;
                    }
                }
            }
            else
            {
                ret = true;
            }


        }
        return ret;
    }
    public void RayRoutine()
    {
        if (!temporaryDisabled)
        {
            //set targetDir appropriate to the 'direction' enum instance.
            FindOutTargetDir();

            ray = new Ray(gameObject.transform.position, targetDir);

            RaycastHit[] allHits = Physics.RaycastAll(ray, rayLength, rayLayers);
            int? hitIndex = null;
            for(int i=0;i<allHits.Length;i++)
            {
                if (allHits[i].collider != null)
                {
                    if (allHits[i].collider.isTrigger == true)
                    {
                        if (detectTriggers)
                        {
                            if (TriggerFilteringByClass(allHits[i].collider.gameObject))
                            {
                                hitIndex = i;
                                break;
                            }
                        }
                    }
                    else
                    {
                        hitIndex = i;
                        break;
                    }

                }
            }

            if (hitIndex != null)
            {
                currentRayHit = allHits[hitIndex ?? 0];

                RayHitPoint = (currentRayHit??default(RaycastHit)).point;
                RaySurface = (currentRayHit ?? default(RaycastHit)).collider.gameObject;

            }
            else
            {
                NoCollisionsSetting();

            }

        }
        else
        {
            NoCollisionsSetting();
        }





    }

    void NoCollisionsSetting()
    {
        currentRayHit = null;
        RaySurface = null;

        RayHitPoint = Vector3.zero;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (updateType == E_UpdateType.Late)
        {
            RayRoutine();
        }


    }

    void FixedUpdate()
    {
        if (updateType == E_UpdateType.Fixed)
        {
            RayRoutine();
        }


    }
    void Update()
    {
        if (updateType == E_UpdateType.Regular)
        {
            RayRoutine();
        }


    }


    private void OnDrawGizmos()
    {
        Gizmos.color = rayCol;
        Gizmos.DrawLine(gameObject.transform.position, gameObject.transform.position + (targetDir * rayLength));
    }


}
