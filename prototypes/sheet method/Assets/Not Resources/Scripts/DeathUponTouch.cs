using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathUponTouch : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Player_Respawn respawnScr = other.GetComponent<Player_Respawn>();

        if (respawnScr != null)
        {
            respawnScr.Respawn();

        }
    }
}
