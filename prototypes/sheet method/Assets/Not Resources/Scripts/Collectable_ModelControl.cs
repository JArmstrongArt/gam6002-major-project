using System.Collections;
using System.Collections.Generic;
using UnityEngine;



enum E_ModelStates_Collectable
{
    Idle,
    Bounce
}

public class Collectable_ModelControl : ModelControl
{

    public override void SetupEntityOrMesh(GameObject playerObj = null)
    {
        if (base.FindResources() && base.FindDependencies())
        {
            base.SetupEntityOrMesh();
            SetSmileyColour();

        }
    }

    public Color? GetSmileyColour()
    {
        Color? ret = null;

        if (base.FindResources() && base.FindDependencies())
        {
            if (MyModel_Inst != null)
            {





                Renderer rend = MyModel_Inst.GetComponentInChildren<Renderer>();

                if (rend != null)
                {
                    Material[] rendMats = rend.materials;

                    if (rendMats != null)
                    {
                        for (int i = 0; i < rendMats.Length; i++)
                        {
                            if (rendMats[i].HasProperty("reflectColour"))
                            {
                                ret = rendMats[i].GetColor("reflectColour");
                                break;
                            }
                        }
                    }
                }
            }
        }

        return ret;
    }

    public void SetSmileyColour(Color? randomColourOverride=null)
    {
        if (base.FindResources() && base.FindDependencies())
        {
            if (MyModel_Inst != null)
            {
                Color setCol = randomColourOverride ?? ExtendedFunctionality.RainbowRandomThisColour();



                Renderer rend = MyModel_Inst.GetComponentInChildren<Renderer>();

                if (rend != null)
                {
                    Material[] rendMats = rend.materials;

                    if (rendMats != null)
                    {
                        for (int i = 0; i < rendMats.Length; i++)
                        {
                            if (rendMats[i].HasProperty("reflectColour"))
                            {
                                rendMats[i].SetColor("reflectColour", setCol);
                            }
                        }
                    }
                }
            }
        }

    }


    protected override void DecideAnimation_ApplyFromContext(int? animEnum)
    {
        if (base.FindResources() && base.FindDependencies())
        {
            int animEnum_nonNull = animEnum ?? (int)E_ModelStates_Collectable.Idle;

            E_ModelStates_Collectable modelState = (E_ModelStates_Collectable)animEnum_nonNull;


            switch (modelState)
            {

                case E_ModelStates_Collectable.Idle:
                default:

                    MyModel_Anim.Play("Idle");
                    break;

                case E_ModelStates_Collectable.Bounce:

                    MyModel_Anim.Play("Bounce");
                    break;


            }

            ModelOffset =new Vector3(0, -Mathf.Abs(ExtendedFunctionality.GetHalfMeasureOfGameObject(gameObject, E_WorldAxes.Y)),0);
            MyModel_Inst.transform.rotation = gameObject.transform.rotation;

        }

    }

    protected override int? DecideAnimation_Contextual()
    {
        int? ret = null;
        if (base.FindResources() && base.FindDependencies())
        {




            E_ModelStates_Collectable animToPlay = E_ModelStates_Collectable.Bounce;



            ret = (int)animToPlay;


        }
        return ret;
    }


}
