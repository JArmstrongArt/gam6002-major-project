﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class GeneralSoundEffect : MonoBehaviour,IEditorFunctionality
{
    [SerializeField] AudioClip[] allSoundEffects;
    [SerializeField] Vector2 pitchRangePercent = new Vector2(1,1);
    private Vector2 pitchRangeActual;
    private AudioSource soundSource;
    private float? timeBeforeDestruction=null;
    private bool soundPlayed;
    private int selectedSoundIndex;
    [SerializeField] float volumePercent=100;
    [SerializeField] bool looping = false;

    [SerializeField] bool spatial =false;
    [SerializeField] float spatialRadius = 10;




    public void EditorUpdate()
    {
        pitchRangePercent = new Vector2(Mathf.Abs(pitchRangePercent.x), Mathf.Abs(pitchRangePercent.y));
        volumePercent = Mathf.Clamp(volumePercent, 0, 100);
    }

    // Start is called before the first frame update
    void Awake()
    {
        ExtendedFunctionality.GutComponentsFromGameObject(gameObject,new List<System.Type> { this.GetType() });

        gameObject.transform.parent = null;
        for(int i = 0; i < allSoundEffects.Length; i++)
        {
            if (allSoundEffects[i] != null)
            {
                if (timeBeforeDestruction==null || (timeBeforeDestruction!=null && allSoundEffects[i].length > timeBeforeDestruction))
                {

                    timeBeforeDestruction = allSoundEffects[i].length;
                }
            }

        }

        if (gameObject.GetComponent<AudioSource>() != null)
        {
            soundSource = gameObject.GetComponent<AudioSource>();
        }
        else
        {
            soundSource = gameObject.AddComponent<AudioSource>();
        }
        LifeTimer lifeScr = gameObject.GetComponent<LifeTimer>();

        if (!looping)
        {
            if (lifeScr == null)
            {
                lifeScr = gameObject.AddComponent<LifeTimer>();
            }

            lifeScr.LifeTime = timeBeforeDestruction??lifeScr.LifeTime;
        }
        else
        {
            if (lifeScr != null)
            {
                Destroy(lifeScr);
            }
        }


        soundSource.playOnAwake = false;

        selectedSoundIndex = Random.Range(0, allSoundEffects.Length);
        pitchRangeActual = new Vector2((float)pitchRangePercent.x / (float)100, (float)pitchRangePercent.y / (float)100);
    }

    // Update is called once per frame
    void Update()
    {
        if (looping)
        {


            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
        }
        else
        {


            DontDestroyOnLoad(gameObject);
        }

        if (allSoundEffects[selectedSoundIndex] != null)
        {
            if (soundPlayed == false)
            {
                if (looping)
                {
                    soundSource.loop = true;


                }
                else
                {
                    soundSource.loop = false;


                }

                if (spatial == true)
                {
                    soundSource.spatialBlend = 1.0f;
                    soundSource.dopplerLevel = 0.0f;
                    soundSource.spread = 0.0f;
                    soundSource.minDistance = 0.0f;
                    soundSource.maxDistance = spatialRadius;
                    soundSource.rolloffMode = AudioRolloffMode.Linear;
                    
                }
                else
                {
                    soundSource.spatialBlend = 0.0f;
                }
                soundSource.clip = allSoundEffects[selectedSoundIndex];
                soundSource.pitch = Random.Range(pitchRangeActual.x, pitchRangeActual.y);
                soundSource.volume =(float) volumePercent / (float)100;
                soundSource.Play();
                soundPlayed = true;

            }

        }
        else
        {
            Destroy(gameObject);
        }




    }


}
