BUGS
1?. make the mesh generation decoration scale with the level rescale around the player
2?. figure out why the renderer only gets disabled sometimes for the player on the level preview
3?. ignore shapes that have either dimension be shorter than the mesh resolution
4?. figure out why the camera turns slower on the multi-image level when lava is put in (fluke?)
5?. prevent the theme and music from being 'as they were last' when re-entering the level editor

ADDITIONS
1. make the story levels
2. include a readme with the final build saying where to put the level pack
3. add the 100% completion bonus render page